{* BEGIN: main *}
<div class="nv-block-banners">
    {* BEGIN: loop *}

    {* BEGIN: type_image_link *}
    {if $BANNER.type_image_link}

        <img width="{$BANNER.img_width}" alt="{$BANNER.alt}" src="{$BANNER.src_full}" srcset="{$BANNER.srcset}" sizes="(max-width: 991px) 100vw, 459px" class=" | "/>

    {/if}
    {* END: type_image_link *}

    {* BEGIN: type_image *}
    {if $BANNER.type_image}

        <img width="{$BANNER.img_width}" alt="{$BANNER.alt}" src="{$BANNER.src_full}" srcset="{$BANNER.srcset}" sizes="(max-width: 991px) 100vw, 459px" class=" | "/>

    {/if}
    {* END: type_image *}

    {* BEGIN: loop *}
</div>
{* END: main *}