{* BEGIN: main *}
    {* BEGIN: loop *}

        {* BEGIN: type_image_link *}
        {if $BANNER.type_image_link}

            <a id="{$BANNER.link_id}" class="{$BANNER.link_class}>" rel="{$BANNER.link_rel}" href="{$BANNER.link}" target="{$BANNER.target}" title="{$BANNER.link_title}">
                <img alt="{$BANNER.alt}" src="{$BANNER.src_full}" class=" | "/>
            </a>

        {/if}
        {* END: type_image_link *}

        {* BEGIN: type_image *}
        {if $BANNER.type_image}

            <img alt="{$BANNER.alt}" src="{$BANNER.src_full}" class=" | icon-background"/>

        {/if}
        {* END: type_image *}

    {* BEGIN: loop *}
{* END: main *}