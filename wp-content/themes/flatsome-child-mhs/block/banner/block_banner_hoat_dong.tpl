{* BEGIN: main *}
<div>
    <div class="galery-item">
        {* BEGIN: loop *}

        {* BEGIN: type_image_link *}
        {if $BANNER.type_image_link}

            <a id="{$BANNER.link_id}" class="{$BANNER.link_class}>" rel="{$BANNER.link_rel}" href="{$BANNER.link}" target="{$BANNER.target}" title="{$BANNER.link_title}">
                <img width="{$BANNER.img_width}" alt="{$BANNER.alt}" src="{$BANNER.src_full}" srcset="{$BANNER.srcset}" sizes="(max-width: 425px) 100vw, (max-width: 767px) 500px, (max-width: 991px) 700px, 300px" class=" | "/>
            </a>

        {/if}
        {* END: type_image_link *}

        {* BEGIN: type_image *}
        {if $BANNER.type_image}

            <img width="{$BANNER.img_width}" alt="{$BANNER.alt}" src="{$BANNER.src_full}" srcset="{$BANNER.srcset}" sizes="(max-width: 425px) 100vw, (max-width: 767px) 500px, (max-width: 991px) 700px, 300px" class=" | "/>

        {/if}
        {* END: type_image *}

        {* BEGIN: loop *}
    </div>
</div>
{* END: main *}