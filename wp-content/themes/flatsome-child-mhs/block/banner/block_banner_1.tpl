{* BEGIN: main *}

{* BEGIN: loop *}
<div data-p="170.00">
    {* BEGIN: type_image_link *}
    {if $BANNER.type_image_link}

        <a id="{$BANNER.link_id}" class="{$BANNER.link_class} |" rel="{$BANNER.link_rel}" href="{$BANNER.link}" target="{$BANNER.target}" title="{$BANNER.link_title}">
            <img width="{$BANNER.img_width}" data-u="image" alt="{$BANNER.alt}" src="{$BANNER.src_full}" srcset="{$BANNER.srcset}" sizes="(max-width: 991px) 100vw, 870px" class=" |  "/>
        </a>

    {/if}
    {* END: type_image_link *}

    {* BEGIN: type_image *}
    {if $BANNER.type_image}

        <img width="{$BANNER.img_width}" data-u="image" alt="{$BANNER.alt}" src="{$BANNER.src_full}" srcset="{$BANNER.srcset}" sizes="(max-width: 991px) 100vw, 870px" class=" |  "/>

    {/if}
    {* END: type_image *}
</div>

{* BEGIN: loop *}

{* END: main *}