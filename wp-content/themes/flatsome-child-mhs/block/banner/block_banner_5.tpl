{* BEGIN: main *}
	{* BEGIN: loop *}

	{* BEGIN: type_image_link *}
	{if $BANNER.type_image_link}

		<a id="{$BANNER.link_id}" class="{$BANNER.link_class}>" rel="{$BANNER.link_rel}" href="{$BANNER.link}" target="{$BANNER.target}" title="{$BANNER.link_title}">
			<img alt="{$BANNER.alt}" src="{$BANNER.src_full}" class=" | img-responsive hvr-grow"/>
		</a>

	{/if}
	{* END: type_image_link *}

	{* BEGIN: type_image *}
	{if $BANNER.type_image}

		<a class="img_mfp" href="{$BANNER.src_full}">
			<img alt="{$BANNER.alt}" src="{$BANNER.src_full}" class=" | img-responsive hvr-grow no_lazy"/>
		</a>

	{/if}
	{* END: type_image *}

	{* BEGIN: loop *}
{* END: main *}