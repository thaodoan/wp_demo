{* BEGIN: main *}
<div class="panel nv-block-banners">
	{* BEGIN: loop *}

	{* BEGIN: type_image_link *}
	{if $BANNER.type_image_link}

		<a id="{$BANNER.link_id}" class="{$BANNER.link_class} |" rel="{$BANNER.link_rel}" href="{$BANNER.link}" target="{$BANNER.target}" title="{$BANNER.link_title}">
			<img width="{$BANNER.img_width}" alt="{$BANNER.alt}" src="{$BANNER.src_full}" srcset="{$BANNER.srcset}" sizes="(max-width: 991px) 100vw, 300px" class=" | "/>
		</a>

	{/if}
	{* END: type_image_link *}

	{* BEGIN: type_image *}
	{if $BANNER.type_image}

		<img width="{$BANNER.img_width}" alt="{$BANNER.alt}" src="{$BANNER.src_full}" srcset="{$BANNER.srcset}" sizes="(max-width: 991px) 100vw, 300px" class=" | "/>

	{/if}
	{* END: type_image *}

	{* BEGIN: loop *}
</div>
{* END: main *}