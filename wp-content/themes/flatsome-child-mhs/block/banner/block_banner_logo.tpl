{* BEGIN: main *}
{* BEGIN: loop *}
<div class="item col">
	{* BEGIN: type_image_link *}
	{if $BANNER.type_image_link}

		<a id="{$BANNER.link_id}" class="{$BANNER.link_class} logo logo_with_text hvr-grow " rel="{$BANNER.link_rel}" href="{$BANNER.link}" target="{$BANNER.target}" title="{$BANNER.link_title}">
			<img alt="{$BANNER.alt}" src="{$BANNER.src_full}" class=" | img-responsive"/>
		</a>

	{/if}
	{* END: type_image_link *}

	{* BEGIN: type_image *}
	{if $BANNER.type_image}

		<img alt="{$BANNER.alt}" src="{$BANNER.src_full}" class=" | img-responsive"/>

	{/if}
	{* END: type_image *}
</div>
{* BEGIN: loop *}
{* END: main *}