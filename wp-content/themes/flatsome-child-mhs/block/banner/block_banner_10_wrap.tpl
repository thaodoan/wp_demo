<aside class="widget woocommerce">
    <span class="widget-title shop-sidebar">{$BLOCK_TITLE}</span>
    <div class="is-divider small mb-2"></div>
    {$BLOCK_CONTENT}
</aside>