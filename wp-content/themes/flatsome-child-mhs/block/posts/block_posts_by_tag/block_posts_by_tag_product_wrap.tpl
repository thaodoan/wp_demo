{* BEGIN: main *}
<div class="mt-4">
        <h3 class="product-section-title container-width product-section-title-related pb-half | uppercase display-6 mb-2 font-weight-500 title_bg_line">
                {$BLOCK_TITLE}
        </h3>
        <div>
                <ul class="list_product_posts list-unstyled">
                        {$BLOCK_CONTENT}
                </ul>
        </div>
</div>
{* END: main *}