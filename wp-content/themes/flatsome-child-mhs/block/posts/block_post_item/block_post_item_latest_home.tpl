{* BEGIN: loop *}
<div class="col post-item">
    <div class="col-inner">
        <a href="{$LINK}" title="{$TITLE}" class="plain">
            <div class="box box-text-bottom box-blog-post has-hover">
                <div class="box-image">
                    <div class="image-cover" style="padding-top:75%;">
                        <img width="{$THUMB->img_width}" src="{$THUMB->img_src}" class="attachment-thumbnail size-thumbnail wp-post-image wp-post-image" alt="{$THUMB->img_alt}" srcset="{$THUMB->img_srcset}" sizes="(max-width: 549px) 50vw, {$THUMB->img_width}px">
                    </div>
                </div><!-- .box-image -->

                <div class="box-text text-left">
                    <div class="box-text-inner blog-post-inner">
                        <h5 class="post-title is-large  | text3line">{$TITLE}</h5>
                        <div class="post-meta is-small op-8">{$PUBLISH_DATE}</div>
                        <div class="is-divider"></div>
                        <p class="from_the_blog_excerpt ">{$EXCERPT}</p>
                    </div><!-- .box-text-inner -->
                </div><!-- .box-text -->
            </div><!-- .box -->
        </a><!-- .link -->
    </div><!-- .col-inner -->
</div>
{* BEGIN: loop *}