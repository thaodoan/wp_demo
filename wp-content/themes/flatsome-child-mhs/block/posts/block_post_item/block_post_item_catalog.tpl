{* BEGIN: loop *}
<div class="post-item | mb-4 pb-4 border-bottom border-dashed">
    <div class="col-inner">
        <div class="box box-vertical box-text-bottom box-blog-post has-hover">
            <div class="box-image | align-top" style="width:30%;">
                <div class="image-cover" style="padding-top:56%;">
                    <a href="{$LINK}" title="{$TITLE}" class="plain">
                        <img width="{$THUMB->img_width}" src="{$THUMB->img_src}" class="attachment-medium size-medium wp-post-image wp-post-image" alt="{$THUMB->img_alt}" srcset="{$THUMB->img_srcset}">
                    </a><!-- .link -->
                </div>
            </div><!-- .box-image -->
            <div class="box-text text-left | align-top pb-0 pt-0">
                <div class="box-text-inner blog-post-inner">
                    <h5 >
                        <a href="{$LINK}" class="plain | post-title">
                            {$TITLE}
                        </a><!-- .link -->
                    </h5>
                    <div class="post-date text-size-n1">{$PUBLISH_DATE}</div>
                    <div class="is-divider"></div>
                    <p class="from_the_blog_excerpt ">{$EXCERPT}</p>
                </div><!-- .box-text-inner -->
            </div><!-- .box-text -->
        </div><!-- .box -->
    </div><!-- .col-inner -->
</div>
{* BEGIN: loop *}