<ul class="breadcrumbs_ list-none">
    {* BEGIN: loop *}
    {foreach $BREAD_CRUMBS as $key => $BREAD_CRUMB}
        <li id="brcr_{$key}" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
            <a itemprop="url" href="{$BREAD_CRUMB[1]}">
                <i class="fa fa-angle-double-right "></i><span itemprop="title">{$BREAD_CRUMB[0]}</span>
            </a>
        </li>
    {/foreach}
    {* END: loop *}
</ul>
