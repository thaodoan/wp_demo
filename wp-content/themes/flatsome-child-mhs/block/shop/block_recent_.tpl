{* BEGIN: main *}
{* BEGIN: loop *}
<li class="content-widget-product | ">
    <a href="{$product->link}">
        <img src="{$THUMB->img_src}" width="{$THUMB->img_width}" class="woocommerce-placeholder wp-post-image" alt="{$THUMB->img_alt}">
        <span class="product-title">{$product->title}</span>
    </a>
    <div class="price color-pink font-weight-500">
        {$product->price}
    </div>
</li>
{* BEGIN: loop *}
{* END: main *}
