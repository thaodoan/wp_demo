{extends file="block_product_cat_{$tpl}_tpl.tpl"}

{block name="SUB"}
    {* BEGIN: sub_menu *}
    <div class="dropdown-menu">
        <div class="dropdown-inner">
            <ul class="list-unstyled">
                {* BEGIN: loop *}
                {foreach $SUB_CAT as $SUB}

                    <li class=" || ">
                        <a href="{$SUB->link}" title="{$SUB->name}" class="position-relative | {if $SUB->_current}current-menu-item{/if} ||">
                            {$SUB->name} ({$SUB->count})
                        </a>
                    </li>

                {/foreach}
                {* END: loop *}
            </ul>
        </div>
    </div>
    {* END: sub_menu *}
{/block}