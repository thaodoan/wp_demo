{* BEGIN: main *}

{* BEGIN: top_menu *}

<li class="{if $CAT->_current_parent}mm-active{/if} |  ">
    <a href="{if !$CAT->_has_sub}{$CAT->link}{/if}" class="  | {if $CAT->_current}current-menu-item{/if} {if $CAT->_has_sub}has-arrow{/if}">
        {$CAT->name}
    </a>
    {* BEGIN: sub *}
    {if $CAT->_has_sub}
        {block name="SUB"}{$smarty.block.child}{/block}
    {/if}
    {* END: sub *}
</li>

{* END: top_menu *}

{* END: main *}