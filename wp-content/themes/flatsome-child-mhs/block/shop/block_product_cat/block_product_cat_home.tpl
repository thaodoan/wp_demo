{extends file="block_product_cat_{$tpl}_tpl.tpl"}

{block name="SUB"}
    {* BEGIN: sub_menu *}

            <ul >
                {* BEGIN: loop *}
                {foreach $SUB_CAT as $SUB}

                    <li class=" || ">
                        <a href="{$SUB->link}" title="{$SUB->name}" class=" | {if $SUB->_current}current-menu-item{/if} || ">
                            {$SUB->name} ({$SUB->count})
                        </a>
                    </li>

                {/foreach}
                {* END: loop *}
            </ul>

    {* END: sub_menu *}
{/block}