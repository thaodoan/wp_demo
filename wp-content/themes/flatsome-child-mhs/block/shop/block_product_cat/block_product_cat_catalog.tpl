{extends file="block_product_cat_{$tpl}_tpl.tpl"}

{block name="SUB"}
    {* BEGIN: sub_menu *}
    <ul class="metis_sub | {if $CAT->_current || $CAT->_current_parent}collapse show{/if}">
        {* BEGIN: loop *}
        {foreach $SUB_CAT as $SUB}

            <li class=" || ">
                <a href="{$SUB->link}" title="{$SUB->name}" class="position-relative | {if $SUB->_current}current-menu-item{/if} || ">
                    {$SUB->name} <span class="metis_count position-absolute | color-holder">({$SUB->count})</span>
                </a>
            </li>

        {/foreach}
        {* END: loop *}
    </ul>
    {* END: sub_menu *}
{/block}