{*/**
* The template for displaying product search form
*
* This template can be overridden by copying it to yourtheme/woocommerce/product-searchform.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see     https://docs.woocommerce.com/document/template-structure/
* @package WooCommerce/Templates
* @version 3.3.0
*/*}

{* BEGIN: main *}

<form class="col-md-8" action="{$FORM_ACTION}" method="get" role="search">
    <div class="headerSearch ">
        <div class="input-group">
            {* BEGIN: dropdown cat *}
            {if isset($PRODUCT_CAT)}

                <div class="flex-col search-form-categories">
                    <select class="search_categories resize-select mb-0" name="product_cat">

                        {foreach $PRODUCT_CAT as $CAT}
                            {if $CAT AND !$CAT->parent}
                                {assign var="SELECTED" value="{selected( $CAT->slug, $SELECTED_CAT, false )}"}

                                {* BEGIN: loop *}
                                <option value="{$CAT->slug}" {$SELECTED}>{$CAT->name}</option>
                                {* END: loop *}

                            {/if}
                        {/foreach}

                    </select>
                </div>

            {/if}
            {* END: dropdown cat *}

            {* BEGIN: input_search *}
            <input type="text" value="{$SEARCH_VALUE}" placeholder="{$SEARCH_PLACEHOLDER}" name="s" class="keyword form-control" autocomplete="off">
            {* END: input_search *}

            {* BEGIN: buttton_search *}
            <span class="input-group-btn">
                <button type="submit" id="btn-search-form" class="btn btn-success bg-pink">
                    <i class="fa fa-search"></i>
                </button>
            </span>
            {* END: buttton_search *}

            {* required hidden field*}
            <input type="hidden" name="post_type" value="product" />

            {if isset($ICL_LANGUAGE_CODE)}
                <input type="hidden" name="lang" value="{$ICL_LANGUAGE_CODE}" />
            {/if}
        </div>
    </div>
</form>

{* END: main *}