<div class="row mt-3">
    <div class="col homepage-section">
        {* BEGIN: CAT *}
        <div class="head">
            <h2 class="uppercase text-uppercase">
                <a href="{$CAT->link}">
                    <span class="span1">{$CAT->name}</span>
                </a>
            </h2>
            <a class="float-right font-weight-500 text-underline color-info" href="{$CAT->link}">{$CAT->see_all_text}</a>
        </div>
        {* BEGIN: CAT *}

        {* BEGIN: PRODUCTS *}
        <div class="panel-custom row">
            <div class="flex-row panel-body">
                {$LIST_PRODUCT}
            </div>
        </div>
        {* END: PRODUCTS *}
    </div>
</div>