<div class="block_product flex-row-items col-6 col-sm-4 col-md-4 col-lg-4">
	<div class="product-box">
		<div class="product-thumbnail">
			<a class="image_link" href="{$LINK}" title="{$TITLE}">
				<img width="{$THUMB->img_width}" src="{$THUMB->img_src}" srcset="{$THUMB->img_srcset}" sizes="(max-width: 576px) 50vw, 270px" alt="{$THUMB->img_alt}" title="{$TITLE}" class="" />
			</a>
		</div>

		<div class="product-info effect a-left">
			<div class="info_hhh">
				<h3 class="product-name product-name-hover text2line text-center">
					<a href="{$LINK}">{$TITLE}</a>
				</h3>
				<div class="price-box clearfix text-center">
                    <span class="price product-price color-pink">
                        {$PRICE}
                    </span>

					{do_shortcode('[block_sale_flash]')}

				</div>
			</div>
		</div>

		<div class="button-group text-center m-button d-none">
			{* button Wishlist*}
			{do_shortcode('[yith_wcwl_add_to_wishlist]')}

			{* button Add to Cart*}
			{do_shortcode('[block_addtocart_btn tpl="home"]')}
		</div>
	</div>
</div>
