{extends file="block_menu_{$tpl}_tpl.tpl"}

{block name="SUB"}
	{* BEGIN: sub_menu *}
	<ul class="metis_sub | {if $TOP_MENU->_current || $TOP_MENU->_current_parent}collapse show{/if}">
		{* BEGIN: loop *}
		{foreach $SUB_MENU as $SUB}

			<li class="{$SUB->classes} || ">
				<a href="{$SUB->url}" title="{$SUB->description}" class="{$SUB->classes} || ">
					{$SUB->title}
				</a>
			</li>

		{/foreach}
		{* END: loop *}
	</ul>
	{* END: sub_menu *}
{/block}