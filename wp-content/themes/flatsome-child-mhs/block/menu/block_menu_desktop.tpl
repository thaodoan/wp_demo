{extends file="block_menu_desktop_tpl.tpl"}

{block name="TOP_MENU"}

{/block}

{block name="SUB"}
    {* BEGIN: sub_menu *}
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
        {* BEGIN: loop *}
        {foreach $SUB_MENU as $SUB}

                <a href="{$SUB->url}" title="{$SUB->description}" class="{$SUB->classes} || dropdown-item">
                    {$SUB->title}
                </a>

        {/foreach}
        {* END: loop *}
    </div>
    {* END: sub_menu *}
{/block}