{* BEGIN: main *}
{* BEGIN: top_menu *}
<li class="{if $TOP_MENU->_has_sub}dropdown{/if}">

    {block name="TOP_MENU"}{$smarty.block.child}{/block}

    {* BEGIN: _has_sub *}
    {if $TOP_MENU->_has_sub}
        {block name="SUB"}{$smarty.block.child}{/block}
    {/if}
    {* END: _has_sub *}

</li>
{* END: top_menu *}
{* END: main *}