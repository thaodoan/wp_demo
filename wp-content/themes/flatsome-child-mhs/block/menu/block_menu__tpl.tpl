{* BEGIN: main *}
{* BEGIN: top_menu *}
<li class="nav-item {if $TOP_MENU->_has_sub}has_sub{/if} ">

	{block name="TOP_MENU"}{$smarty.block.child}{/block}

	{* BEGIN: sub *}
	{if $TOP_MENU->_has_sub}
		{block name="SUB"}{$smarty.block.child}{/block}
	{/if}
	{* END: sub *}

</li>
{* END: top_menu *}
{* END: main *}