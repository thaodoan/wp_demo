{* BEGIN: main *}
{* BEGIN: top_menu *}
<li class="{if $TOP_MENU->_current_parent}active{/if}">
	<a class=" || {$TOP_MENU->classes} ||" href="{$TOP_MENU->url}" title="{$TOP_MENU->description}" >
		{$TOP_MENU->title}
	</a>
	{if $TOP_MENU->_has_sub}
		<span class="fa arrow expand"></span>
	{/if}

	{* BEGIN: sub *}
	{if $TOP_MENU->_has_sub}
		{block name="SUB"}{$smarty.block.child}{/block}
	{/if}
	{* END: sub *}
</li>
{* END: top_menu *}
{* END: main *}