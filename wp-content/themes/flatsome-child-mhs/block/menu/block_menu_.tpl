{extends file="block_menu__tpl.tpl"}

{block name="TOP_MENU"}
	{* BEGIN: top_menu *}
	<a class="nav-link {if $TOP_MENU->_current}active{/if} || {$TOP_MENU->classes} ||" href="{$TOP_MENU->url}" title="{$TOP_MENU->description}" >
		{$TOP_MENU->title}
	</a>
	{* END: top_menu *}
{/block}

{block name="SUB"}
	{* BEGIN: sub_menu *}
	<ul>
		{* BEGIN: loop *}
		{foreach $SUB_MENU as $SUB}

			<li class="{$SUB->classes} || ">
				<a href="{$SUB->url}" title="{$SUB->description}" class=" || ">
					{$SUB->title}
				</a>
			</li>

		{/foreach}
		{* END: loop *}
	</ul>
	{* END: sub_menu *}
{/block}