{* BEGIN: main *}
<li class="nav-item {if $TOP_MENU->_has_sub}dropdown{/if} {if $TOP_MENU->_current_parent || $TOP_MENU->_current}active{/if} ">
	{* BEGIN: top_menu *}
	<a class=" || nav-link {if $TOP_MENU->_has_sub}dropdown-toggle{/if}" href="{$TOP_MENU->url}" title="{$TOP_MENU->description}" {if $TOP_MENU->_has_sub}role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"{/if}>
		<em class="{$TOP_MENU->classes}">&nbsp;</em> {$TOP_MENU->title}
	</a>
	{* END: top_menu *}

	{* BEGIN: _has_sub *}
	{if $TOP_MENU->_has_sub}
		{block name="SUB"}{$smarty.block.child}{/block}
	{/if}
	{* END: _has_sub *}
</li>
{* END: main *}