<h5 class="">
    {$BLOCK_TITLE}
    <button type="button" class="btn btn-primary toggle collapsed" data-toggle="collapse" data-target="#aboutf"></button>
</h5>
<div id="aboutf" class="collapse footer-collapse">
    <ul class="list-unstyled menu">
        {$BLOCK_CONTENT}
    </ul>
</div>