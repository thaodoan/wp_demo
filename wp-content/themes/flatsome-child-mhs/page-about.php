<?php
/*
Template name: MHS - Flatsome Child - Gioi Thieu
*/
echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
echo do_shortcode('[header_extend]');
?>

<div class="fix-more-padding row">

<aside id="column-left" class="col-md-3 col-sm-4 col-xs-12 hidden-xs">
    <?php get_template_part('layout/body', 'left'); ?>
</aside>

<div id="content" class="col-md-9 col-sm-8 page-contact">
    <div class="bg text-center" >
        <img src="<?= get_the_post_thumbnail_url(); ?>" alt="<?= get_the_title(); ?>">
    </div>
    <div class="entry-content single-page">
        <?php
        // TO SHOW THE PAGE CONTENTS
        while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
            <?php the_content(); ?> <!-- Page Content -->
        <?php
        endwhile; //resetting the page loop
        wp_reset_query(); //resetting the page query
        ?>
    </div>
</div>

</div>

<?php
echo do_shortcode('[footer_extend]');
echo do_shortcode('[get_footer]');
echo do_shortcode('[wrap_end]');
?>

