<?php
/*
Template name: MHS - Flatsome child - WooCommerce - Cart
*/
echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
echo do_shortcode('[header_extend]');

wc_get_template_part( 'cart/layouts/cart', 'index' );

echo do_shortcode('[footer_extend]');
echo do_shortcode('[get_footer]');
echo do_shortcode('[wrap_end]');
?>