<?php if(IS_FRONT_PAGE || is_product()):?>
<div class="clearfix"></div>
<div class="unselectable" >
    <h3 class="text-center galery-title">Hình ảnh hoạt động</h3>
    <div class="galery" >
        <?= do_shortcode('[block_banner id="12" num="8" tpl="hoat_dong" img_size="woocommerce_thumbnail"]'); ?>
    </div>
</div>
<?php endif; ?>

</div>
</section>

<footer class="footer ">
    <div class="row">
        <div class="homepage-section footer-session col-12 col-md-4 col-lg-4">
            <div class="head clearfix">
                <h4 class="">
                    <span class="span1">Giới thiệu <?= do_shortcode('[cgv company_name]'); ?></span>
                </h4>
            </div>
            <div class="card panel-custom">
                <div class="panel-body">
                    <p>
                        <?= do_shortcode('[block id="footer-gioi-thieu-1"]')?>
                    </p>
                </div>
            </div>
            <div class="card panel-custom">
                <div class="panel-body">
                    <p>
                        <?= do_shortcode('[block id="footer-gioi-thieu-2"]')?>
                    </p>
                </div>
            </div>
        </div>
        <div class="homepage-section footer-session col-12 col-md-4 col-lg-4">
            <div class="head clearfix">
                <h4 class="">
                    <span class="span1">Thông tin liên hệ</span>
                </h4>
            </div>

            <div class="card panel-custom">
                <div class="panel-body">Hotline:
                    <a href="tel:<?= do_shortcode('[cgv phone]')?>">
                        <span class="fw_bold color-pink"><em class="fa fa-phone"></em>&nbsp;<?= do_shortcode('[cgv phone_format]')?></span>
                    </a>
                </div>
            </div>

            <div class="card panel-custom">
                <div class="panel-body"><em class="fa fa-envelope"></em>  Email: <a href="mailto:<?= do_shortcode('[cgv email]')?>"><?= do_shortcode('[cgv email]')?></a></div>
            </div>

            <div class="card panel-custom">
                <div class="panel-body">
                    <a class="pointer" rel="noopener noreferrer" data-toggle="modal" data-target="#company-map-modal-75">
                        <em class="fa fa-map-marker"></em>&nbsp;Địa chỉ: <span class="company-address"><?= do_shortcode('[cgv company_address]')?></span>
                    </a>
                </div>
            </div>

            <?= do_shortcode('[block id="1283-2"]')?>
        </div>
        <div class="homepage-section footer-session col-12 col-md-4 col-lg-4">
            <div class="head clearfix">
                <h4 class="">
                    <span class="span1"><?= do_shortcode('[cgv company_name]'); ?>&nbsp;Facebook</span>
                </h4>
            </div>
            <div class="card">
                <div class="panel-body">
                    <div id="fbPage" class="centerTj"> <div id="fb-page" class="fb-page" data-href="https://www.facebook.com/337900329688370" data-small-header="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"></div></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>

<div id="go-top" class="back-to-top">
    <i class="fa fa-arrow-circle-up"></i>
</div>