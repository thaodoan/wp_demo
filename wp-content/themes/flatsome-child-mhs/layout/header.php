<!DOCTYPE html>
<!--[if IE 9 ]> <html <?php language_attributes(); ?> class="ie9 <?php flatsome_html_classes(); ?>"> <![endif]-->
<!--[if IE 8 ]> <html <?php language_attributes(); ?> class="ie8 <?php flatsome_html_classes(); ?>"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="<?php flatsome_html_classes(); ?>"> <!--<![endif]-->
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />

    <meta name="google-site-verification" content="n5Kh6ur7n4l79pvpiYEU1yLAqd4AQBBb0OUr3BfEFNc" />

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

    <?php wp_head(); ?>
</head>

<body class="<?= join( ' ', get_body_class( '' ) ); ?> | " >

<div id="main-message">
    <div class="message-body"></div>
</div>
<?php do_action( 'flatsome_after_body_open' ); ?>
<?php wp_body_open(); ?>