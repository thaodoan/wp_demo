<div id="fb-root"></div>
<div id="statics-top" class="">
    <div class="header-nav" >
        <div class="container" >

            <span id="aaa">
                <?php echo get_theme_mod( 'header_text' ); ?>
            </span>

            <?= do_shortcode('[block_logo tpl="mobile"]'); ?>

            <div class="social-icons">
                <div id="socialList" class="content">
                    <ul class="socialList">
                        <?= do_shortcode("[block_menu location='social-icon-1' tpl='social']")?>
                    </ul>
                </div>
            </div>

            <div class="header_phone unselectable" >
                <span class="">
                    <i class="fa fa-phone faa-shake animated " > </i>
                    <a href="tel:<?= do_shortcode("[cgv phone]")?>">
                        <?= do_shortcode("[cgv phone_format]")?>
                    </a>
                </span>
            </div>

            <div id="tip" data-content="">
                <div class="bg"></div>
            </div>
            <div class="index-cart">
                <?= do_shortcode('[block_cart_icon]'); ?>
            </div>
        </div>
    </div>
</div>

<div id="header">
    <div class="co-2 col-sm-3 col-md-3 col-lg-3">
        <div class="logo hidden-xs" data-aos="zoom-in" data-aos-duration="1000">
            <?= do_shortcode('[block_logo]'); ?>
        </div>
    </div>
    <div class="co-1 col-md-9 col-lg-9 hidden-xs">
        <div class="row">
            <?= do_shortcode('[block_product_search]'); ?>

            <div class="col-md-4">
                <div class="index-cart">
                    <?= do_shortcode('[block_cart_icon]'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-dark" id="menusite">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars"></i>
        <i class="fa fa-times"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <?= do_shortcode("[block_menu location='primary' tpl='desktop' cache=0]"); ?>
    </div>
</nav>

<section>
    <div id="body" class="">
        <nav class="third-nav">
            <div class="bg">
                <div class="clearfix">
                    <div class="breadcrumbs-wrap">
                        <div class="display">
                            <?= do_shortcode('[block_breadcrumb]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </nav>