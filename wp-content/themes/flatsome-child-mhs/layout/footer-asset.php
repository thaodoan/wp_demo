

<!-- BEGIN: themes assets-->
<script type="text/javascript" src="<?= ASSETS_PATH . '/themes/' ?>main.js"></script>
<script src="<?= ASSETS_PATH . '/themes/' ?>aos.js"></script>
<script>
    AOS.init();
</script>

<?php if(IS_FRONT_PAGE): ?>
    <script src="<?= ASSETS_PATH . '/themes/' ?>jssor.slider-26.9.0.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {
            var jssor_1_SlideshowTransitions = [
                {$Duration:800,$Delay:12,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$SlideOut:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2049,$Easing:$Jease$.$OutQuad},
                {$Duration:800,$Delay:40,$Cols:10,$Rows:5,$Opacity:2,$Clip:15,$SlideOut:true,$Easing:$Jease$.$OutQuad}
            ];
            var jssor_1_options = {
                $AutoPlay: 1,
                $Cols: 1,
                $Align: 0,
                $SlideshowOptions: {
                    $Class: $JssorSlideshowRunner$,
                    $Transitions: jssor_1_SlideshowTransitions,
                    $TransitionsOrder: 1
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            var MAX_WIDTH = 885;
            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        };
    </script>
    <script type="text/javascript">jssor_1_slider_init();</script>
<?php endif; ?>

<?php if(IS_FRONT_PAGE || is_product()):?>
<script type="text/javascript" src="<?= ASSETS_PATH . '/themes/' ?>slick.min.js?t=165"></script>
<script type="text/javascript">
    (function ($) {
        $('.galery').slick({
            rows: 2,
            slidesPerRow: 4,
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 10000,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        rows: 2,
                        slidesPerRow: 3,
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        rows: 2,
                        slidesPerRow: 2,
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        rows: 2,
                        slidesPerRow: 1,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    })(jQuery);
</script>
<?php endif; ?>

<script src="<?= LIBS_PATH . '/jquery/' ?>jquery.validate.min.js"></script>
<script src="<?= LIBS_PATH . '/bootstrap/' ?>bootstrap.min.js"></script>
<script src="<?= ASSETS_PATH . '/themes/' ?>jquery.goTop.js"></script>
<!-- END: themes assets-->

<!-- BEGIN: default assets-->

<script type="text/javascript" src="<?= LIBS_PATH . '/metismenu/' ?>jquery.metismenu.js"></script>
<script type="text/javascript">

    (function ($) {
        $('#menu_97').metisMenu({
            toggle: false
        });
    })(jQuery);

</script>

<script src="<?= LIBS_PATH . '/bootstrap/popper.min.js' ?>"></script>
<style type="text/css">
    body{
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
    }
</style>

<link defer rel='stylesheet' href="<?= LIBS_PATH . '/font-awesome/css/' ?>font-awesome.css" type='text/css' media='all' />
<link rel='stylesheet' href="<?= LIBS_PATH.'/hover-css/css/hover-min.css'; ?>" type='text/css' media='all' />

<!-- END: default assets-->

<!-- EMBED -->
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = '//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=591476614353655&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>var wFb= document.getElementById("fbPage").clientWidth;document.getElementById("fb-page").setAttribute("data-width",wFb); </script>


<div class="chat-now "></div>
<div class="fb-customerchat"
     theme_color="#6699cc"
     attribution=setup_tool
     page_id="<?= do_shortcode('[cgv fb_page_id]'); ?>"
     logged_in_greeting="<?= do_shortcode('[cgv logged_in_greeting]'); ?>"
     logged_out_greeting="<?= do_shortcode('[cgv logged_out_greeting]'); ?>"
     greeting_dialog_delay="15"
     greeting_dialog_display="fade"
>
</div>

<script>
    /*setTimeout(function() {
        console.log('showDialog');
        //FB.CustomerChat.showDialog();
    }, 15000);*/
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>