<!-- default css -->
<link rel="StyleSheet" href="<?= CSS_PATH.'/flatsome.css'; ?>" type='text/css' media='all' />

<link rel="StyleSheet" href="<?= CSS_PATH.'/flatsome-shop.css'; ?>" type='text/css' media='all' />

<!-- default css -->

<!-- BEGIN: themes css -->

<link rel="preload" as="script" href="<?= ASSETS_PATH . '/themes/' ?>aos.js">
<link href="<?= ASSETS_PATH . '/themes/' ?>aos.css" rel="stylesheet">

<?php if(IS_FRONT_PAGE):?>
    <link rel="preload" as="script" href="<?= ASSETS_PATH . '/themes/' ?>jssor.slider-26.9.0.min.js">
<?php endif; ?>

<link rel="preload" as="script" href="<?= LIBS_PATH . '/metismenu/' ?>jquery.metismenu.css">
<link rel="preload" as="script" href="<?= LIBS_PATH . '/bootstrap/' ?>bootstrap.min.js">
<link rel="preload" as="script" href="<?= ASSETS_PATH . '/themes/' ?>jquery.goTop.js">

<link rel='stylesheet' href="<?= LIBS_PATH.'/bootstrap/bootstrap.min.css'; ?>" type='text/css' media='all' />

<link rel="StyleSheet" href="<?= ASSETS_PATH . '/themes/' ?>style.css">
<link rel="StyleSheet" href="<?= ASSETS_PATH . '/themes/' ?>style.responsive.css">
<link rel="StyleSheet" href="<?= ASSETS_PATH . '/themes/' ?>custom.css">
<link rel="StyleSheet" href="<?= ASSETS_PATH . '/themes/' ?>pro_group_temp.css">
<link rel="StyleSheet" href="<?= ASSETS_PATH . '/themes/' ?>theme.css">
<link rel="StyleSheet" href="<?= ASSETS_PATH . '/themes/' ?>res_img.css">
<link rel="StyleSheet" href="<?= ASSETS_PATH . '/themes/' ?>header_temp.css">

<?php if(IS_FRONT_PAGE):?>
    <link rel="stylesheet" type="text/css" media="screen" href="<?= ASSETS_PATH . '/themes/' ?>jssor.css" />
<?php endif; ?>

<?php if(IS_FRONT_PAGE || is_product()):?>
    <link rel="preload" as="font" crossorigin type="font/woff" href="<?= ASSETS_PATH . '/themes/fonts/' ?>slick.woff">
    <link rel="StyleSheet" href="<?= ASSETS_PATH . '/themes/' ?>slick.css">
    <link rel="StyleSheet" href="<?= ASSETS_PATH . '/themes/' ?>slick-theme.css">
<?php endif; ?>

<link rel="stylesheet" type="text/css"	href="<?= LIBS_PATH . '/metismenu/' ?>jquery.metismenu.css" />

<style>#fbPage {width:100%; margin:auto;background-size: cover;} .centerTj {display:-ms-flexbox;-ms-flex-pack:center;-ms-flex-align:center; display:-moz-box;-moz-box-pack:center;-moz-box-align:center; display:-webkit-box;-webkit-box-pack:center;-webkit-box-align:center; display:box;box-pack:center;box-align:center; }</style>
<!--END: themes css-->

<!-- default css -->

<link rel="StyleSheet" href="<?= CSS_PATH . '/' ?>mm-vertical.css">
<link rel='stylesheet' href="<?= CSS_PATH.'/themes_reset.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/shop_reset.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/news.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/news_reset.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/wishlist_reset.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/custom.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/assets.css'; ?>" type='text/css' media='all' />

<?php if(IS_CART || IS_CHECKOUT):?>
    <link rel='stylesheet' href="<?= CSS_PATH.'/cart_reset.css'; ?>" type='text/css' media='all' />
    <link rel='stylesheet' href="<?= CSS_PATH.'/checkout_reset.css'; ?>" type='text/css' media='all' />
<?php endif; ?>

<!-- default css -->
