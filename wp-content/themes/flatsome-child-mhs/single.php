<?php
/**
 * The blog template file.
 *
 * @package flatsome
 */

echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
echo do_shortcode('[header_extend]');

?>

<div class="fix-more-padding row">
    <div class="co-1 col-lg-9 order-lg-2">
        <div id="content">
            <?php get_template_part( 'template-parts/posts/layout', get_theme_mod('blog_post_layout','right-sidebar') ); ?>
        </div>
    </div>

    <div class="co-2 col-lg-3 order-lg-1 mt-4 mt-lg-0">
        <?php get_template_part('layout/body', 'left'); ?>
    </div><!-- #content .page-wrapper -->
</div>

<?php

echo do_shortcode('[footer_extend]');
echo do_shortcode('[get_footer]');
echo do_shortcode('[wrap_end]');

