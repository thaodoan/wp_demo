<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package storefront
 */

wp_register_style( 'checkout_reset', CSS_PATH.'/checkout_reset.css');
wp_enqueue_style( 'checkout_reset');

?>

<div class="cart-container col">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php if(!is_wc_endpoint_url()):?>
        <div class="d-flex">
            <?php wc_get_template_part('cart/layouts/cart','stepbar')?>
        </div>
        <?php endif; ?>
        <?php echo do_shortcode('[woocommerce_checkout]'); ?>
    </article><!-- #post-## -->
</div>
