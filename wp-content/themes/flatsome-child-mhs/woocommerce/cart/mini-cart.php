<?php
/**
 * Mini-cart
 */
defined( 'ABSPATH' ) || exit;
?>

<?php if ( ! WC()->cart->is_empty() ) : ?>

    <?php
    /**
     * <--  BEGIN: cart container  -->
     */?>
    <div class="mkdf-sc-dropdown-items">
        <?php
        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
            $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
            $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

            if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                $product_name      = apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key );
                $thumbnail         = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );
                $product_price     = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
                $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );

                $remove_href = esc_url( wc_get_cart_remove_url( $cart_item_key ) );
                $remove_product_sku = $_product->get_sku();
                $remove_label = esc_html__( 'Remove this item', 'woocommerce' );
                ?>
                <?php
                /**
                 * <--  BEGIN: items  -->
                 */?>
                <div class="mkdf-sc-dropdown-item">
                    <?php
                    /**
                     * <--  BEGIN: thumbnail  -->
                     */
                    ?>
                    <div class="mkdf-sc-dropdown-item-image">
                        <a href="<?= esc_url( $product_permalink ); ?>">
                            <?= $thumbnail ?>
                        </a>
                    </div>
                    <div class="mkdf-sc-dropdown-item-content">
                        <h5 itemprop="name" class="mkdf-sc-dropdown-item-title entry-title">
                            <!-- cart_title-->
                            <a href="<?= esc_url( $product_permalink ); ?>">
                                <?= $product_name ?>
                            </a>
                        </h5>

                        <!--  BEGIN quantity * price -->
                        <span class="mkdf-sc-dropdown-item-price">
                            <?= $product_price; ?>
                        </span>
                        <span> × </span>
                        <span class="mkdf-sc-dropdown-item-quantity">
                            <?= $cart_item['quantity']; ?>
                        </span>
                        <!--
                        BEGIN: remove button
                        Ajax button class: remove remove_from_cart_button
                        -->
                        <a href="<?= $remove_href; ?>" class="remove remove_from_cart_button mkdf-sc-dropdown-item-remove" aria-label="<?= $remove_label; ?>" data-product_id="<?= $product_id ?>" data-cart_item_key="<?= $cart_item_key ?>" data-product_sku="<?= $remove_product_sku ?>">
                            <i class="fa fa-times"></i>
                        </a>

                        <div class="minicart_variation">
                            <!-- Variation Product -->
                            <?php echo wc_get_formatted_cart_item_data( $cart_item ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
                        </div>
                    </div>
                </div>
                <?php
            }
        }

        ?>
    </div>

    <?php
    /**
     * <--  Begin: Cart total -->
     */
    ?>
    <div class="mkdf-sc-dropdown-subtotal">
        <h6 class="mkdf-sc-dropdown-total"><?= esc_html__( 'Subtotal', 'woocommerce' ); ?>:</h6>
        <span class="mkdf-sc-dropdown-total-amount"><?= WC()->cart->get_cart_subtotal(); ?></span>
    </div>

    <!--Begin: view Cart button-->
    <div class="mkdf-sc-dropdown-button-holder">
        <a itemprop="url" href="<?= esc_url( wc_get_cart_url()); ?>" class="mkdf-sc-dropdown-button mkdf-btn mkdf-btn-medium mkdf-btn-simple btn btn-success">
            <span class="mkdf-btn-text"><i class="fa fa-shopping-cart"></i> <?= esc_html__( 'Cart', 'woocommerce' ); ?></span>
        </a>
        <a itemprop="url" href="<?= esc_url( wc_get_checkout_url() ); ?>" class="mkdf-sc-dropdown-button mkdf-btn mkdf-btn-medium mkdf-btn-simple btn btn-success">
            <span class="mkdf-btn-text"><i class="fa fa-share"></i> <?= esc_html__( 'Checkout', 'woocommerce' ); ?></span>
        </a>
    </div>

<?php
/**
 * <--  Begin: empty Message -->
 */
else :?>
    <p class="woocommerce-mini-cart__empty-message"><?php esc_html_e( 'No products in the cart.', 'woocommerce' ); ?></p>

<?php endif; ?>

<?php //do_action( 'woocommerce_after_mini_cart' ); ?>
