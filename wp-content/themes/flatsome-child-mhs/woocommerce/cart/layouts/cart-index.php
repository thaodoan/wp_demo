<?php
/**
 * The template used for displaying page content in Cart page
 */

?>
<div class="cart-container col">
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="d-flex">
            <?php wc_get_template_part('cart/layouts/cart','stepbar')?>
        </div>

        <div class="clearfix"></div>

        <div class="header-cart border d-block d-lg-none mb-3">
            <div class="title-cart">
                <h3 class="text-uppercase mb-1 mt-0"><?= __('Your Cart', 'woocommerce'); ?></h3>
                <span>(
                <a class="cart-contents">
                    <span class="count">
                        <?= WC()->cart->get_cart_contents_count(); ?>
                    </span>
                </a>
                <?= mb_strtolower(__('Product', 'woocommerce')); ?>
                 )</span>
            </div>
        </div>

        <?php echo do_shortcode('[woocommerce_cart]'); ?>
    </article><!-- #post-## -->
</div>

