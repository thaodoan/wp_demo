<div class="step_bar alert alert-default clearfix py-2 px-4">
    <a class="step <?= IS_CART?'step_current':'step_disable color-holder'; ?> " title="<?= __('Check Cart', 'woocommerce')?>" href="<?= wc_get_cart_url(); ?>"><span>1. <?= __('Cart', 'woocommerce')?></span></a>
    <i class="fa fa-angle-double-right"></i>
    <a class="step <?= IS_CHECKOUT?'step_current':'step_disable color-holder'; ?> " title="<?= __('Payments', 'woocommerce'); ?>" href="<?= wc_get_checkout_url(); ?>" id="cart_next"><span>2. <?= __('Payments', 'woocommerce'); ?></span></a>
    <i class="fa fa-angle-double-right"></i>
    <a class="step <?= is_wc_endpoint_url('order-received')?'step_current':'step_disable color-holder';?> " title="<?= __('Order complete', 'woocommerce'); ?>" href="javascript:void(0);" id="cart_next"><span>3. <?= __('Order complete', 'woocommerce'); ?></span></a>
</div>
