<?php
/**
 * Result Count
 *
 * Shows text: Showing x - x of x results.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/result-count.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>

<div class="woocommerce-result-count | align-self-center mx-0 my-0">
    <h1 class="text-size-3 d-inline-block mx-0 my-0 w-auto color-pink"><?php woocommerce_page_title(); ?></h1>
</div>

<div class="mt-4">
    <h4 class="h5">
        <?= __('Product', 'woocommerce').' (' . $total . ')'; ?>
    </h4>
    <div class="is-divider small"></div>
</div>
