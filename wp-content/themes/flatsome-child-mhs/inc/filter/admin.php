<?php
/**
 * ADMIN css
 * */
function customAdmin(){
    $url = CSS_PATH . '/wp-admin.css';
    echo '<!-- custom admin css -->
          <link rel="stylesheet" type="text/css" href="' . $url . '" />
          <!-- /end custom adming css -->';
}

add_action('admin_head', 'customAdmin');


/**
 * clear Smarty cache onclick WP-Rocket clear button
 * */
function do_clearSmartyCache(){
    if(is_super_admin()){
        $list_path = [ASSETS_DIR.'/cache/templates_c', ASSETS_DIR.'/cache/template_ca'];
        foreach ($list_path as $path) {
            chmod($path, 0777);
            $files = glob($path.'/{,.}*', GLOB_BRACE);
            foreach($files as $index => $file){ // iterate files
                if(is_file($file)){
                    unlink($file);
                }
            }
        }
    }
}

if(isset($_GET['action']) && $_GET['action'] === 'purge_cache'){
    add_action( 'after_rocket_clean_domain', 'do_clearSmartyCache' );
}

/**
 * Gutenbert block editor
 * */
/*if(!wp_doing_ajax()){
    add_filter( 'use_block_editor_for_post', '__return_true' );
}*/

