<?php
/**
 *  Change text for Wishlist
 */
function yith_wcwl_product_removed_text_custom( $message ){
    $message = __( 'Product successfully removed.', 'woocommerce' );
    return $message;
}
add_filter( 'yith_wcwl_product_removed_text', 'yith_wcwl_product_removed_text_custom', 10, 1 );

function yith_wcwl_no_product_to_remove_message_custom( $message ){
    $message = __( 'No products were added to the wishlist', 'woocommerce' );
    return $message;
}
add_filter( 'yith_wcwl_no_product_to_remove_message', 'yith_wcwl_no_product_to_remove_message_custom', 10, 1 );

/**
 * update wishlist counter
 * */
if( defined( 'YITH_WCWL' ) && ! function_exists( 'yith_wcwl_ajax_update_count' ) ){
    function yith_wcwl_ajax_update_count(){
        wp_send_json( array(
            'count' => yith_wcwl_count_all_products()
        ) );
    }
    add_action( 'wp_ajax_yith_wcwl_update_wishlist_count', 'yith_wcwl_ajax_update_count' );
    add_action( 'wp_ajax_nopriv_yith_wcwl_update_wishlist_count', 'yith_wcwl_ajax_update_count' );
}

function yith_wcwl_product_added_to_wishlist_message_custom($message){
    $message = '<i class="fa fa-check-circle color-green"></i>' . get_option( 'yith_wcwl_product_added_text' );
    return $message;
}
add_filter( 'yith_wcwl_product_added_to_wishlist_message', 'yith_wcwl_product_added_to_wishlist_message_custom', 9999);
