<?php
function themeslug_customize_register( $wp_customize ) {
    //Hompage text
    $wp_customize->add_section("homepage_text", array(
        'title' => __("Homepage Text", "devvn"),
        'priority' => 130,
        'description' => __( 'Description Custom footer here' ),
    ));

    //Footer text
    $wp_customize->add_setting("banner_text", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"footer_text", array(
        'label' => __("Footer text here", "devvn"),
        'section' => 'homepage_text',
        'settings' => 'banner_text',
        'type' => 'textarea',
    )));

    //Header text
    $wp_customize->add_setting("header_text_setting", array(
        'default' => '',
        'transport' => 'postMessage',
    ));
    $wp_customize->add_control(new WP_Customize_Control($wp_customize,"header_text", array(
        'label' => __("Header text here", "devvn"),
        'section' => 'homepage_text',
        'settings' => 'header_text_setting',
        'type' => 'select',
        'choices' => array(
            '<a href="https://thachpham.com/hosting-domain/danh-gia-a2hosting-dich-vu-hosting-toc-do-tot.html" data-wpel-link="internal">a2hosting</a>' => 'AAA',
            '<a href="https://thachpham.com/nen-dung/stablehost" data-wpel-link="internal">stablehost</a>' => 'BBB',
            'nuclear-energy' => 'Nuclear Energy',
            '<a href="https://thachpham.com/nen-dung/wpengine" data-wpel-link="internal">wpengine</a>' => 'CCC',
            '<a href="https://thachpham.com/nen-dung/wpengines" data-wpel-link="internal">wpengine</a>' => 'CCC',
        ),
    )));

    $wp_customize->selective_refresh->add_partial( 'icon_edit_header_text', array(
        'selector' => '.headerSearch',
        'settings' => array('header_text_setting'),
        'render_callback' => function(){

        },
    ));
}
add_action( 'customize_register', 'themeslug_customize_register' );