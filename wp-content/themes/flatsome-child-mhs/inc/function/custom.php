<?php
function get_nav_menu_item_children( $parent_id, $nav_menu_items, $depth = true ) {
    $nav_menu_item_list = array();
    foreach ( (array) $nav_menu_items as $nav_menu_item ) {
        if ( $nav_menu_item->menu_item_parent == $parent_id ) {
            $nav_menu_item_list[] = $nav_menu_item;
            if ( $depth ) {
                if ( $children = get_nav_menu_item_children( $nav_menu_item->ID, $nav_menu_items ) )
                    $nav_menu_item_list = array_merge( $nav_menu_item_list, $children );
            }
        }
    }
    return $nav_menu_item_list;
}

function get_product_category($product_id = 0, $num_cat = 10){
    /**
     * Go:: block_category_to_storefront_product_categories
     * */
    $orderby = 'menu_order';
    $order = 'asc';
    $hide_empty = false ;
    $cat_args = array(
        'orderby'    => $orderby,
        'order'      => $order,
        'hide_empty' => $hide_empty,
        'number' => $num_cat,
        'parent' => $product_id,
    );

    return get_terms( 'product_cat', $cat_args );
}

/**
 * Get a coupon label.
 *
 * @param string|WC_Coupon $coupon Coupon data or code.
 * @param bool             $echo   Echo or return.
 *
 * @return string
 */
function wc_cart_totals_coupon_label_custom( $coupon, $echo = true ) {
    if ( is_string( $coupon ) ) {
        $coupon = new WC_Coupon( $coupon );
    }

    /* translators: %s: coupon code */
    $label = __( 'Coupon', 'woocommerce' ).' : ';
    $label .= '<span class="code text-uppercase">'.$coupon->get_code().'</span>';
    $label .= ' <a href="' . esc_url( add_query_arg( 'remove_coupon', rawurlencode( $coupon->get_code() ), defined( 'WOOCOMMERCE_CHECKOUT' ) ? wc_get_checkout_url() : wc_get_cart_url() ) ) . '" title="'. __('Remove','woocommerce') .'" class="woocommerce-remove-coupon" data-coupon="' . esc_attr( $coupon->get_code() ) . '"><i class="fa fa-times-circle"></i></a>';


    if ( $echo ) {
        echo $label; // WPCS: XSS ok.
    } else {
        return $label;
    }
}

/**
 * Get coupon display HTML.
 *
 * @param string|WC_Coupon $coupon Coupon data or code.
 */
function wc_cart_totals_coupon_html_custom( $coupon ) {
    if ( is_string( $coupon ) ) {
        $coupon = new WC_Coupon( $coupon );
    }

    $discount_amount_html = '';

    $amount               = WC()->cart->get_coupon_discount_amount( $coupon->get_code(), WC()->cart->display_cart_ex_tax );
    $discount_amount_html = '-' . wc_price( $amount );

    if ( $coupon->get_free_shipping() && empty( $amount ) ) {
        $discount_amount_html = __( 'Free shipping coupon', 'woocommerce' );
    }

    $discount_amount_html = apply_filters( 'woocommerce_coupon_discount_amount_html', $discount_amount_html, $coupon );
    $coupon_html = $discount_amount_html;
    //$coupon_html .= ' <a href="' . esc_url( add_query_arg( 'remove_coupon', rawurlencode( $coupon->get_code() ), defined( 'WOOCOMMERCE_CHECKOUT' ) ? wc_get_checkout_url() : wc_get_cart_url() ) ) . '" title="'. __('Remove','woocommerce') .'" class="woocommerce-remove-coupon" data-coupon="' . esc_attr( $coupon->get_code() ) . '"><i class="fa fa-times-circle"></i></a>';
    echo wp_kses( apply_filters( 'woocommerce_cart_totals_coupon_html', $coupon_html, $coupon, $discount_amount_html ), array_replace_recursive( wp_kses_allowed_html( 'post' ), array( 'a' => array( 'data-coupon' => true ) ) ) ); // phpcs:ignore PHPCompatibility.PHP.NewFunctions.array_replace_recursiveFound
}

if (!function_exists('shopical_get_products')){
    /**
     * @param $number_of_products
     * @param $category
     * @param $show
     * @param $orderby
     * @param $order
     * @return WP_Query
     */
    function shopical_get_products($number_of_products = '4', $category = 0, $show = '', $orderby = 'date', $order = 'DESC'){
        if (!class_exists('WooCommerce')) {
            return;
        }

        $product_visibility_term_ids = wc_get_product_visibility_term_ids();

        $query_args = array(
            'posts_per_page' => $number_of_products,
            'post_status' => 'publish',
            'post_type' => 'product',
            'no_found_rows' => 1,
            'order' => $order,
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'product_visibility',
                    'terms' => array('exclude-from-catalog'),
                    'field' => 'name',
                    'operator' => 'NOT IN',
                ),
            ),
        );

        if (absint($category) > 0) {
            $query_args['tax_query'][] = array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id',
                'terms' => $category

            );
        }

        switch ($show) {
            case 'featured' :
                $query_args['tax_query'][] = array(
                    'taxonomy' => 'product_visibility',
                    'field' => 'term_id',
                    'terms' => $product_visibility_term_ids['featured'],
                );
                break;
            case 'onsale' :
                $product_ids_on_sale = wc_get_product_ids_on_sale();
                $product_ids_on_sale[] = 0;
                $query_args['post__in'] = $product_ids_on_sale;
                break;
        }

        switch ($orderby) {
            case 'price' :
                $query_args['meta_key'] = '_price';
                $query_args['orderby'] = 'meta_value_num';
                break;
            case 'rand' :
                $query_args['orderby'] = 'rand';
                break;
            case 'sales' :
                $query_args['meta_key'] = 'total_sales';
                $query_args['orderby'] = 'meta_value_num';
                break;
            default :
                $query_args['orderby'] = 'date';
        }

        return new WP_Query(apply_filters('shopical_widget_query_args', $query_args));
    }
}

if (!function_exists('shopical_get_featured_image')){
    function shopical_get_featured_image($post_id, $size = 'thumbnail'){
        if (has_post_thumbnail()) {
            $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post_id), $size);
            if($thumb){
                $url = $thumb['0'];
            }else{
                $url = wc_placeholder_img_src();
            }
        } else {
            $url = wc_placeholder_img_src();
        }
        return $url;
    }
}

function cf_get_srcset($attachment_id, $size = 'full'){
    $image = wp_get_attachment_image_src($attachment_id, $size);

    if ( $image ) {
        list($src, $width, $height) = $image;
        $image_meta = wp_get_attachment_metadata( $attachment_id );

        if ( is_array( $image_meta ) ) {
            $size_array = array( absint( $width ), absint( $height ) );
            $srcset     = wp_calculate_image_srcset( $size_array, $src, $image_meta, $attachment_id );
            $sizes      = wp_calculate_image_sizes( $size_array, $src, $image_meta, $attachment_id );
        }
        return [
            'srcset' => $srcset,
            'sizes' => $sizes,
            'width' => $width,
            'height' => $height,
        ];
    }
    return null;
}

function get_the_post_thumbnail_alt($post_id) {
    return get_post_meta(get_post_thumbnail_id($post_id), '_wp_attachment_image_alt', true);
}

/**
 * is_realy_woocommerce_page - Returns true if on a page which uses WooCommerce templates (cart and checkout are standard pages with shortcodes and which are also included)
 *
 * @access public
 * @return bool
 */
function is_realy_woocommerce_page () {
    if( function_exists ( "is_woocommerce" ) && is_woocommerce()){
        return true;
    }
    $woocommerce_keys = array ( "woocommerce_shop_page_id" ,
        "woocommerce_terms_page_id" ,
        "woocommerce_cart_page_id" ,
        "woocommerce_checkout_page_id" ,
        "woocommerce_pay_page_id" ,
        "woocommerce_thanks_page_id" ,
        "woocommerce_myaccount_page_id" ,
        "woocommerce_edit_address_page_id" ,
        "woocommerce_view_order_page_id" ,
        "woocommerce_change_password_page_id" ,
        "woocommerce_logout_page_id" ,
        "woocommerce_lost_password_page_id" ) ;

    foreach ( $woocommerce_keys as $wc_page_id ) {
        if ( get_the_ID () == get_option ( $wc_page_id , 0 ) ) {
            return true ;
        }
    }
    return false;
}

function cf_get_attachment_id_from_src ($image_src) {
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
    $id = $wpdb->get_var($query);
    return $id;
}


function cf_get_image_sizes() {
    global $_wp_additional_image_sizes;

    $sizes = array();

    foreach ( get_intermediate_image_sizes() as $_size ) {
        if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
            $sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
            $sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
            $sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
        } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
            $sizes[ $_size ] = array(
                'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
            );
        }
    }

    return $sizes;
}

function cf_wc_placeholder_img_src($size = 'woocommerce_thumbnail') {
    $all_image_sizes = cf_get_image_sizes();
    $width = $all_image_sizes[$size]['width'];
    $height = $all_image_sizes[$size]['height'];

    $DIR = wp_upload_dir();
    if(file_exists($DIR['basedir'].'/2020/04/placeholder-'.$width.'x'.$height.'.png')){
        $src = untrailingslashit( $DIR['baseurl'] ). '/2020/04/placeholder-'.$width.'x'.$height.'.png';
    }else{
        $src = untrailingslashit( $DIR['baseurl'] ). '/2020/04/placeholder.png';
    }
    return [$src, $width, $height];
}

function cf_get_thumb_info($img_size = 'woocommerce_thumbnail'){
    global $product;
    $THUMB = new stdClass();
    if(has_post_thumbnail()){
        $attach_id = get_post_thumbnail_id();
        $thumb_info = wp_get_attachment_image_src($attach_id, $img_size);
        $THUMB->img_srcset = wp_get_attachment_image_srcset($attach_id, $img_size);
        $THUMB->img_sizes = wp_get_attachment_image_sizes($attach_id, $img_size);
    }else{
        $thumb_info = cf_wc_placeholder_img_src($img_size);
        $THUMB->img_srcset = null;
        $THUMB->img_sizes = null;
    }

    $THUMB->img_src = $thumb_info[0];
    $THUMB->img_width = $thumb_info[1];
    $THUMB->img_height = $thumb_info[2];

    return $THUMB;
}

function clearSmartyCache(){
    if(is_super_admin()){
        $list_path = [ASSETS_DIR.'/cache/templates_c', ASSETS_DIR.'/cache/template_ca'];
        foreach ($list_path as $path) {
            chmod($path, 0777);
            $files = glob($path.'/{,.}*', GLOB_BRACE);
            foreach($files as $index => $file){ // iterate files
                if(is_file($file)){
                    echo $index.": ";
                    if (unlink($file)) {
                        echo 'success';
                    } else {
                        echo 'fail';
                    }
                    echo "<br/>";
                }
            }
        }
    }else{
        return false;
    }
    return true;
}