<?php
require GET_STYLESHEET_DIRECTORY . '/inc/filter/woo.php';
require GET_STYLESHEET_DIRECTORY . '/inc/filter/cart.php';

if ( class_exists( 'YITH_WCWL' ) ) {
    require GET_STYLESHEET_DIRECTORY . '/inc/filter/wishlist.php';
}

require GET_STYLESHEET_DIRECTORY . '/inc/filter/shop_catalog.php';

/**
 * REMOVE PARENT THEMES FUNCTION
 * */
function remove_parent_theme_function() {
    remove_filter( 'woocommerce_default_address_fields', 'flatsome_override_existing_checkout_fields');
}
add_action('wp_loaded', 'remove_parent_theme_function');

/**
 * REMOVE PARENT THEMES ACTION
 * */
function remove_parent_theme_action(){
    remove_action( 'wp_head', 'wp_generator' );
}
add_action( 'init', 'remove_parent_theme_action', 0 );

/**
 * REMOVE SCRIPTS
 * */
function PREFIX_remove_scripts() {
    wp_dequeue_style( 'flatsome-main' );
    wp_dequeue_style( 'flatsome-shop' );
    wp_dequeue_style( 'menu-image' );
    wp_dequeue_style( 'msl-main' );
    wp_dequeue_style( 'msl-custom' );
    wp_dequeue_style( 'awts-style' );
    if(!IS_PRODUCT){
        wp_dequeue_style( 'woobt-frontend' );
    }
    if(IS_FRONT_PAGE){
        wp_dequeue_style( 'flatsome-icons' );
    }

    wp_dequeue_script( 'flatsome-live-search' );
}

add_action( 'parent_after_script', 'PREFIX_remove_scripts', 100 );

/**
 * Remove default image sizes here.
 * */
add_action('wp_head', 'my_analytics', 20);
function my_analytics(){
    ?>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-165684896-1', 'auto');
        ga('send', 'pageview');
    </script>
    <?php
}