<?php
function add_custom_const(){
    if(is_woocommerce_activated()){
        define("IS_PRODUCT", is_product());
        define("IS_FRONT_PAGE", is_front_page());
        define("IS_CART", is_cart());
        define("IS_CHECKOUT", is_checkout());
    }else{
        define("IS_PRODUCT", false);
        define("IS_FRONT_PAGE", false);
        define("IS_CART", false);
        define("IS_CHECKOUT", false);
    }

    define('IS_USER_LOGGED_IN', is_user_logged_in());
}
add_action('parent_after_script', 'add_custom_const');
