<?php
function localize_script(){
    wp_enqueue_script('themes-js', JS_PATH . '/themes.js', false);
    wp_localize_script( 'themes-js', 'woo_vars', array(
            'added_to_cart_text' => __( 'Added to cart successfully', 'woocommerce' ),
            'variation_required_text' => __( 'Variation required', 'woocommerce' ),
            'added_to_cart_error_text' => __( 'Add to cart unsuccessful', 'woocommerce' ),
        )
    );
}
add_action('wp_footer', 'localize_script');

/**
 * Change Jquery version
 * */
function change_jquery_version() {
    if (!is_admin() && $GLOBALS['pagenow'] !== 'wp-login.php') {
        wp_deregister_script('jquery');
        wp_enqueue_script('jquery', LIBS_PATH . '/jquery/jquery-3.3.1.min.js', false);
    }
}
add_action('init', 'change_jquery_version');
@ini_set( 'display_errors', 1 );