<?php
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-menu-1' => __( 'Footer Menu 1' ),
            'footer-menu-2' => __( 'Footer Menu 2' ),
            'social-icon-1' => __( 'Social icon 1' ),
            'catalog-menu' => __( 'Catalog Menu' ),
        ));}
add_action( 'init', 'register_my_menus' );