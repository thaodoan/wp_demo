<?php
require_once GET_STYLESHEET_DIRECTORY . '/modules/extend/block_logo.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/extend/block_copyright.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/extend/block_breadcrumb.php';

require_once GET_STYLESHEET_DIRECTORY . '/modules/banner/block_banner.php';

require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_cart_icon.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_mini_cart.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_product_cat.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_product_by_cat.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_wishlist_icon.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_addtocart_btn.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_quickview_btn.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_product_by_block.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_product_by_tag.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_product_search.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_recent.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_group.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_sale_flash.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_content_product.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/shop/block_product_item.php';

require_once GET_STYLESHEET_DIRECTORY . '/modules/posts/block_post_search.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/posts/block_list_post_search.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/posts/block_post_item.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/posts/block_posts_by_tag.php';
require_once GET_STYLESHEET_DIRECTORY . '/modules/posts/block_latest_post.php';

require_once GET_STYLESHEET_DIRECTORY . '/modules/menu/block_menu.php';

/**
 *
 */
function get_header_custom(){
    get_template_part('layout/header', '');
}
add_shortcode( 'get_header', 'get_header_custom' );

//
function header_extend(){
    get_template_part('layout/header', 'extend');
}
add_shortcode( 'header_extend', 'header_extend' );

//
function header_asset(){
    get_template_part('layout/header', 'asset');
}
add_shortcode('header_asset', 'header_asset' );

//
function get_footer_custom(){
    get_template_part('layout/footer', '');
}
add_shortcode( 'get_footer', 'get_footer_custom' );

//
function footer_extend(){
    get_template_part('layout/footer', 'extend');
}
add_shortcode( 'footer_extend', 'footer_extend' );

//
function footer_asset(){
    get_template_part('layout/footer', 'asset');
}
add_shortcode( 'footer_asset', 'footer_asset' );

//
function wrapBegin(){
    get_template_part('layout/body', 'begin');
}
add_shortcode( 'wrap_begin', 'wrapBegin' );

//
function wrapEnd(){
    get_template_part('layout/body', 'end');
}
add_shortcode( 'wrap_end', 'wrapEnd' );