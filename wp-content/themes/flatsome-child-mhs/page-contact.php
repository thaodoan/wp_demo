<?php
/*
Template name: MHS - Flatsome Child - Lien he
*/
echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
echo do_shortcode('[header_extend]');
?>

<div class="fix-more-padding row">

<aside id="column-left" class="col-md-3 col-sm-4 col-xs-12 hidden-xs">
    <?php get_template_part('layout/body', 'left'); ?>
</aside>

<div id="content" class="col-md-9 col-sm-8">
    <div class="text-center">
        <?php if(has_post_thumbnail()):?>
            <img src="<?= get_the_post_thumbnail_url(); ?>" alt="<?= get_the_title(); ?>">
        <?php endif; ?>
    </div>
    <div class="page-contact">
        <p class="text-center font-weight-bold text-size-4">
            Liên hệ với chúng tôi tại:
        </p>
        <div class="row">
            <div class="col-lg-6">
                <?= do_shortcode('[contact-form-7 id="849" title="Form Liên hệ"]')?>
            </div>
            <div class="content col-lg-6 mt-4 mt-lg-0">
                <?php
                // TO SHOW THE PAGE CONTENTS
                while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                    <?php the_content(); ?> <!-- Page Content -->
                <?php
                endwhile; //resetting the page loop
                wp_reset_query(); //resetting the page query
                ?>
            </div>
        </div>
        <div class="row">
            <div class="wpb_map_wraper col-lg-12 mt-5 pt-4 border-top">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9320.825214448794!2d106.6887285019756!3d10.817108558503918!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752924d71a5401%3A0x1f71648e40b85a9d!2sC%C3%B4ng%20ty%20TNHH%20Richcom%20(%20office%202%20)!5e0!3m2!1sen!2s!4v1570353490279!5m2!1sen!2s" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
        </div>
    </div>
</div>

</div>

<?php
echo do_shortcode('[footer_extend]');
echo do_shortcode('[get_footer]');
echo do_shortcode('[wrap_end]');
?>

