(function ($) {
    var myTimerPage = "",
        myTimersecField = "",
        tip_active = !1,
        ftip_active = !1,
        tip_autoclose = !0,
        ftip_autoclose = !0,
        winX = 0,
        winY = 0,
        oldWinX = 0,
        oldWinY = 0,
        cRangeX = 0,
        cRangeY = 0,
        docX = 0,
        docY = 0,
        brcb = $(".breadcrumbs-wrap"),
        reCapIDs = [];

    function winResize() {
        oldWinX = winX, oldWinY = winY, winX = $(window).width(), winY = $(window).height(), docX = $(document).width(), docY = $(document).height(), cRangeX = Math.abs(winX - oldWinX), cRangeY = Math.abs(winY - oldWinY)
    }

    function fix_banner_center() {
        var t = Math.round((winX - 1330) / 2);
        0 <= t ? ($("div.fix_banner_left").css("left", t + "px"), $("div.fix_banner_right").css("right", t + "px"), 0 >= (t = Math.round((winY - $("div.fix_banner_left").height()) / 2)) && (t = 0), $("div.fix_banner_left").css("top", t + "px"), 0 >= (t = Math.round((winY - $("div.fix_banner_right").height()) / 2)) && (t = 0), $("div.fix_banner_right").css("top", t + "px"), $("div.fix_banner_left").show(), $("div.fix_banner_right").show()) : ($("div.fix_banner_left").hide(), $("div.fix_banner_right").hide())
    }

    function timeoutsesscancel() {
        $("#timeoutsess").slideUp("slow", function() {
            clearInterval(myTimersecField), myTimerPage = setTimeout(function() {
                timeoutsessrun()
            }, 1)
        })
    }

    function timeoutsessrun() {

    }

    function locationReplace(t) {
        history.pushState && history.pushState(null, null, t)
    }

    function checkWidthMenu() {
        theme_responsive && "absolute" == $("#menusite").css("position") ? ($("li.dropdown ul").removeClass("dropdown-menu"), $("li.dropdown ul").addClass("dropdown-submenu"), $("li.dropdown a").addClass("dropdown-mobile"), $("#menu-site-default ul li a.dropdown-toggle").addClass("dropdown-mobile"), $("li.dropdown ul li a").removeClass("dropdown-mobile")) : ($("li.dropdown ul").addClass("dropdown-menu"), $("li.dropdown ul").removeClass("dropdown-submenu"), $("li.dropdown a").removeClass("dropdown-mobile"), $("li.dropdown ul li a").removeClass("dropdown-mobile"), $("#menu-site-default ul li a.dropdown-toggle").removeClass("dropdown-mobile")), $("#menu-site-default .dropdown").hover(function(t) {
            ("fixed" != $("#menusite").css("position") || $("#menusite").hasClass("sticky")) && ($(this).addClass("open"), $(this).find("a").attr("aria-expanded", "true"))
        }, function() {
            ("fixed" != $("#menusite").css("position") || $("#menusite").hasClass("sticky")) && ($(this).removeClass("open"), $(this).find("a").attr("aria-expanded", "false"))
        })
    }

    function checkAll(t) {
        return $(".checkAll", t).is(":checked") ? $(".checkSingle", t).each(function() {
            $(this).prop("checked", !0)
        }) : $(".checkSingle", t).each(function() {
            $(this).prop("checked", !1)
        }), !1
    }

    function checkSingle(t) {
        var e = 0,
            a = 0;
        return $(".checkSingle", t).each(function() {
            $(this).is(":checked") ? e++ : a++
        }), 0 != e && 0 == a ? $(".checkAll", t).prop("checked", !0) : $(".checkAll", t).prop("checked", !1), !1
    }

    function tipHide() {
        $("[data-toggle=tip]").attr("data-click", "y").removeClass("active"), $("#tip").hide(), tip_active = !1, tipAutoClose(!0)
    }

    function ftipHide() {
        $("[data-toggle=ftip]").attr("data-click", "y").removeClass("active"), $("#ftip").hide(), ftip_active = !1, ftipAutoClose(!0)
    }

    function tipAutoClose(t) {
        1 != t && (t = !1), tip_autoclose = t
    }

    function ftipAutoClose(t) {
        1 != t && (t = !1), ftip_autoclose = t
    }

    function tipShow(t, e, a) {
        $(t).is(".pa") && switchTab(".guest-sign", t), tip_active && tipHide(), ftip_active && ftipHide(), $("[data-toggle=tip]").removeClass("active"), $(t).attr("data-click", "n").addClass("active"), void 0 !== a ? $("#tip").attr("data-content", e).show("fast", function() {
            "recaptchareset" == a && "undefined" != typeof nv_is_recaptcha && nv_is_recaptcha && ($('[data-toggle="recaptcha"]', $(this)).each(function() {
                var t, e = $(this).parent(),
                    a = $(this).attr("id"),
                    n = "recaptcha" + (new Date).getTime() + nv_randomPassword(8),
                    o = !1,
                    r = 0,
                    s = "";
                for ($(this).remove(), e.append('<div id="' + n + '" data-toggle="recaptcha"></div>'), i = 0, j = nv_recaptcha_elements.length; i < j; i++)
                    if (void 0 !== (t = nv_recaptcha_elements[i]).pnum && void 0 !== t.btnselector && t.pnum && "" != t.btnselector && t.id == a) {
                        for (r = t.pnum, s = t.btnselector, o = $("#" + n), k = 1; k <= t.pnum; k++) o = o.parent();
                        o = $(t.btnselector, o);
                        break
                    }
                var c = {};
                c.id = n, 0 != o && (c.btn = o, c.pnum = r, c.btnselector = s), nv_recaptcha_elements.push(c)
            }), reCaptchaLoadCallback())
        }) : $("#tip").attr("data-content", e).show("fast"), tip_active = 1
    }

    function ftipShow(t, e, a) {
        if ($(t).is(".qrcode") && "no" == $(t).attr("data-load")) return qrcodeLoad(t), !1;
        tip_active && tipHide(), ftip_active && ftipHide(), $("[data-toggle=ftip]").removeClass("active"), $(t).attr("data-click", "n").addClass("active"), void 0 !== a ? $("#ftip").attr("data-content", e).show("fast", function() {
            "recaptchareset" == a && "undefined" != typeof nv_is_recaptcha && nv_is_recaptcha && ($('[data-toggle="recaptcha"]', $(this)).each(function() {
                var t, e = $(this).parent(),
                    a = $(this).attr("id"),
                    n = "recaptcha" + (new Date).getTime() + nv_randomPassword(8),
                    o = !1,
                    r = 0,
                    s = "";
                for ($(this).remove(), e.append('<div id="' + n + '" data-toggle="recaptcha"></div>'), i = 0, j = nv_recaptcha_elements.length; i < j; i++)
                    if (void 0 !== (t = nv_recaptcha_elements[i]).pnum && void 0 !== t.btnselector && t.pnum && "" != t.btnselector && t.id == a) {
                        for (r = t.pnum, s = t.btnselector, o = $("#" + n), k = 1; k <= t.pnum; k++) o = o.parent();
                        o = $(t.btnselector, o);
                        break
                    }
                var c = {};
                c.id = n, 0 != o && (c.btn = o, c.pnum = r, c.btnselector = s), nv_recaptcha_elements.push(c)
            }), reCaptchaLoadCallback())
        }) : $("#ftip").attr("data-content", e).show("fast"), ftip_active = 1
    }

    function openID_load(t) {
        $(this).attr("src");
        return nv_open_browse(t, "NVOPID", 550, 500, "resizable=no,scrollbars=1,toolbar=no,location=no,titlebar=no,menubar=0,location=no,status=no"), !1
    }

    function openID_result() {
        return $("#openidResult").fadeIn(), setTimeout(function() {
            "" != $("#openidResult").attr("data-redirect") ? window.location.href = $("#openidResult").attr("data-redirect") : "success" == $("#openidResult").attr("data-result") ? window.location.href = window.location.href : $("#openidResult").hide(0).text("").attr("data-result", "").attr("data-redirect", "")
        }, 5e3), !1
    }

    function qrcodeLoad(t) {
        var e = new Image,
            a = $(t).data("img");
        $(e).on("load", function() {
            $(a).attr("src", e.src), $(t).attr("data-load", "yes").click()
        }), e.src = nv_base_siteurl + "index.php?second=qr&u=" + encodeURIComponent($(t).data("url")) + "&l=" + $(t).data("level") + "&ppp=" + $(t).data("ppp") + "&of=" + $(t).data("of")
    }

    function switchTab(t) {
        if ($(t).is(".current")) return !1;
        var e = $(t).data("switch").split(/\s*,\s*/),
            a = $(t).data("obj");
        for ($(a + " [data-switch]").removeClass("current"), $(t).addClass("current"), $(a + " " + e[0]).removeClass("hidden"), i = 1; i < e.length; i++) $(a + " " + e[i]).addClass("hidden")
    }

    function change_captcha(t) {
        if ("undefined" != typeof nv_is_recaptcha && nv_is_recaptcha) {
            for (i = 0, j = reCapIDs.length; i < j; i++) {
                var e = reCapIDs[i],
                    a = nv_recaptcha_elements[e[0]];
                $("#" + a.id).length && (void 0 !== a.btn && "" != a.btn && a.btn.prop("disabled", !0), grecaptcha.reset(e[1]))
            }
            reCaptchaLoadCallback()
        } else $("img.captchaImg").attr("src", nv_base_siteurl + "index.php?scaptcha=captcha&nocache=" + nv_randomPassword(10)), void 0 !== t && "" != t && $(t).val("");
        return !1
    }


    function modalShow(t, e, a) {
        "" != t && void 0 !== t && $("#sitemodal .modal-content").prepend('<div class="modal-header"><h2 class="modal-title">' + t + "</h2></div>"), $("#sitemodal").find(".modal-title").html(t), $("#sitemodal").find(".modal-body").html(e);
        var n = !1;
        void 0 !== a && "recaptchareset" == a && "undefined" != typeof nv_is_recaptcha && nv_is_recaptcha && (n = $(window).scrollTop(), $("#sitemodal").on("show.bs.modal", function() {
            $('[data-toggle="recaptcha"]', $(this)).each(function() {
                var t, e = $(this).parent(),
                    a = $(this).attr("id"),
                    n = "recaptcha" + (new Date).getTime() + nv_randomPassword(8),
                    o = !1,
                    r = 0,
                    s = "";
                for ($(this).remove(), e.append('<div id="' + n + '" data-toggle="recaptcha"></div>'), i = 0, j = nv_recaptcha_elements.length; i < j; i++)
                    if (void 0 !== (t = nv_recaptcha_elements[i]).pnum && void 0 !== t.btnselector && t.pnum && "" != t.btnselector && t.id == a) {
                        for (r = t.pnum, s = t.btnselector, o = $("#" + n), k = 1; k <= t.pnum; k++) o = o.parent();
                        o = $(t.btnselector, o);
                        break
                    }
                var c = {};
                c.id = n, 0 != o && (c.btn = o, c.pnum = r, c.btnselector = s), nv_recaptcha_elements.push(c)
            }), reCaptchaLoadCallback()
        })), n ? ($("html,body").animate({
            scrollTop: 0
        }, 200, function() {
            $("#sitemodal").modal({
                backdrop: "static"
            })
        }), $("#sitemodal").on("hide.bs.modal", function() {
            $("html,body").animate({
                scrollTop: n
            }, 200)
        })) : $("#sitemodal").modal({
            backdrop: "static"
        }), $("#sitemodal").on("hidden.bs.modal", function() {
            $("#sitemodal .modal-content").find(".modal-header").remove()
        })
    }

    function modalShowByObj(t, e) {
        modalShow($(t).attr("title"), $(t).html(), e)
    }

    function initializeMap() {
        var t, e, a, i, n, o, r = !1;
        ($(".company-map-modal").each(function() {
            $(this).data("trigger") && (r = $(".company-map", $(this)).attr("id"))
        }), r) && (e = parseFloat($("#" + r).data("clat")), a = parseFloat($("#" + r).data("clng")), i = parseFloat($("#" + r).data("lat")), n = parseFloat($("#" + r).data("lng")), o = parseInt($("#" + r).data("zoom")), t = new google.maps.Map(document.getElementById(r), {
            zoom: o,
            center: {
                lat: e,
                lng: a
            }
        }), new google.maps.Marker({
            map: t,
            position: new google.maps.LatLng(i, n),
            draggable: !1,
            animation: google.maps.Animation.DROP
        }))
    }

    function nvbreadcrumbs() {
        if (brcb.length) {
            var t = $(".display", brcb).innerWidth() - 40,
                e = $(".breadcrumbs", brcb),
                a = $(".temp-breadcrumbs", brcb),
                n = $(".subs-breadcrumbs", brcb),
                o = $(".show-subs-breadcrumbs", brcb),
                r = [],
                s = !1;
            for (a.find("a").each(function() {
                r.push([$(this).attr("title"), $(this).attr("href")])
            }), e.html(""), n.html(""), i = r.length - 1; 0 <= i; i--) {
                if (!s) {
                    var c = 0;
                    e.prepend('<li id="brcr_' + i + '" itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="' + r[i][1] + '"><i class="fa fa-angle-double-right "></i><span itemprop="title">' + r[i][0] + "</span></a></li>"), e.find("li").each(function() {
                        c += $(this).outerWidth(!0)
                    }), c > t && (s = !0, $("#brcr_" + i, e).remove())
                }
                s && n.append('<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="' + r[i][1] + '"><span itemprop="title"><em class="fa fa-long-arrow-up"></em> ' + r[i][0] + "</span></a></li>")
            }
            s ? o.removeClass("hidden") : o.addClass("hidden")
        }
    }

    function showSubBreadcrumbs(t, e) {
        e.preventDefault(), e.stopPropagation();
        var a = $(".subs-breadcrumbs", brcb);
        $("em", t).is(".fa-angle-right") ? $("em", t).removeClass("fa-angle-right").addClass("fa-angle-down") : $("em", t).removeClass("fa-angle-down").addClass("fa-angle-right"), a.toggleClass("open"), $(document).on("click", function() {
            $("em", t).is(".fa-angle-down") && ($("em", t).removeClass("fa-angle-down").addClass("fa-angle-right"), a.removeClass("open"))
        })
    }

    function add_hint(t, e) {
        if (t && e) {
            var a = document.createElement("link");
            a.setAttribute("rel", t), a.setAttribute("href", e), document.getElementsByTagName("head")[0].appendChild(a)
        }
    }
    $("#menu-site-default .dropdown").click(function(t) {
        if ("fixed" == $("#menusite").css("position")) {
            var e = $(this);
            e.hasClass("open") ? (e.removeClass("open"), e.find("a").attr("aria-expanded", "false")) : (e.addClass("open"), e.find("a").attr("aria-expanded", "true"))
        }
    }), $("#menu-site-default .dropdown").hover(function(t) {
        ("fixed" != $("#menusite").css("position") || $("#menusite").hasClass("sticky")) && ($(this).addClass("open"), $(this).find("a").attr("aria-expanded", "true"))
    }, function() {
        ("fixed" != $("#menusite").css("position") || $("#menusite").hasClass("sticky")) && ($(this).removeClass("open"), $(this).find("a").attr("aria-expanded", "false"))
    });
    var reCaptchaLoadCallback = function() {
            for (i = 0, j = nv_recaptcha_elements.length; i < j; i++) {
                var t = nv_recaptcha_elements[i];
                if ($("#" + t.id).length && void 0 === reCapIDs[i]) {
                    var e = "";
                    void 0 !== t.btn && "" != t.btn && t.btn.prop("disabled", !0), void 0 !== t.size && "compact" == t.size && (e = "compact"), reCapIDs.push([i, grecaptcha.render(t.id, {
                        sitekey: nv_recaptcha_sitekey,
                        type: nv_recaptcha_type,
                        size: e,
                        callback: reCaptchaResCallback
                    })])
                }
            }
        },
        reCaptchaResCallback = function() {
            for (i = 0, j = reCapIDs.length; i < j; i++) {
                var t = reCapIDs[i],
                    e = nv_recaptcha_elements[t[0]];
                if ($("#" + e.id).length) "" != grecaptcha.getResponse(t[1]) && void 0 !== e.btn && "" != e.btn && e.btn.prop("disabled", !1)
            }
        };
    $(function() {
        winResize(), fix_banner_center(), $('a[href="#"], a[href=""]').attr("href", "javascript:void(0);"), $("#totop,#bttop,.bttop").click(function() {
            return $("html,body").animate({
                scrollTop: 0
            }, 800), !1
        }), $(".headerSearch____sss button").on("click", function() {
            if ("n" == $(this).attr("data-click")) return !1;
            $(this).attr("data-click", "n");
            var t = $(".headerSearch input"),
                e = t.attr("maxlength"),
                a = strip_tags(t.val()),
                i = $(this).attr("data-minlength");
            return t.parent().removeClass("has-error"), "" == a || a.length < i || a.length > e ? (t.parent().addClass("has-error"), t.val(a).focus(), $(this).attr("data-click", "y")) : window.location.href = $(this).attr("data-url") + rawurlencode(a), !1
        }), $(".headerSearch____sss input").on("keypress", function(t) {
            13 != t.which || t.shiftKey || (t.preventDefault(), $(".headerSearch button").trigger("click"))
        }), (myTimerPage = setTimeout(function() {
            timeoutsessrun()
        }, 1)), $("form.confirm-reload").change(function() {
            $(window).bind("beforeunload", function() {
                return nv_msgbeforeunload
            })
        }), $(".form-tooltip").tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        }), $("[data-rel='tooltip'][data-content!='']").removeAttr("title").tooltip({
            container: "body",
            html: !0,
            title: function() {
                return ("" != $(this).data("img") && $(this).data("img") ? '<img class="img-thumbnail pull-left" src="' + $(this).data("img") + '" width="90" />' : "") + $(this).data("content")
            }
        }), $(".nv_change_site_lang").change(function() {
            document.location = $(this).val()
        }), $("[data-toggle=collapse]").click(function(t) {
            tipHide(), ftipHide()
        }), $(document).on("keydown", function(t) {
            27 === t.keyCode && (tip_active && tip_autoclose && tipHide(), ftip_active && ftip_autoclose && ftipHide())
        }), $(document).on("click", function() {
            tip_active && tip_autoclose && tipHide(), ftip_active && ftip_autoclose && ftipHide()
        }), $("#tip, #ftip").on("click", function(t) {
            t.stopPropagation()
        }), $("[data-toggle=tip], [data-toggle=ftip]").click(function() {
            var t = $(this).attr("data-target"),
                e = $(t).html(),
                a = $(this).attr("data-toggle"),
                i = "tip" == a ? $("#tip").attr("data-content") : $("#ftip").attr("data-content"),
                n = $(this).data("callback");
            return t != i ? ("" != i && $('[data-target="' + i + '"]').attr("data-click", "y"), "tip" == a ? ($("#tip .bg").html(e), tipShow(this, t, n)) : ($("#ftip .bg").html(e), ftipShow(this, t, n))) : "n" == $(this).attr("data-click") ? "tip" == a ? tipHide() : ftipHide() : "tip" == a ? tipShow(this, t, n) : ftipShow(this, t, n), !1
        }), $(".company-address").length && $(".company-map-modal").on("shown.bs.modal", function() {
            if ($(".company-map-modal").data("trigger", !1), $(this).data("trigger", !0), $("#googleMapAPI").length) initializeMap();
            else {
                var t = document.createElement("script");
                t.type = "text/javascript", t.id = "googleMapAPI", t.src = "https://maps.googleapis.com/maps/api/js?" + ("" != $(this).data("apikey") ? "key=" + $(this).data("apikey") + "&" : "") + "callback=initializeMap", document.body.appendChild(t)
            }
        }), $("textarea").on("input propertychange", function() {
            if (!(t = $(this).prop("maxLength")) || "number" != typeof t) {
                var t = $(this).attr("maxlength"),
                    e = $(this).val();
                e.length > t && $(this).val(e.substr(0, t))
            }
        }), $("[data-dismiss=alert]").on("click", function(t) {
            $(this).is(".close") && $(this).parent().remove()
        }), $("#openidBt").on("click", function() {
            return openID_result(), !1
        }), $("[data-location]").on("click", function() {
            locationReplace($(this).data("location"))
        })
    }), $(document).on({
        "show.bs.modal": function() {
            var t = 1040 + 10 * $(".modal:visible").length;
            $(this).css("z-index", t), setTimeout(function() {
                $(".modal-backdrop").not(".modal-stack").css("z-index", t - 1).addClass("modal-stack")
            }, 0)
        },
        "hidden.bs.modal": function() {
            $(".modal:visible").length > 0 && setTimeout(function() {
                $(document.body).addClass("modal-open")
            }, 0)
        }
    }, ".modal"), $(window).on("resize", function() {
        winResize(), fix_banner_center(), nvbreadcrumbs()
    }), $(window).on("load", function() {
        var t, e, a, i, n;
        if (nvbreadcrumbs(), 0 < $(".fb-like").length && (1 > $("#fb-root").length && $("body").append('<div id="fb-root"></div>'), t = document, e = "facebook-jssdk", a = t.getElementsByTagName("script")[0], i = $('[property="fb:app_id"]').length > 0 ? "&appId=" + $('[property="fb:app_id"]').attr("content") : "", n = $('[property="og:locale"]').length > 0 ? $('[property="og:locale"]').attr("content") : "vi" == nv_lang_data ? "vi_VN" : "en_US", t.getElementById(e) || ((t = t.createElement("script")).id = e, t.src = "//connect.facebook.net/" + n + "/all.js#xfbml=1" + i, a.parentNode.insertBefore(t, a))), 0 < $(".g-plusone").length && (window.___gcfg = {
            lang: nv_lang_data
        }, function() {
            var t = document.createElement("script");
            t.type = "text/javascript", t.async = !0, t.src = "//apis.google.com/js/plusone.js";
            var e = document.getElementsByTagName("script")[0];
            e.parentNode.insertBefore(t, e)
        }()), 0 < $(".twitter-share-button").length && function() {
            var t = document.createElement("script");
            t.type = "text/javascript", t.src = "//platform.twitter.com/widgets.js";
            var e = document.getElementsByTagName("script")[0];
            e.parentNode.insertBefore(t, e)
        }(), "undefined" != typeof nv_is_recaptcha && nv_is_recaptcha && nv_recaptcha_elements.length > 0) {
            var o = document.createElement("script");
            o.type = "text/javascript", o.async = !0, o.src = "https://www.google.com/recaptcha/api.js?hl=" + nv_lang_interface + "&onload=reCaptchaLoadCallback&render=explicit";
            var r = document.getElementsByTagName("script")[0];
            r.parentNode.insertBefore(o, r)
        }
    }), $(function() {
        $("#go-top").goTop({
            scrollTop: 450,
            scrollSpeed: 0,
            fadeInSpeed: 0,
            fadeOutSpeed: 0
        })
    }), window.onscroll = function() {
        myFunction()
    };
    var header = document.getElementById("menusite"),
        sticky = header.offsetTop;

    function myFunction() {
        "none" == $("#menusite .navbar-toggler").css("display") && (window.pageYOffset > sticky + 50 ? header.classList.add("sticky") : header.classList.remove("sticky"))
    }
})(jQuery);

