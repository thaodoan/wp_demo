function sendrating(a, e, n) {
    1 != e && 2 != e && 3 != e && 4 != e && 5 != e || $.post(nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=rating&nocache=" + (new Date).getTime(), "id=" + a + "&checkss=" + n + "&point=" + e, function(a) {
        $("#stringrating").html(a)
    })
}

function remove_text() {
    document.getElementById("to_date").value = "", document.getElementById("from_date").value = ""
}

function nv_del_content(a, e, n) {
    return confirm(nv_is_del_confirm[0]) && $.post(n + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=del_content&nocache=" + (new Date).getTime(), "id=" + a + "&checkss=" + e, function(a) {
        var e = a.split("_");
        "OK" == e[0] ? window.location.href = strHref : "ERR" == e[0] ? alert(e[1]) : alert(nv_is_del_confirm[2])
    }), !1
}

function cartorder(a, e, n) {
    var i = $(a).attr("id");
    "0" == e ? $.ajax({
        type: "GET",
        url: nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=setcart&id=" + i + "&nocache=" + (new Date).getTime(),
        data: "",
        success: function(a) {
            var e = a.split("_")[1];
            if (null != e) {
                for (var n = e.indexOf("#@#"); - 1 != n;) n = (e = e.replace("#@#", "_")).indexOf("#@#");
                alert_msg(e), linkloadcart = nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=loadcart", $("#cart_" + nv_module_name).load(linkloadcart)
            }
        }
    }) : ($("#idmodals").removeData("bs.modal"), $.post(n + "&popup=1", function(a) {
        $("#idmodals .modal-body").html(a), $("#idmodals").modal("show"), setTimeout(function() {
            $("#ispopup .slider-for").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: !1,
                fade: !0,
                asNavFor: ".slider-nav",
                autoplay: !1
            }), $("#ispopup .slider-nav").slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                asNavFor: ".slider-for",
                dots: !1,
                centerMode: !1,
                focusOnSelect: !0,
                infinite: !1
            })
        }, 500)
    }))
}

function cartorder_detail(a, e, n) {
    var i = $("#pnum").val(),
        t = $(a).attr("data-id"),
        l = "",
        _ = "",
        r = 0;
    if ($(".itemsgroup").each(function() {
        0 == $('input[name="groupid[' + $(this).data("groupid") + ']"]:checked').length && (1 == ++r ? _ += $(this).data("header") : _ = _ + ", " + $(this).data("header"))
    }), "" != _) return $("#group_error").css("display", "block"), $("#group_error").html(detail_error_group + " <strong>" + _ + "</strong>"), !1;
    r = 0, $(".groupid").each(function() {
        $(this).is(":checked") && (1 == ++r ? l += $(this).val() : l = l + "," + $(this).val())
    }), $.ajax({
        type: "POST",
        url: nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=setcart&id=" + t + "&group=" + l + "&nocache=" + (new Date).getTime(),
        data: "num=" + i,
        success: function(a) {
            var i = a.split("_")[1];
            if (null != i) {
                for (var t = i.indexOf("#@#"); - 1 != t;) t = (i = i.replace("#@#", "_")).indexOf("#@#");
                alert_msg(i), linkloadcart = nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=loadcart", $("#cart_" + nv_module_name).load(linkloadcart), n ? parent.location = nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=cart" : e && alert("Đã thêm vào giỏ hàng")
            }
        }
    });
}

function alert_msg(a) {
    $("#msgshow").html(a), $("#msgshow").show("slide").delay(3e3).hide("slow")
}

function checknum() {
    var a = $("#price1").val(),
        e = $("#price2").val();
    "" == e && (e = 0), e < a && (document.getElementById("price2").value = ""), isNaN(a) ? (alert(isnumber), document.getElementById("submit").disabled = !0) : 0 != e && isNaN(e) && (alert(isnumber), document.getElementById("submit").disabled = !0)
}

function cleartxtinput(a, e) {
    $("#" + a).focusin(function() {
        var a = $(this).val();
        e == a && $(this).val("")
    }), $("#" + a).focusout(function() {
        "" == $(this).val() && $(this).val(e)
    })
}

function onsubmitsearch(a) {
    var e = $("#keyword").val(),
        n = $("#price1").val();
    null == n && (n = "");
    var i = $("#price2").val();
    null == i && (i = "");
    var t = $("#typemoney").val();
    null == t && (t = "");
    var l = $("#cata").val();
    return ("" != e || "" != n || "" != i || 0 != l) && (window.location.href = nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + a + "&" + nv_fc_variable + "=search_result&keyword=" + rawurlencode(e) + "&price1=" + n + "&price2=" + i + "&typemoney=" + t + "&cata=" + l, !1)
}

function onsubmitsearch1() {
    var a = $("#keyword1").val(),
        e = $("#price11").val();
    null == e && (e = "");
    var n = $("#price21").val();
    null == n && (n = "");
    var i = $("#typemoney1").val();
    null == i && (i = "");
    var t = $("#cata1").val();
    return ("" != a || "" != e || "" != n || 0 != t) && (window.location.href = nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=search_result&keyword=" + rawurlencode(a) + "&price1=" + e + "&price2=" + n + "&typemoney=" + i + "&cata=" + t, !1)
}

function nv_chang_price() {
    var a = $("#sort").val();
    $.post(nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=main&nocache=" + (new Date).getTime(), "changesprice=1&sort=" + a, function(a) {
        "OK" != a ? alert(a) : window.location.href = window.location.href
    })
}

function nv_chang_viewtype() {
    var a = $("#viewtype").val();
    $.post(nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=main&nocache=" + (new Date).getTime(), "changeviewtype=1&viewtype=" + a, function(a) {
        "OK" != a ? alert(a) : window.location.href = window.location.href
    })
}

function nv_compare(a) {
    nv_settimeout_disable("compare_" + a, 5e3), $.post(nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=compare&nocache=" + (new Date).getTime(), "compare=1&id=" + a, function(a) {
        "OK" != (a = a.split("[NV]"))[0] && ($("#compare_" + a[2]).removeAttr("checked"), alert(a[1]))
    })
}

function nv_compare_click() {
    $.post(nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=compare&nocache=" + (new Date).getTime(), "compareresult=1", function(a) {
        "OK" != a ? alert(a) : window.location.href = nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=compare"
    })
}

function nv_compare_del(a, e) {
    confirm(lang_del_confirm) && $.post(nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=compare&nocache=" + (new Date).getTime(), "compare_del=1&id=" + a + "&all=" + e, function(a) {
        "OK" == a && (window.location.href = window.location.href)
    })
}

function wishlist(a, e) {
    $.ajax({
        type: "GET",
        url: nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=wishlist_update&id=" + a + "&ac=add&nocache=" + (new Date).getTime(),
        data: "",
        success: function(a) {
            var n = a.split("_");
            "OK" == n[0] && ($(e).addClass("disabled"), $("#wishlist_num_id").text(n[1])), alert_msg(n[2])
        }
    })
}

function wishlist_del_item(a) {
    confirm(lang_del_confirm) && $.ajax({
        type: "GET",
        url: nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=wishlist_update&id=" + a + "&ac=del&nocache=" + (new Date).getTime(),
        data: "",
        success: function(e) {
            var n = e.split("_");
            "OK" == n[0] && ($("#wishlist_num_id").text(n[1]), $("#item_" + a).remove(), "0" == n[1] && (window.location.href = window.location.href)), alert_msg(n[2])
        }
    })
}

function payment_point(a, e, n) {
    confirm(n) && $.ajax({
        type: "GET",
        url: nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=point&paypoint=1&checkss=" + e + "&order_id=" + a + "&nocache=" + (new Date).getTime(),
        data: "",
        success: function(a) {
            var e = a.split("_");
            "OK" == e[0] ? (alert(e[1]), window.location.href = window.location.href) : alert(e[1])
        }
    })
}

function check_price(a, e) {
    var n = [];
    $(".groupid:checked").each(function() {
        var a = $(this).val();
        "" != a && n.push(a)
    }), n.length > 0 && $.ajax({
        method: "POST",
        url: nv_base_siteurl + "index.php?" + nv_lang_variable + "=" + nv_lang_data + "&" + nv_name_variable + "=" + nv_module_name + "&" + nv_fc_variable + "=detail&nocache=" + (new Date).getTime(),
        data: "check_quantity=1&id_pro=" + a + "&pro_unit=" + e + "&listid=" + n,
        success: function(a) {
            var e = a.split("_");
            "OK" == e[0] ? ($("#product_number").html(e[2]), $("#pnum, .btn-order").attr("disabled", !1), $("#product_number").html(e[2]).removeClass("text-danger"), $("#pnum").attr("max", e[1])) : ($("#pnum, .btn-order").attr("disabled", !0), $("#product_number").html(e[2]).addClass("text-danger"))
        }
    })
}

function fix_image_content() {
    var a, e, n = $(".tab-content");
    if (n.length) {
        var i = n.innerWidth();
        $.each($("img", n), function() {
            void 0 === $(this).data("width") ? (a = $(this).innerWidth(), e = $(this).innerHeight(), $(this).data("width", a), $(this).data("height", e)) : (a = $(this).data("width"), e = $(this).data("height")), a > i && ($(this).prop("width", i), $(this).prop("height", e * i / a))
        })
    }
}
$(window).on("load", function() {
    fix_image_content()
}), $(window).on("resize", function() {
    fix_image_content()
});

$('#keyword').keypress(function (e) {
    if (e.which == 13) {
        $('#search_form_shops').submit();
        return false;
    }
});