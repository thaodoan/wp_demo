<?php
/*
* Template name: MHS - Flatsome Child - HomePage
*/
echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
echo do_shortcode('[header_extend]');
?>

    <h1 class="hidden"><?= do_shortcode('[cgv company_name]'); ?></h1>

    <div class="fix-more-padding row">
        <div class="co-1 col-lg-9 order-lg-2">
            <div class="row">
                <div class="col jssor_0" style="position: relative;">
                    <div id="jssor_1">
                        <div data-u="loading" class="jssorl-009-spin">
                            <img src="<?= ASSETS_PATH . '/themes/' ?>spin.svg" alt="spin"/>
                        </div>
                        <div data-u="slides" class="images">
                            <?= do_shortcode('[block_banner id="1" num="3" img_size="full" tpl="1"]'); ?>
                        </div>

                        <div data-u="navigator" class="jssorb053" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
                            <div data-u="prototype" class="i">
                                <svg viewbox="0 0 16000 16000">
                                    <circle class="co" cx="8000" cy="8000" r="5000"></circle>
                                    <circle class="ci" cx="8000" cy="8000" r="3000"></circle>
                                </svg>
                            </div>
                        </div>
                        <div data-u="arrowleft" class="jssora093" style="" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
                            <svg viewbox="0 0 16000 16000">
                                <circle class="c" cx="8000" cy="8000" r="6080"></circle>
                                <polygon class="a" points="8789.5,4902.8 8789.5,5846.4 6801.7,8000 8789.5,10253.6 8789.5,11097.2 5930.5,8000 "></polygon>
                            </svg>
                        </div>
                        <div data-u="arrowright" class="jssora093" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
                            <svg viewbox="0 0 16000 16000">
                                <circle class="c" cx="8000" cy="8000" r="6080"></circle>
                                <polygon class="a" points="7210.5,4902.8 7210.5,5846.4 9198.3,8000 7210.5,10253.6 7210.5,11097.2 10069.5,8000 "></polygon>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col homepage-section">
                    <div class="head">
                        <h2 class="uppercase text-uppercase">
                            <span class="span1"><?= __('Featured products', 'woocommerce'); ?></span>
                        </h2>
                    </div>
                    <div class="panel-custom row">
                        <div class="flex-row panel-body">
                            <?= do_shortcode('[block_product_by_block tax_name="product_block" tax_value="san-pham-noi-bat" per_page="6"]'); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-3">
                <div class="col homepage-section">
                    <div class="head">
                        <h2 class="uppercase text-uppercase">
                            <span class="span1"><?= __('Other products', 'woocommerce'); ?></span>
                        </h2>
                    </div>
                    <div class="panel-custom row">
                        <div class="flex-row panel-body">
                            <?= do_shortcode('[block_product_by_block tax_name="product_block" tax_value="san-pham-khac" per_page="9"]'); ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row panel">
                <div class="col">
                    <div class="delivery ">
                        <?= do_shortcode('[block_banner id="2" num="1" tpl="2"]'); ?>
                    </div>
                </div>
            </div>

            <!--<div class="row block_news">
                <div class="homepage-section col">
                    <div class="head">
                        <h3 class="title">
                            <span class="span1">
                                <a href="<?/*= get_permalink( get_option( 'page_for_posts' ) ); */?>">TIN TỨC MỚI</a>
                            </span>
                        </h3>
                    </div>
                    <div class="panel-custom">
                        <div class="panel-body">
                            <?/*= do_shortcode('[block_latest_post tpl="home" type="row" posts="8" columns__sm="2" text_align="left" show_date="text" image_size="thumbnail" image_height="75%"]')*/?>
                        </div>
                    </div>
                </div>
            </div>-->

        </div>
        <div class="co-2 col-lg-3 order-lg-1 mt-4 mt-lg-0">
            <?php get_template_part('layout/body', 'left'); ?>
        </div>
    </div>


<?php
echo do_shortcode('[footer_extend]');
echo do_shortcode('[wrap_end]');
echo do_shortcode('[get_footer]');
?>