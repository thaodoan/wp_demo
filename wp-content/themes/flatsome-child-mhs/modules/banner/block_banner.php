<?php
function blockBanner($args){
    extract(shortcode_atts(array(
        'id' => null,
        'num' => 0,
        'tpl' => '',
        'width' => null,
        'height' => null,
        'cache' => true,
        'img_size' => 'full'
    ), $args));

    $cache = $cache && !is_user_logged_in();

    $file_name = 'block_banner_' . $tpl . '_wrap.tpl';

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/banner');

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }

    if(!$smarty->isCached($file_name)){
        $sliders = get_masterslider_parsed_data($id);

        $banners = $sliders ? $sliders['slides'] : null;

        if($banners){
            if(isset($num) && $num > 0){
                $banners = array_slice($banners, 0, $num);
            }

            $BLOCK_CONTENT = '';

            foreach($banners as $key => $banner){
                $banner['srcset'] = null;
                $banner['sizes'] = null;
                $banner['img_width'] = null;
                $banner['img_width'] = null;

                /*if($width){
                    $banner['src_full'] = msp_get_the_resized_image_src(UPLOADS_PATH . $banner['src_full'], $width, $height, true);
                }else{*/
                $banner['src_full'] = UPLOADS_PATH . $banner['src_full'];
                $attachment_id = attachment_url_to_postid($banner['src_full']);
                $img_src = wp_get_attachment_image_src($attachment_id, $img_size);
                $banner['src_full'] = $img_src[0];
                $banner['img_width'] = $img_src[1];
                $banner['img_height'] = $img_src[2];
                $banner['srcset'] = wp_get_attachment_image_srcset( $attachment_id, $img_size);
                $banner['sizes'] = wp_get_attachment_image_sizes($attachment_id, $img_size);
                //}

                if($banner['link']){
                    $banner['type_image_link'] = true;
                    $banner['type_image'] = false;
                }else{
                    $banner['type_image_link'] = false;
                    $banner['type_image'] = true;
                }

                if(empty($banner['alt'])){
                    $banner['alt'] = $banner['link_title'];
                }

                $smarty->assign('BANNER', $banner);

                if($cache){
                    $smarty->setCaching(Smarty::CACHING_OFF);
                }

                $BLOCK_CONTENT .= $smarty->fetch('block_banner_' . $tpl . '.tpl');
                $smarty->clearAllAssign();
            }

            $smarty->assign('BLOCK_TITLE', $sliders['setting']['title']);
            $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
        }
    }

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    }
    return $smarty->display($file_name);
}

add_shortcode( 'block_banner', 'blockBanner' );