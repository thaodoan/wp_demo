<?php
function product_by_block_shortcode($args) {
    // Get attribuets
    extract(shortcode_atts(array(
        'tax_name' => '',
        'tax_value' => '',
        'per_page' => 10,
        'tpl' => '',
        'thumb_size' => 'woocommerce_thumbnail',
        'cache' => false
    ), $args));

    $cache = $cache && !is_user_logged_in();
    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop/block_product_by_block');
    $file_name = 'block_product_by_block_'.$tpl.'_wrap.tpl';
    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }

    if(!$smarty->isCached($file_name)){
        // Define Query Arguments
        $atts = array(
            'post_type' => 'product',
            'posts_per_page' => $per_page,
            'tax_query' => [
                [
                    'taxonomy' => $tax_name,
                    'field' => 'slug',
                    'terms' => $tax_value
                ]
            ]
        );
        $products = new WP_Query( $atts );
        $BLOCK_CONTENT = '';
        if($products->have_posts()){
            $smarty->setCaching(Smarty::CACHING_OFF);
            while ( $products->have_posts() ){
                $products->the_post();
                $smarty->assign('thumb_size', $thumb_size);
                $BLOCK_CONTENT .= $smarty->fetch('block_product_by_block_'.$tpl.'.tpl');
                $smarty->clearAllAssign();
            }
            wp_reset_postdata();
        }

        $tag_info = get_term_by('slug', $tax_value, $tax_name);
        if($tag_info){
            $smarty->assign('BLOCK_TITLE', $tag_info->name);
        }
        $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
    }

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    }
    return $smarty->fetch($file_name);

}
add_shortcode('block_product_by_block', 'product_by_block_shortcode');
