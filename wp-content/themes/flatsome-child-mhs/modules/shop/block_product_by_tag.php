<?php
function product_by_tag_shortcode($args) {
    extract(shortcode_atts(array(
        'tpl' => '',
        'tax_name' => '',
        'posts_per_page' => 12,
        'block_title' => __('Suggest products', 'woocommerce'),
        'thumb_size' => 'woocommerce_thumbnail',
    ), $args));
    $BLOCK_CONTENT = '';
    if($tax_name){
        global $post;
        $smarty = new Smarty;
        $smarty->setTemplateDir(BLOCK_DIR . '/shop/block_product_by_tag');
        if(!is_user_logged_in()){
            $smarty->setCompileCheck(false);
        }
        $terms = get_the_terms($post->ID, $tax_name);
        if($terms){
            unset($GLOBALS['post']);
            global $product;
            foreach($terms as $index => $term){
                $tax_value = $term->name;
                $product = get_page_by_path($tax_value, OBJECT, 'product');
                setup_postdata($product);
                $smarty->assign('thumb_size', $thumb_size);
                $BLOCK_CONTENT .= $smarty->fetch('block_product_by_tag_' . $tpl . '.tpl');
            }
            wp_reset_postdata();
            if(file_exists(BLOCK_DIR . '/shop/block_product_by_tag/block_product_by_tag_' . $tpl . '_wrap.tpl')){
                $smarty->assign('BLOCK_TITLE', $block_title);
                $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
                return $smarty->fetch('block_product_by_tag_'. $tpl .'_wrap.tpl');
            }
        }
    }
    return $BLOCK_CONTENT;
}
add_shortcode('block_product_by_tag', 'product_by_tag_shortcode');
