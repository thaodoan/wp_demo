<?php
function blockWishListIcon($args){
    extract(shortcode_atts(array(
        'tpl' => ''
    ), $args));

    $wishlist_count = YITH_WCWL()->count_products();

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop');

    $smarty->assign('LINK', get_permalink( get_option('yith_wcwl_wishlist_page_id') ));
    $smarty->assign('TEXT', __('Wishlist', 'woocommerce'));
    $smarty->assign('COUNT', $wishlist_count);

    return $smarty->fetch('block_wishlist_icon_'.$tpl.'.tpl');
}
add_shortcode( 'block_wishlist_icon', 'blockWishListIcon' );

