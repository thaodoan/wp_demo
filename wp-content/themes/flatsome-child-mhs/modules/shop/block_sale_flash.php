<?php
function blockSaleFlash($args){
    extract(shortcode_atts(array(
        'tpl' => ''
    ), $args));

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop/block_sale_flash');

    global $product;
    if ( $product->is_on_sale() ){
        // Default Sale Bubble Text
        $text = __( 'Sale!', 'woocommerce' );

        // Custom Sale Bubble text
        $custom_text = get_theme_mod('sale_bubble_text');
        if($custom_text){
            $text = $custom_text;
        }

        // Presentage Sale Bubble
        if(get_theme_mod('sale_bubble_percentage')){
            $text = flatsome_presentage_bubble( $product );
        }
        $smarty->assign('TEXT', $text);
        return $smarty->fetch('block_sale_flash_'.$tpl.'.tpl');
    }
    return '';
}

add_shortcode( 'block_sale_flash', 'blockSaleFlash' );