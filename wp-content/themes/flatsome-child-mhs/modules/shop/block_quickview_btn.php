<?php
function blockQuickViewBtn($args){
    extract(shortcode_atts(array(
        'tpl' => '',
    ), $args));

    global $product;

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR .'/'. basename(__DIR__) .'/block_quickview_btn');
    $smarty->assign('product', $product);
    $smarty->assign('text', __( 'Quick view', 'woocommerce' ));
    return $smarty->fetch('block_quickview_btn_' . $tpl .'.tpl');
}
add_shortcode( 'block_quickview_btn', 'blockQuickViewBtn' );

?>
