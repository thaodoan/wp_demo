<?php
/**
 * @see: class-wc-widget-products
 * */
add_shortcode( 'block_product_group', 'product_group_shortcode' );

function product_group_shortcode($args) {
    extract(shortcode_atts(array(
        'num' => 5,
        'show' => '', //featured,
        'product_cat' => '',
        'hide_free' => false,
        'block_title' => '',
        'tpl' => '',
        'cache' => true,
        'thumb_size' => 'woocommerce_thumbnail'
    ), $args));

    $cache = $cache && !is_user_logged_in();
    $file_name = 'block_group_'.$tpl.'_wrap.tpl';

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop');
    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }
    if(!$smarty->isCached($file_name)) {
        $widget = new WC_Widget_Products;
        $widget->settings['show']['options'][''] = __( 'Lastest products', 'woocommerce' );
        $attr = [
            'show' => $show,
            'number' => $num,
            'hide_free' => $hide_free,
            'product_cat' => $product_cat,
        ];

        $products = $widget->get_products([], $attr);
        $BLOCK_CONTENT = '';
        if($products->have_posts()){
            while ( $products->have_posts() ) {
                $products->the_post();

                $smarty->assign('thumb_size', $thumb_size);
                if($cache){
                    $smarty->setCaching(Smarty::CACHING_OFF);
                }

                $BLOCK_CONTENT .= $smarty->fetch('block_group_' . $tpl . '.tpl');
                $smarty->clearAllAssign();
            }
            wp_reset_postdata();
        }

        if($block_title){
            $smarty->assign('BLOCK_TITLE', $block_title);
        }else{
            $smarty->assign('BLOCK_TITLE', $widget->settings['show']['options'][$show]);
        }
        $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
    }

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    }
    return $smarty->fetch($file_name);
}


function product_list_before(){
    return '';
}
add_filter( 'woocommerce_before_widget_product_list', 'product_list_before' );

function product_list_after(){
    return '';
}
add_filter( 'woocommerce_after_widget_product_list', 'product_list_after' );