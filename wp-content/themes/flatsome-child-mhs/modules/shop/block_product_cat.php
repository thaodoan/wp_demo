<?php

function blockProductCat($args){
    extract(shortcode_atts(array(
        'tpl' => '',
        'cache' => true
    ), $args));
    $cache = $cache && !is_user_logged_in();
    $file_name = 'block_product_cat_' . $tpl . '_wrap.tpl';

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR .'/'. basename(__DIR__) .'/block_product_cat');
    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }

    if(!$smarty->isCached($file_name)){
        $BLOCK_CONTENT = '';
        $product_cat = get_product_category();
        if( !empty($product_cat) ){
            foreach ($product_cat as $key => $cat) {
                _block_ProductCat_Filter_Item($cat);

                $sub_cat = get_product_category( $cat->term_id );
                if($sub_cat){
                    $cat->_has_sub = true;
                    foreach ($sub_cat as $key => $sub){
                        _block_ProductCat_Filter_Item($sub);
                        if($sub->_current){
                            $cat->_current_parent = true;
                        }
                    }
                    $smarty->assign('SUB_CAT', $sub_cat);
                }

                if($cache){
                    $smarty->setCaching(Smarty::CACHING_OFF);
                }

                $smarty->assign('CAT', $cat);
                $smarty->assign('tpl', $tpl);
                $BLOCK_CONTENT .= $smarty->fetch('block_product_cat_'.$tpl.'.tpl');
                $smarty->clearAllAssign();
            }
        }
        $smarty->assign('tpl', $tpl);
        $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
    }

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    }
    return $smarty->fetch($file_name);
}

function _block_ProductCat_Filter_Item(&$cat){
    $cat->link = get_term_link($cat);
    $thumbnail_id = get_term_meta($cat->term_id, 'thumbnail_id',true);
    $cat->thumb = wp_get_attachment_image_src( $thumbnail_id)[0];
    if(is_product_category() && get_queried_object()->slug === $cat->slug){
        $cat->_current = true;
    }
}
add_shortcode( 'block_product_cat', 'blockProductCat' );
