<?php
/**
 * @see: woocommerce_template_loop_add_to_cart
 * */
function blockAddToCartBtn( $args){
    extract(shortcode_atts(array(
        'tpl' => ''
    ), $args));

    global $product;

    if ( $product ) {
        $defaults = array(
            'quantity'   => 1,
            'class'      => implode(
                ' ',
                array_filter(
                    array(
                        'button',
                        'product_type_' . $product->get_type(),
                        $product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
                        $product->supports( 'ajax_add_to_cart' ) && $product->is_purchasable() && $product->is_in_stock() ? 'ajax_add_to_cart' : '',
                    )
                )
            ),
            'attributes' => array(
                'data-product_id'  => $product->get_id(),
                'data-product_sku' => $product->get_sku(),
                'aria-label'       => $product->add_to_cart_description(),
                'rel'              => 'nofollow',
            ),
        );

        $args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );

        if ( isset( $args['attributes']['aria-label'] ) ) {
            $args['attributes']['aria-label'] = wp_strip_all_tags( $args['attributes']['aria-label'] );
        }

        $args['quantity'] = esc_attr(isset( $args['quantity'] ) ? $args['quantity'] : 1);
        $args['class'] = esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' );
        $args['attributes'] = isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '';

        $smarty = new Smarty;
        $smarty->setTemplateDir(BLOCK_DIR.'/shop/block_addtocart_btn');
        $smarty->assign('product', $product);
        $smarty->assign('args', $args);
        return $smarty->fetch('block_addtocart_btn_' . $tpl . '.tpl');
    }
}

add_shortcode('block_addtocart_btn', 'blockAddToCartBtn');
