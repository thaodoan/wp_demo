<?php
function blockProductByCat($args){
    extract(shortcode_atts(array(
        'num_cat' => 10,
        'num_row' => 10,
        'tpl' => '',
        'cache' => true,
        'thumb_size' => 'woocommerce_thumbnail'
    ), $args));

    $cache = $cache && !is_user_logged_in();

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR . '/shop/block_product_by_cat');
    $file_name = 'block_product_by_cat_'.$tpl.'_wrap.tpl';
    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }
    if(!$smarty->isCached($file_name)){
        $product_cat = get_product_category( 0 , $num_cat);
        $BLOCK_CONTENT = '';
        if($product_cat){
            $smarty->setCaching(Smarty::CACHING_OFF);
            foreach ($product_cat as $cat){
                $products = shopical_get_products($num_row, $cat->term_id);
                if($products->have_posts()){
                    $PRODUCTS = '';
                    while ( $products->have_posts() ) {
                        $products->the_post();
                        $smarty->assign('thumb_size', $thumb_size);
                        $PRODUCTS .= $smarty->fetch('block_product_by_cat_'.$tpl.'_child.tpl');
                    }
                    wp_reset_postdata();

                    $cat->link = get_term_link($cat->slug, $cat->taxonomy);
                    $cat->see_all_text = __('See all', 'woocommerce');
                    $smarty->assign('CAT', $cat);
                    $smarty->assign('LIST_PRODUCT', $PRODUCTS);
                    $BLOCK_CONTENT .= $smarty->fetch('block_product_by_cat_'.$tpl.'.tpl');
                    $smarty->clearAllAssign();
                }
            }
        }
        $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
    }

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    }
    return $smarty->fetch($file_name);
}

add_shortcode( 'block_product_by_cat', 'blockProductByCat' );
