<?php
/**
 * param $location: 'primary'
 * param $tpl: 'desktop'
 */

function blockMenu($args){
    extract(shortcode_atts(array(
        'location' => '',
        'tpl' => '',
        'cache' => true,
    ), $args));

    $cache = $cache && !is_user_logged_in();

    $file_name = 'block_menu_'.$tpl.'_wrap.tpl';
    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/menu');

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }

    if(!$smarty->isCached($file_name)){
        $menuID = get_nav_menu_locations()[$location]; // Get menu ID by location
        $menu_name = wp_get_nav_menu_name($location);// Get menu NAME by location
        $menu_items = wp_get_nav_menu_items($menuID); // Get menu Items
        $BLOCK_CONTENT = '';
        if($menu_items){
            $menu_items = wc_nav_menu_item_classes( $menu_items );
            _wp_menu_item_classes_by_context($menu_items);

            foreach ( $menu_items as $key=>$item ) {
                if(!$item->menu_item_parent){
                    _blockMenu_Filter_Item($item);
                    $sub_menu = get_nav_menu_item_children($item->ID, $menu_items, 1);
                    if(!empty($sub_menu)){
                        $item->_has_sub = true;
                        foreach ($sub_menu as $key => $sub){
                            _blockMenu_Filter_Item($sub);
                        }
                        $smarty->assign('SUB_MENU', $sub_menu);
                    }
                    $smarty->assign('TOP_MENU', $item);
                    $smarty->assign('tpl', $tpl);

                    if($cache){
                        $smarty->setCaching(Smarty::CACHING_OFF);
                    }

                    $BLOCK_CONTENT .= $smarty->fetch('block_menu_'.$tpl.'.tpl');
                    $smarty->clearAllAssign();
                }else{
                    unset($menu_items[$key]);
                }
            }
        }

        $smarty->assign('BLOCK_TITLE', $menu_name);
        $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
        $smarty->assign('MENU_ID', $tpl.'_'.$menuID);
    }

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    }

    return $smarty->fetch($file_name);
}

function _blockMenu_Filter_Item(&$menu){
    $menu->_current = in_array('current-menu-item', $menu->classes)?true:false;
    $menu->_current_parent = in_array('current-menu-ancestor', $menu->classes) || in_array('current-menu-parent', $menu->classes)?true:false;

    $menu->title_clean = wp_trim_words( $menu->title,10, '...' );
    $menu->description = ($menu->description)?$menu->description:$menu->title;
    $menu->url = ($menu->url === '#')?'javascript:void(0);':$menu->url;
    $menu->classes = implode(' ', $menu->classes);
    $menu->thumbnail = wp_get_attachment_image_url($menu->thumbnail_id, $menu->image_size);
}

add_shortcode( 'block_menu', 'blockMenu' );