<?php
/**
 * @see:
 * */
function blockPostItem($args){
    extract(shortcode_atts(array(
        'tpl' => '',
        'excerpt_length' => 15,
        'img_size' => 'woocommerce_thumbnail',
        'cache' => true,
    ), $args));

    $cache = $cache && !is_user_logged_in();
    $file_name = 'block_post_item_' . $tpl . '.tpl';

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/posts/block_post_item');
    if($cache){
        $smarty->setCompileCheck(false);
    }

    global $post;
    $post_id = get_the_ID();
    $post_title = get_the_title();

    $THUMB = cf_get_thumb_info($img_size);

    if ( $alt = get_the_post_thumbnail_alt($post_id) ) {
        $THUMB->img_alt = $alt;
    } else {
        $THUMB->img_alt = $post_title;
    }

    $the_excerpt  = get_the_excerpt();
    $excerpt_more = apply_filters( 'excerpt_more', ' [...]' );

    $smarty->assign('post', $post);
    $smarty->assign('THUMB', $THUMB);
    $smarty->assign('TITLE', $post_title);
    $smarty->assign('LINK', get_the_permalink());

    $smarty->assign('EXCERPT', flatsome_string_limit_words($the_excerpt, $excerpt_length) . $excerpt_more);
    $smarty->assign('PUBLISH_DATE', get_the_date());

    return $smarty->fetch($file_name);
}

add_shortcode( 'block_post_item', 'blockPostItem' );