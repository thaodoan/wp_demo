<?php
/**
 * block get Posts by tag
 * */

function get_posts_by_tag($args){
    extract(shortcode_atts(array(
        'tpl' => 'detail',
        'tag_product' => '',
        'posts_per_page' => 12,
        'block_title' => __('Related Posts', 'flatsome'),
    ), $args));

    if($tag_product){
        $smarty = new Smarty;
        $smarty->setTemplateDir(BLOCK_DIR.'/posts/block_posts_by_tag');
        if(!is_user_logged_in()){
            $smarty->setCompileCheck(false);
        }

        $attr = [
            'tax_query' =>[
                [
                    'taxonomy' => 'tag_product',
                    'field' => 'name',
                    'terms' => $tag_product,
                ],

            ],
            'orderby' => 'publish_date',
            'order' => 'DESC',
            'posts_per_page' => $posts_per_page
        ];
        $post_query = new WP_Query( $attr );

        if ($post_query->have_posts()) {
            $BLOCK_CONTENT = '';
            while ($post_query->have_posts()) {
                $post_query->the_post();
                $smarty->assign('POSTS', $post_query->post);
                $BLOCK_CONTENT .= $smarty->fetch('block_posts_by_tag_'.$tpl.'.tpl');
            }
            wp_reset_postdata();

            if(file_exists(BLOCK_DIR . '/posts/block_posts_by_tag/block_posts_by_tag_' . $tpl . '_wrap.tpl')){
                $smarty->assign('BLOCK_TITLE', $block_title);
                $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
                return $smarty->fetch('block_posts_by_tag_'.$tpl.'_wrap.tpl');
            }
        }
    }
    return '';
}

add_shortcode( 'block_posts_by_tag', 'get_posts_by_tag' );

