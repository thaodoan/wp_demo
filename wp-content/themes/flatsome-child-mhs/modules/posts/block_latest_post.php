<?php
/**
 * @see: shortcode_latest_from_blog()
 * */
function get_latest_post($atts, $content = null, $tag)
{
    extract(shortcode_atts(array(
        'tpl' => '',
        'cache' => true,
        //id
        "_id" => 'row-' . rand(),
        'style' => '',
        'class' => '',
        'visibility' => '',

        // Layout
        "columns" => '4',
        "columns__sm" => '1',
        "columns__md" => '',
        'col_spacing' => '',
        "type" => 'slider', // slider, row, masonery, grid
        'width' => '',
        'grid' => '1',
        'grid_height' => '600px',
        'grid_height__md' => '500px',
        'grid_height__sm' => '400px',
        'slider_nav_style' => 'reveal',
        'slider_nav_position' => '',
        'slider_nav_color' => '',
        'slider_bullets' => 'false',
        'slider_arrows' => 'true',
        'auto_slide' => 'false',
        'infinitive' => 'true',
        'depth' => '',
        'depth_hover' => '',

        // posts
        'posts' => '8',
        'ids' => false, // Custom IDs
        'cat' => '',
        'category' => '', // Added for Flatsome v2 fallback
        'excerpt' => 'visible',
        'excerpt_length' => 15,
        'offset' => '',

        // Read more
        'readmore' => '',
        'readmore_color' => '',
        'readmore_style' => 'outline',
        'readmore_size' => 'small',

        // div meta
        'post_icon' => 'true',
        'comments' => 'true',
        'show_date' => 'badge', // badge, text
        'badge_style' => '',
        'show_category' => 'false',

        //Title
        'title_size' => 'large',
        'title_style' => '',

        // Box styles
        'animate' => '',
        'text_pos' => 'bottom',
        'text_padding' => '',
        'text_bg' => '',
        'text_size' => '',
        'text_color' => '',
        'text_hover' => '',
        'text_align' => 'center',
        'image_size' => 'medium',
        'image_width' => '',
        'image_radius' => '',
        'image_height' => '56%',
        'image_hover' => '',
        'image_hover_alt' => '',
        'image_overlay' => '',
        'image_depth' => '',
        'image_depth_hover' => '',

    ), $atts));

    $cache = $cache && !is_user_logged_in();
    $file_name = 'block_latest_post_' . $tpl . '_wrap.tpl';

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR . '/' . basename(__DIR__) . '/block_latest_post');
    if ($cache) {
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }
    if (!$smarty->isCached($file_name)) {
        $args = array(
            'post_status' => 'publish',
            'post_type' => 'post',
            'offset' => $offset,
            'cat' => $cat,
            'posts_per_page' => $posts,
            'ignore_sticky_posts' => true
        );

        // Added for Flatsome v2 fallback
        if (get_theme_mod('flatsome_fallback', 0) && $category) {
            $args['category_name'] = $category;
        }

        // If custom ids
        if (!empty($ids)) {
            $ids = explode(',', $ids);
            $ids = array_map('trim', $ids);

            $args = array(
                'post__in' => $ids,
                'post_type' => array(
                    'post',
                    'featured_item', // Include for its tag archive listing.
                ),
                'numberposts' => -1,
                'orderby' => 'post__in',
                'posts_per_page' => 9999,
                'ignore_sticky_posts' => true,
            );
        }

        query_posts($args);

        $BLOCK_CONTENT = '';
        while (have_posts()) {
            the_post();
            if($cache){
                $smarty->setCaching(Smarty::CACHING_OFF);
            }
            $BLOCK_CONTENT .= $smarty->fetch('block_latest_post_'.$tpl.'.tpl');
            $smarty->clearAllAssign();
        }
        wp_reset_query();
        $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
    }

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    }

    return $smarty->fetch($file_name);
}
add_shortcode('block_latest_post', 'get_latest_post');