<?php

add_shortcode( 'block_logo', 'blockLogo' );
function blockLogo($args){
    extract(shortcode_atts(array(
        'cache' => true,
        'tpl' => '',
        'img_size' => 'thumbnail',
    ), $args));
    $cache = $cache && !is_user_logged_in();

    if(isset($args['tpl'])){
        $file_name = 'block_logo_'.$args['tpl'].'.tpl';
    }else{
        $file_name = 'block_logo.tpl';
    }

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/extend/logo');
    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }

    if(!$smarty->isCached($file_name)){
        $smarty->assign('home_url', esc_url( home_url('/') ));
        $smarty->assign('site_title', esc_attr( get_bloginfo( 'name', 'display' ) ));
        $smarty->assign('logo_height', get_theme_mod('header_height',90));
        $smarty->assign('logo_width', get_theme_mod('logo_width', 200));
        $smarty->assign('site_title', esc_attr( get_bloginfo( 'name', 'display' )));

        $logo_src = flatsome_option('site_logo');
        if($logo_src){
            $attach_id = attachment_url_to_postid($logo_src);
            $logo_src = wp_get_attachment_image_src($attach_id, $img_size)[0];
        }

        $smarty->assign('logo_src', $logo_src);
    }
    return $smarty->fetch($file_name);
}