<?php
do_action('flatsome_before_blog');
?>

<?php if(!is_single() && flatsome_option('blog_featured') == 'top'){ get_template_part('template-parts/posts/featured-posts'); } ?>

    <div class="row-large | ">

        <?php if(!is_single() && flatsome_option('blog_featured') == 'content'){
            get_template_part('template-parts/posts/featured-posts'); }
            ?>
        <?php
        if(is_single()){
            get_template_part( 'template-parts/posts/single');
            comments_template();
        } elseif(flatsome_option('blog_style_archive') && (is_archive() || is_search())){
            get_template_part( 'template-parts/posts/archive', flatsome_option('blog_style_archive') );
        } else {
            get_template_part( 'template-parts/posts/archive', flatsome_option('blog_style') );
        }
        ?>

    </div><!-- .row -->

<?php
do_action('flatsome_after_blog');
?>