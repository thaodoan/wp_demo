<h6 class="entry-category is-xsmall">
    <?php echo get_the_category_list( __( ', ', 'flatsome' ) ) ?>
</h6>

<?php
if ( is_single() ) {
    echo '<h1 class="entry-title h4 mt-3">' . get_the_title() . '</h1>';
} else {
    echo '<h2 class="entry-title h4 mt-3"><a href="' . get_the_permalink() . '" title="'. get_the_title() .'" rel="bookmark" class="plain">' . get_the_title() . '</a></h2>';
}
?>

<div class="entry-divider is-divider small"></div>

<div class="entry-meta uppercase is-xsmall">
    <time class="entry-date published text-size-n1 color-holder font-weight-normal" datetime="<?= get_the_date( 'c' ); ?>">
        <?= get_the_date(); ?>
    </time>
    <time class="updated" datetime="<?= get_the_modified_date('c'); ?>"><?= get_the_modified_date(); ?></time>
</div><!-- .entry-meta -->