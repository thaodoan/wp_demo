<?php
/**
 * Flatsome functions and definitions
 *
 * @package flatsome
 */

require get_template_directory() . '/inc/init.php';

/**
 * Note: It's not recommended to add any custom code here. Please use a child theme so that your customizations aren't lost during updates.
 * Learn more here: http://codex.wordpress.org/Child_Themes
 */

/**
 * Login page css
 * */
if(!function_exists('wpse_login_styles')){
    add_action( 'login_enqueue_scripts', 'wpse_login_styles' );
    function wpse_login_styles() {
        wp_enqueue_style( 'wpse-custom-login', get_template_directory_uri() . '/assets/css/style_login.css' );
        wp_enqueue_style( 'wpse-custom-login-1', get_template_directory_uri() . '/assets/libs/bootstrap/bootstrap.min.css' );
    }
}