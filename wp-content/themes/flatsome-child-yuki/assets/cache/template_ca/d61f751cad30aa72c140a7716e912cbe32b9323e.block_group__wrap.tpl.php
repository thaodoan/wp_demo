<?php
/* Smarty version 3.1.33, created on 2021-02-23 04:24:48
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_group__wrap.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60348390bd5b29_67996289',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd5da789f940279167ded945b9a0cbf5bd3f939f4' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_group__wrap.tpl',
      1 => 1588556470,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 3600,
),true)) {
function content_60348390bd5b29_67996289 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="item">
	<div class="item-product">
		<div class="item-thumb">
			<div class="product-thumb-hover">
				<a href="http://mhs.wpdemo.test/san-pham/tay-te-bao-chet-cho-moi-sunsmile-choosy-lip-scrub-huong-dao/" class="woocommerce-LoopProduct-link">
					<img width="1000" height="1000" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" data-lazy-src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_fd48f43f4bac903f6619435ce59c80d8-100x100.jpeg" />
					<noscript><img width="1000" height="1000" src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_fd48f43f4bac903f6619435ce59c80d8-100x100.jpeg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" /></noscript>
				</a>
			</div>
		</div>
		<div class="content-bottom">
			<div class="item-title"> <a href="http://mhs.wpdemo.test/san-pham/tay-te-bao-chet-cho-moi-sunsmile-choosy-lip-scrub-huong-dao/">Tẩy tế bào chết cho môi Sunsmile CHOOSY Lip Scrub - Hương đào</a></div>
			<div class="price"> <span class="woocommerce-Price-amount amount">280.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></div>
		</div>
	</div>
</div>

<div class="item">
	<div class="item-product">
		<div class="item-thumb">
			<div class="product-thumb-hover">
				<a href="http://mhs.wpdemo.test/san-pham/son-duong-am-lau-troi-ceazanne-3-9g-mau-do-402/" class="woocommerce-LoopProduct-link">
					<img width="1000" height="1000" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" data-lazy-src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_94da2b22758882c35c665423e898e247-100x100.jpeg" />
					<noscript><img width="1000" height="1000" src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_94da2b22758882c35c665423e898e247-100x100.jpeg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" /></noscript>
				</a>
			</div>
		</div>
		<div class="content-bottom">
			<div class="item-title"> <a href="http://mhs.wpdemo.test/san-pham/son-duong-am-lau-troi-ceazanne-3-9g-mau-do-402/">SON DƯỠNG ẨM LÂU TRÔI CEAZANNE 3.9G MÀU ĐỎ 402</a></div>
			<div class="price"> <span class="woocommerce-Price-amount amount">230.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></div>
		</div>
	</div>
</div>

<div class="item">
	<div class="item-product">
		<div class="item-thumb">
			<div class="product-thumb-hover">
				<a href="http://mhs.wpdemo.test/san-pham/son-cezanne-khong-chi-mem-moi-lau-troi-mau-do-nau/" class="woocommerce-LoopProduct-link">
					<img width="1000" height="1000" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" data-lazy-src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_c9145184bdeff994c977631f57d15669-100x100.jpeg" />
					<noscript><img width="1000" height="1000" src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_c9145184bdeff994c977631f57d15669-100x100.jpeg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" /></noscript>
				</a>
			</div>
		</div>
		<div class="content-bottom">
			<div class="item-title"> <a href="http://mhs.wpdemo.test/san-pham/son-cezanne-khong-chi-mem-moi-lau-troi-mau-do-nau/">SON CEZANNE KHÔNG CHÌ MỀM MÔI LÂU TRÔI, MÀU ĐỎ NÂU</a></div>
			<div class="price"> <span class="woocommerce-Price-amount amount">230.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></div>
		</div>
	</div>
</div>

<div class="item">
	<div class="item-product">
		<div class="item-thumb">
			<div class="product-thumb-hover">
				<a href="http://mhs.wpdemo.test/san-pham/vien-uong-bo-mau-ho-tro-tien-man-kinh-rubina/" class="woocommerce-LoopProduct-link">
					<img width="1000" height="1000" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" data-lazy-src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_d3875ba2647ac0d5783c45e0c010d3c3-100x100.jpeg" />
					<noscript><img width="1000" height="1000" src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_d3875ba2647ac0d5783c45e0c010d3c3-100x100.jpeg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" /></noscript>
				</a>
			</div>
		</div>
		<div class="content-bottom">
			<div class="item-title"> <a href="http://mhs.wpdemo.test/san-pham/vien-uong-bo-mau-ho-tro-tien-man-kinh-rubina/">Viên uống bổ máu hỗ trợ tiền mãn kinh Rubina</a></div>
			<div class="price"> <span class="woocommerce-Price-amount amount">549.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></div>
		</div>
	</div>
</div>

<div class="item">
	<div class="item-product">
		<div class="item-thumb">
			<div class="product-thumb-hover">
				<a href="http://mhs.wpdemo.test/san-pham/thuoc-bo-phoi-kobayashi/" class="woocommerce-LoopProduct-link">
					<img width="1000" height="1000" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" data-lazy-src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_b782d99e5c725e40a721c1638772aa3d-100x100.jpeg" />
					<noscript><img width="1000" height="1000" src="http://mhs.wpdemo.test/wp-content/uploads/sites/7/2021/02/kiotviet_b782d99e5c725e40a721c1638772aa3d-100x100.jpeg" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" /></noscript>
				</a>
			</div>
		</div>
		<div class="content-bottom">
			<div class="item-title"> <a href="http://mhs.wpdemo.test/san-pham/thuoc-bo-phoi-kobayashi/">Thuốc bổ phổi Kobayashi</a></div>
			<div class="price"> <span class="woocommerce-Price-amount amount">549.000&nbsp;<span class="woocommerce-Price-currencySymbol">&#8363;</span></span></div>
		</div>
	</div>
</div>


<?php }
}
