<?php
/* Smarty version 3.1.33, created on 2021-02-03 08:38:50
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\banner\block_banner_2.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_601a611a986d97_33390999',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd7e9486ff8fad836454313cd1e601c9dedf15203' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\banner\\block_banner_2.tpl',
      1 => 1612337152,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_601a611a986d97_33390999 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="bwp-image">
    
        <?php if ($_smarty_tpl->tpl_vars['BANNER']->value['type_image_link']) {?>
        <a id="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link_id'];?>
" class="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link_class'];?>
>" rel="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link_rel'];?>
" href="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link'];?>
" target="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['target'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link_title'];?>
">
            <img width="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['img_width'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['alt'];?>
" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" data-lazy-src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" class=" | "/>
            <noscript><img src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['alt'];?>
"></noscript>
        </a>
    <?php }?>
    
        <?php if ($_smarty_tpl->tpl_vars['BANNER']->value['type_image']) {?>
        <img width="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['img_width'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['alt'];?>
" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"  data-lazy-src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" class=" | "/>
        <noscript><img src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['alt'];?>
"></noscript>
    <?php }?>
    
    </div>
<?php }
}
