<?php
/* Smarty version 3.1.33, created on 2021-02-03 08:38:50
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_product_by_cat\block_product_by_cat_.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_601a611af241d2_56580173',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0d4672864c0852be2a10942d6f2e7957c87f809f' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_product_by_cat\\block_product_by_cat_.tpl',
      1 => 1611799836,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_601a611af241d2_56580173 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="elementor-section elementor-top-section elementor-element elementor-element-7478cc7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="7478cc7" data-element_type="section">
    <div class="elementor-container elementor-column-gap-extended">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-7ae683e" data-id="7ae683e" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-2ac1734 title-block-9 elementor-widget elementor-widget-heading" data-id="2ac1734" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['CAT']->value->link;?>
"><?php echo $_smarty_tpl->tpl_vars['CAT']->value->name;?>
</a>
                                </h2>
                            </div>
                        </div>
                        
                                                <div class="elementor-element elementor-element-a9ddd8e elementor-widget elementor-widget-bwp_product_list" data-id="a9ddd8e" data-element_type="widget" data-widget_type="bwp_product_list.default">
                            <div class="elementor-widget-container">
                                <div id="bwp_default_10788761941611795676" class="bwp_product_list default  no-title">
                                    <div class="content products-list grid row">
                                        <?php echo $_smarty_tpl->tpl_vars['LIST_PRODUCT']->value;?>

                                    </div>
                                </div>
                            </div>
                        </div>
                                            </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php }
}
