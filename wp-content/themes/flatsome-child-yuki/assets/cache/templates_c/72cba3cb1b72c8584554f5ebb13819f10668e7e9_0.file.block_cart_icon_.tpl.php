<?php
/* Smarty version 3.1.33, created on 2021-02-22 01:23:38
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_cart_icon_.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6033079a9f1617_61160841',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '72cba3cb1b72c8584554f5ebb13819f10668e7e9' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_cart_icon_.tpl',
      1 => 1611909154,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6033079a9f1617_61160841 (Smarty_Internal_Template $_smarty_tpl) {
?>

<a class="dropdown-toggle cart-icon" data-toggle="dropdown" data-hover="dropdown" data-delay="0" href="<?php echo $_smarty_tpl->tpl_vars['LINK']->value;?>
" title="<?php echo $_smarty_tpl->tpl_vars['TITLE']->value;?>
">
	<div class="icons-cart cart_refragment">
		<i class="fa fa-shopping-cart"></i>
		<span class="cart-count">
			<span class="cart_count"></span>
		</span>
	</div>
</a>



<?php }
}
