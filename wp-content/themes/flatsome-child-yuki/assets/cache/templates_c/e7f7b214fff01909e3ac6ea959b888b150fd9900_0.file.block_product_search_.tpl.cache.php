<?php
/* Smarty version 3.1.33, created on 2021-02-22 04:12:08
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_product_search_.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60332f18261a71_34619054',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e7f7b214fff01909e3ac6ea959b888b150fd9900' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_product_search_.tpl',
      1 => 1613960058,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60332f18261a71_34619054 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->compiled->nocache_hash = '188024478760332f18173712_33925881';
?>


<form role="search" method="get" class="search-from" action="<?php echo $_smarty_tpl->tpl_vars['FORM_ACTION']->value;?>
">
    <div class="search-box">
                <input type="text" value="<?php echo $_smarty_tpl->tpl_vars['SEARCH_VALUE']->value;?>
" name="s" id="ss" class="input-search s" placeholder="<?php echo $_smarty_tpl->tpl_vars['SEARCH_PLACEHOLDER']->value;?>
" />
                <div class="result-search-products-content">
            <ul class="result-search-products"></ul>
        </div>
    </div>
    <button id="searchsubmit2" class="btn" type="submit"> <span class="search-icon"> <i class="fa fa-search"></i> </span> <span>search</span> </button>
        <input type="hidden" name="post_type" value="product" />

    <?php if (isset($_smarty_tpl->tpl_vars['ICL_LANGUAGE_CODE']->value)) {?>
        <input type="hidden" name="lang" value="<?php echo $_smarty_tpl->tpl_vars['ICL_LANGUAGE_CODE']->value;?>
" />
    <?php }?>
</form>
<?php }
}
