<?php
/* Smarty version 3.1.33, created on 2021-02-22 07:28:36
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_addtocart_btn\block_addtocart_btn_home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60335d242175e2_04835355',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e051beb13b3505be600c20d941a0c160c2c6997e' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_addtocart_btn\\block_addtocart_btn_home.tpl',
      1 => 1611819800,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60335d242175e2_04835355 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['product']->value->get_type() == 'variable') {?>
    <?php echo do_shortcode('[block_quickview_btn]');?>

<?php } else { ?>
    <a title="<?php echo $_smarty_tpl->tpl_vars['product']->value->add_to_cart_text();?>
" href="<?php echo $_smarty_tpl->tpl_vars['product']->value->add_to_cart_url();?>
" data-quantity="<?php echo $_smarty_tpl->tpl_vars['args']->value['quantity'];?>
" class="<?php echo $_smarty_tpl->tpl_vars['args']->value['class'];?>
 | add_to_cart_button " <?php echo $_smarty_tpl->tpl_vars['args']->value['attributes'];?>
>
        <?php echo $_smarty_tpl->tpl_vars['product']->value->add_to_cart_text();?>

    </a>
<?php }?>

<?php }
}
