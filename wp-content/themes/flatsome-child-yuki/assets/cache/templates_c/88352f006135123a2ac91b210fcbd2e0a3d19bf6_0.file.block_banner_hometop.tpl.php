<?php
/* Smarty version 3.1.33, created on 2021-02-22 03:12:14
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\banner\block_banner_hometop.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6033210ead4705_87718023',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '88352f006135123a2ac91b210fcbd2e0a3d19bf6' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\banner\\block_banner_hometop.tpl',
      1 => 1613963529,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6033210ead4705_87718023 (Smarty_Internal_Template $_smarty_tpl) {
?>

<rs-slide data-key="rs-<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" data-title="Slide" data-thumb="" data-anim="ei:d;eo:d;s:d;r:default;t:papercut;sl:d;">
    <img loading="lazy" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201101%20531'%3E%3C/svg%3E" title="slider-13" width="1101" height="531" data-bg="f:auto;" class="rev-slidebg" data-no-retina data-lazy-src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
">
    <noscript><img loading="lazy" src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" title="slider-13" width="1101" height="531" data-bg="f:auto;" class="rev-slidebg" data-no-retina></noscript>
</rs-slide>


<div class="elementor-element elementor-element-b316f60 elementor-widget elementor-widget-bwp_image" data-id="b316f60" data-element_type="widget" data-widget_type="bwp_image.default">
    <div class="elementor-widget-container">
        <div class="bwp-widget-banner layout-4 style1">
            <div class="bg-banner">
                <div class="banner-wrapper banners">
                    <div class="bwp-image">
                        <a href="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link'];?>
">
                            <img src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" alt="Banner Image" class=""  data-lazy-src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
">
                            <noscript><img src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['alt'];?>
"></noscript>
                        </a>
                    </div>
                    <div class="banner-wrapper-infor">
                        <div class="info">
                            <div class="content">
                                <h3 class="title-banner"><?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link_title'];?>
</h3>
                                <a class="button" href="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link'];?>
"><?php echo $_smarty_tpl->tpl_vars['BANNER']->value['info'];?>
</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php }
}
