<?php
/* Smarty version 3.1.33, created on 2021-02-03 08:38:50
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\menu\block_menu_desktop.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_601a611a7387f4_68421620',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dd7297ac4381d41d2b05d0c5f4fe3e836731a0d4' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\menu\\block_menu_desktop.tpl',
      1 => 1612328215,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_601a611a7387f4_68421620 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1232963379601a611a7368f6_05759713', "TOP_MENU");
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_506838379601a611a7370c5_48170034', "SUB");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "block_menu_desktop_tpl.tpl");
}
/* {block "TOP_MENU"} */
class Block_1232963379601a611a7368f6_05759713 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'TOP_MENU' => 
  array (
    0 => 'Block_1232963379601a611a7368f6_05759713',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


<?php
}
}
/* {/block "TOP_MENU"} */
/* {block "SUB"} */
class Block_506838379601a611a7370c5_48170034 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'SUB' => 
  array (
    0 => 'Block_506838379601a611a7370c5_48170034',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <ul class="sub-menu">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['SUB_MENU']->value, 'SUB');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['SUB']->value) {
?>
            <li  class="level-1 menu-item-14429      menu-item menu-item-type-custom menu-item-object-custom  std-menu      " >
                <a href="<?php echo $_smarty_tpl->tpl_vars['SUB']->value->url;?>
"><?php echo $_smarty_tpl->tpl_vars['SUB']->value->title;?>
</a>
            </li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </ul>
<?php
}
}
/* {/block "SUB"} */
}
