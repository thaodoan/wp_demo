<?php
/* Smarty version 3.1.33, created on 2021-02-03 08:38:50
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_product_cat\block_product_cat_home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_601a611a53e630_60662108',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8656b753ab23127664ffc12353405593670c6f8a' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_product_cat\\block_product_cat_home.tpl',
      1 => 1612323937,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_601a611a53e630_60662108 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1131043714601a611a5390d3_18517655', "SUB");
$_smarty_tpl->inheritance->endChild($_smarty_tpl, "block_product_cat_".((string)$_smarty_tpl->tpl_vars['tpl']->value)."_tpl.tpl");
}
/* {block "SUB"} */
class Block_1131043714601a611a5390d3_18517655 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'SUB' => 
  array (
    0 => 'Block_1131043714601a611a5390d3_18517655',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    
    <div class="sub-menu">
        <div data-elementor-type="wp-post" data-elementor-id="12585" class="elementor elementor-12585" data-elementor-settings="[]">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <section class="elementor-section elementor-top-section elementor-element elementor-element-a0d7f4b wpb-p-0 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="a0d7f4b" data-element_type="section" data-settings="">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-fc440f5 wpb-p-0" data-id="fc440f5" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['SUB_CAT']->value, 'SUB');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['SUB']->value) {
?>
                                                <div class="elementor-element elementor-element-26de65d title-vertical elementor-widget elementor-widget-heading" data-id="26de65d" data-element_type="widget" data-widget_type="heading.default">
                                                    <div class="elementor-widget-container">
                                                        <a href="<?php echo $_smarty_tpl->tpl_vars['SUB']->value->link;?>
" title="<?php echo $_smarty_tpl->tpl_vars['SUB']->value->name;?>
" class=" | ">
                                                            <h2 class="elementor-heading-title elementor-size-default"><?php echo $_smarty_tpl->tpl_vars['SUB']->value->name;?>
 (<?php echo $_smarty_tpl->tpl_vars['SUB']->value->count;?>
)</h2>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    <?php
}
}
/* {/block "SUB"} */
}
