<?php
/* Smarty version 3.1.33, created on 2021-02-22 07:28:36
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_product_item\block_product_item_group_home.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60335d2464dd42_44574099',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a40c1360eb2084f8d082dd44866e723554a4a848' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_product_item\\block_product_item_group_home.tpl',
      1 => 1613978766,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60335d2464dd42_44574099 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="item-product col-xl-3 col-lg-3 col-md-4 col-6">
	<div class="products-entry content-product2 clearfix product-wapper">
		<div class="products-thumb">
			<div class='product-lable'> <?php echo do_shortcode('[block_sale_flash]');?>
</div>
			<div class="product-thumb-hover">
				<a href="<?php echo $_smarty_tpl->tpl_vars['LINK']->value;?>
" class="woocommerce-LoopProduct-link">
					<img width="<?php echo $_smarty_tpl->tpl_vars['THUMB']->value->img_width;?>
" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" srcset="<?php echo $_smarty_tpl->tpl_vars['THUMB']->value->img_srcset;?>
" sizes="(max-width: 576px) 50vw, 270px" alt="<?php echo $_smarty_tpl->tpl_vars['THUMB']->value->img_alt;?>
" title="<?php echo $_smarty_tpl->tpl_vars['TITLE']->value;?>
" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail " loading="lazy" data-lazy-src="<?php echo $_smarty_tpl->tpl_vars['THUMB']->value->img_src;?>
"/>
				</a>
			</div>
			<div class='product-button'>
							</div>
		</div>
		<div class="products-content">
			<div class="contents">
				<h3 class="product-title"><a href="<?php echo $_smarty_tpl->tpl_vars['LINK']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['TITLE']->value;?>
</a></h3>
				<span class="price">
					<?php echo $_smarty_tpl->tpl_vars['PRICE']->value;?>

				</span>
			</div>
		</div>
	</div>
</div><?php }
}
