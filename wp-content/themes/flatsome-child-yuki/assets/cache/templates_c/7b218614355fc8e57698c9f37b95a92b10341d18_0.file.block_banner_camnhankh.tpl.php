<?php
/* Smarty version 3.1.33, created on 2021-02-22 04:31:57
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\banner\block_banner_camnhankh.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_603333bd8d8729_56479816',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7b218614355fc8e57698c9f37b95a92b10341d18' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\banner\\block_banner_camnhankh.tpl',
      1 => 1613968309,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_603333bd8d8729_56479816 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="testimonial-content">
    <div class="item">
        <div class="testimonial-item">
            <div class="star star-5"></div>
            <div class="testimonial-customer-position">
                <p class="post-excerpt"><?php echo $_smarty_tpl->tpl_vars['BANNER']->value['info'];?>
</p>
            </div>
        </div>
        <div class="testimonial-info">
            <div class="testimonial-image">
                <img width="95" height="95" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2095%2095'%3E%3C/svg%3E" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" loading="lazy" data-lazy-src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" />
                <noscript><img width="95" height="95" src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" loading="lazy" /></noscript>
            </div>
            <div class="content">
                <h2 class="testimonial-customer-name"><?php echo $_smarty_tpl->tpl_vars['BANNER']->value['title'];?>
</h2>
                <div class="testimonial-job"><?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link_class'];?>
</div>
            </div>
        </div>
    </div>
</div>
<?php }
}
