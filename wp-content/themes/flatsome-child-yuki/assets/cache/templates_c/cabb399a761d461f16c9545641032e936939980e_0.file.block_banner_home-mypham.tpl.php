<?php
/* Smarty version 3.1.33, created on 2021-02-22 08:50:52
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\banner\block_banner_home-mypham.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_6033706c9a47f5_96040033',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cabb399a761d461f16c9545641032e936939980e' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\banner\\block_banner_home-mypham.tpl',
      1 => 1613983844,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6033706c9a47f5_96040033 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="bwp-image">
    <a href="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link'];?>
">
        <img src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2095%2095'%3E%3C/svg%3E" alt="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" class="" loading="lazy" data-lazy-src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
">
        <noscript><img src="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['src_full'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['alt'];?>
"></noscript>
    </a>
</div>
<div class="banner-wrapper-infor">
    <div class="info">
        <div class="content">
            <h3 class="title-banner"><?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link_title'];?>
</h3>
            <a class="button" href="<?php echo $_smarty_tpl->tpl_vars['BANNER']->value['link'];?>
"><?php echo $_smarty_tpl->tpl_vars['BANNER']->value['info'];?>
</a>
        </div>
    </div>
</div><?php }
}
