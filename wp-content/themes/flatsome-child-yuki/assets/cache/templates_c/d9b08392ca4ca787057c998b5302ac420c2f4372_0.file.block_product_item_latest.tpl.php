<?php
/* Smarty version 3.1.33, created on 2021-02-22 07:32:01
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_product_item\block_product_item_latest.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60335df19533f6_07562509',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd9b08392ca4ca787057c998b5302ac420c2f4372' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_product_item\\block_product_item_latest.tpl',
      1 => 1613979118,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60335df19533f6_07562509 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="item">
	<div class="item-product">
		<div class="item-thumb">
			<div class="product-thumb-hover">
				<a href="<?php echo $_smarty_tpl->tpl_vars['LINK']->value;?>
" class="woocommerce-LoopProduct-link">
					<img width="1000" height="1000" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" data-lazy-src="<?php echo $_smarty_tpl->tpl_vars['THUMB']->value->img_src;?>
" />
					<noscript><img width="1000" height="1000" src="<?php echo $_smarty_tpl->tpl_vars['THUMB']->value->img_src;?>
" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" /></noscript>
				</a>
			</div>
		</div>
		<div class="content-bottom">
			<div class="item-title"> <a href="<?php echo $_smarty_tpl->tpl_vars['LINK']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['TITLE']->value;?>
</a></div>
			<div class="price"> <?php echo $_smarty_tpl->tpl_vars['PRICE']->value;?>
</div>
		</div>
	</div>
</div>
<?php }
}
