var block_product_recent = $('#block_product_recent');
if(typeof ajax_product_data !== 'undefined'){
    var product_id = ajax_product_data.current_id;
}else{
    var product_id = null;
}

$.ajax({ // Hàm ajax
    type : "POST", //Phương thức truyền post hoặc get
    dataType : "html", //Dạng dữ liệu trả về xml, json, script, or html
    url : flatsomeVars.ajaxurl, // Nơi xử lý dữ liệu
    data : {
        action: "product_recent", //Tên action, dữ liệu gởi lên cho server
        ajax_front: true,
        current_id: product_id,
    },
    async: true,
    beforeSend: function(){
    },
    success: function(response) {
        block_product_recent.html(response); // Đổ dữ liệu trả về vào thẻ &lt;div class="display-post"&gt;&lt;/div&gt;
    },
    error: function( jqXHR, textStatus, errorThrown ){
        console.log( 'The following error occured: ' + textStatus, errorThrown );
    }
});
