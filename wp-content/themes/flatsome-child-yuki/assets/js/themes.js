$(document).ready(function($) {
    $('.show_onloaded').css('visibility', 'visible');
});

function display_main_message(message){
    $main_message = $('#main-message');
    $main_message.html(message);
    $main_message.fadeIn();
    setTimeout(function(){
        $main_message.fadeOut();
    }, 2500);
}