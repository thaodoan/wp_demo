<?php
/*
* Template name: MHS - Flatsome Child - HomePage
*/
?>
<?php
echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
echo do_shortcode('[header_extend]');?>


<section class="elementor-section elementor-inner-section elementor-element elementor-element-0cfe420 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="0cfe420" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-76e2ba2 box-slider2-left" data-id="76e2ba2" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-f3cb771 elementor-widget elementor-widget-slider_revolution" data-id="f3cb771" data-element_type="widget" data-widget_type="slider_revolution.default">
                            <div class="elementor-widget-container">
                                <div class="wp-block-themepunch-revslider">
                                    <p class="rs-p-wp-fix"></p>
                                    <?= do_shortcode('[block_banner id="1" num="1" tpl="1"]'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-8aa3937" data-id="8aa3937" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <?= do_shortcode('[block_banner id="12" num="2" tpl="hometop"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>
</div>
</div>
</div>
</div>
</section>

<section class="elementor-section elementor-top-section elementor-element elementor-element-bd81921 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="bd81921" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-71c26ec box-vertical2" data-id="71c26ec" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-ae3abe8 wpb-col-sm-60 box-policy elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="ae3abe8" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-2467b7f" data-id="2467b7f" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-4cb9255 elementor-position-left elementor-vertical-align-middle policy-respon elementor-view-default elementor-widget elementor-widget-icon-box" data-id="4cb9255" data-element_type="widget" data-widget_type="icon-box.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                                                                                    <span class="elementor-icon elementor-animation-wobble-horizontal" >
                                                                                       <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                                                          <path d="M119.47 337.07a51.25 51.25 0 00-51.2 51.2c0 28.23 22.96 51.2 51.2 51.2s51.2-22.97 51.2-51.2a51.25 51.25 0 00-51.2-51.2zm0 85.33a34.17 34.17 0 01-34.14-34.13c0-18.83 15.31-34.14 34.14-34.14s34.13 15.31 34.13 34.14a34.17 34.17 0 01-34.13 34.13zM409.6 337.07a51.25 51.25 0 00-51.2 51.2 51.25 51.25 0 0051.2 51.2 51.25 51.25 0 0051.2-51.2 51.25 51.25 0 00-51.2-51.2zm0 85.33a34.17 34.17 0 01-34.13-34.13 34.17 34.17 0 0134.13-34.14 34.17 34.17 0 0134.13 34.14 34.17 34.17 0 01-34.13 34.13z"/>
                                                                                          <path d="M510.64 289.78l-76.8-119.46a8.54 8.54 0 00-7.17-3.92H332.8a8.53 8.53 0 00-8.53 8.53v213.34a8.52 8.52 0 008.53 8.53h34.13v-17.07h-25.6V183.47h80.68l72.92 113.44v82.82h-42.66v17.07h51.2a8.52 8.52 0 008.53-8.53V294.4c0-1.63-.47-3.24-1.36-4.62z"/>
                                                                                          <path d="M375.47 277.33V217.6h68.26v-17.07h-76.8a8.53 8.53 0 00-8.53 8.54v76.8a8.52 8.52 0 008.53 8.53h128v-17.07H375.47zM332.8 106.67H8.53A8.54 8.54 0 000 115.2v273.07a8.53 8.53 0 008.53 8.53H76.8v-17.07H17.07v-256h307.2v256H162.13v17.07H332.8a8.52 8.52 0 008.53-8.53V115.2a8.53 8.53 0 00-8.53-8.53z"/>
                                                                                          <path d="M8.53 345.6h51.2v17.07H8.53zM179.2 345.6h145.07v17.07H179.2zM469.33 345.6h34.14v17.07h-34.14zM34.13 140.8H332.8v17.07H34.13zM110.93 379.73H128v17.07h-17.07zM401.07 379.73h17.06v17.07h-17.06zM34.13 72.53H153.6V89.6H34.13zM0 72.53h17.07V89.6H0z"/>
                                                                                       </svg>
                                                                                    </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                                <h3 class="elementor-icon-box-title"> <span >Giao hàng tận nơi</span></h3>
                                                                <p class="elementor-icon-box-description">On order over $49.86</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-6cf89d5 elementor-position-left elementor-vertical-align-middle policy-respon elementor-view-default elementor-widget elementor-widget-icon-box" data-id="6cf89d5" data-element_type="widget" data-widget_type="icon-box.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                                                                                    <span class="elementor-icon elementor-animation-wobble-horizontal" >
                                                                                       <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                                                          <g>
                                                                                             <g>
                                                                                                <g>
                                                                                                   <path d="M498.409,175.706L336.283,13.582c-8.752-8.751-20.423-13.571-32.865-13.571c-12.441,0-24.113,4.818-32.865,13.569     L13.571,270.563C4.82,279.315,0,290.985,0,303.427c0,12.442,4.82,24.114,13.571,32.864l19.992,19.992     c0.002,0.001,0.003,0.003,0.005,0.005c0.002,0.002,0.004,0.004,0.006,0.006l134.36,134.36H149.33     c-5.89,0-10.666,4.775-10.666,10.666c0,5.89,4.776,10.666,10.666,10.666h59.189c0.014,0,0.027,0.001,0.041,0.001     s0.027-0.001,0.041-0.001l154.053,0.002c5.89,0,10.666-4.776,10.666-10.666c0-5.891-4.776-10.666-10.666-10.666l-113.464-0.002     L498.41,241.434C516.53,223.312,516.53,193.826,498.409,175.706z M483.325,226.35L226.341,483.334     c-4.713,4.712-11.013,7.31-17.742,7.32h-0.081c-6.727-0.011-13.025-2.608-17.736-7.32L56.195,348.746L302.99,101.949     c4.165-4.165,4.165-10.919,0-15.084c-4.166-4.165-10.918-4.165-15.085,0.001L41.11,333.663l-12.456-12.456     c-4.721-4.721-7.321-11.035-7.321-17.779c0-6.744,2.6-13.059,7.322-17.781L285.637,28.665c4.722-4.721,11.037-7.321,17.781-7.321     c6.744,0,13.059,2.6,17.781,7.322l57.703,57.702l-246.798,246.8c-4.165,4.164-4.165,10.918,0,15.085     c2.083,2.082,4.813,3.123,7.542,3.123c2.729,0,5.459-1.042,7.542-3.124l246.798-246.799l89.339,89.336     C493.128,200.593,493.127,216.546,483.325,226.35z"></path>
                                                                                                   <path d="M262.801,308.064c-4.165-4.165-10.917-4.164-15.085,0l-83.934,83.933c-4.165,4.165-4.165,10.918,0,15.085     c2.083,2.083,4.813,3.124,7.542,3.124c2.729,0,5.459-1.042,7.542-3.124l83.934-83.933     C266.966,318.982,266.966,312.229,262.801,308.064z"></path>
                                                                                                   <path d="M228.375,387.741l-34.425,34.425c-4.165,4.165-4.165,10.919,0,15.085c2.083,2.082,4.813,3.124,7.542,3.124     c2.731,0,5.459-1.042,7.542-3.124l34.425-34.425c4.165-4.165,4.165-10.919,0-15.085     C239.294,383.575,232.543,383.575,228.375,387.741z"></path>
                                                                                                   <path d="M260.054,356.065l-4.525,4.524c-4.166,4.165-4.166,10.918-0.001,15.085c2.082,2.083,4.813,3.125,7.542,3.125     c2.729,0,5.459-1.042,7.541-3.125l4.525-4.524c4.166-4.165,4.166-10.918,0.001-15.084     C270.974,351.901,264.219,351.9,260.054,356.065z"></path>
                                                                                                   <path d="M407.073,163.793c-2-2-4.713-3.124-7.542-3.124c-2.829,0-5.541,1.124-7.542,3.124l-45.255,45.254     c-2,2.001-3.124,4.713-3.124,7.542s1.124,5.542,3.124,7.542l30.17,30.167c2.083,2.083,4.813,3.124,7.542,3.124     c2.731,0,5.459-1.042,7.542-3.124l45.253-45.252c4.165-4.165,4.165-10.919,0-15.084L407.073,163.793z M384.445,231.673     l-15.085-15.084l30.17-30.169l15.084,15.085L384.445,231.673z"></path>
                                                                                                   <path d="M320.339,80.186c2.731,0,5.461-1.042,7.543-3.126l4.525-4.527c4.164-4.166,4.163-10.92-0.003-15.084     c-4.165-4.164-10.92-4.163-15.084,0.003l-4.525,4.527c-4.164,4.166-4.163,10.92,0.003,15.084     C314.881,79.146,317.609,80.186,320.339,80.186z"></path>
                                                                                                   <path d="M107.215,358.057l-4.525,4.525c-4.165,4.164-4.165,10.918,0,15.085c2.083,2.082,4.813,3.123,7.542,3.123     s5.459-1.041,7.542-3.123l4.525-4.525c4.165-4.166,4.165-10.92,0-15.085C118.133,353.891,111.381,353.891,107.215,358.057z"></path>
                                                                                                </g>
                                                                                             </g>
                                                                                          </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                          <g> </g>
                                                                                       </svg>
                                                                                    </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                                <h3 class="elementor-icon-box-title"> <span >Thanh toán khi nhận hàng</span></h3>
                                                                <p class="elementor-icon-box-description">Secured information</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-af93533 elementor-position-left elementor-vertical-align-middle policy-respon elementor-view-default elementor-widget elementor-widget-icon-box" data-id="af93533" data-element_type="widget" data-widget_type="icon-box.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                                                                                    <span class="elementor-icon elementor-animation-wobble-horizontal" >
                                                                                       <svg xmlns="http://www.w3.org/2000/svg" width="511pt" height="511pt" viewBox="0 1 512 511">
                                                                                          <path d="M506.81 111.23L307.41 1.73a10 10 0 00-9.63 0L222.62 43a10.03 10.03 0 00-4.3 2.36L98.39 111.23a10 10 0 00-5.19 8.77v132.43a132.37 132.37 0 00-54.2 32.9A133.09 133.09 0 0013.5 437.85a10 10 0 1017.97-8.78 113.09 113.09 0 0121.67-129.58c21.35-21.36 49.74-33.12 79.94-33.12s58.6 11.76 79.95 33.12c21.35 21.35 33.11 49.74 33.11 79.94s-11.76 58.59-33.12 79.94a113.09 113.09 0 01-129.58 21.67A10 10 0 1074.66 499a133.12 133.12 0 00152.5-25.5 133.03 133.03 0 0027.53-39.92l43.1 23.66a9.98 9.98 0 009.62 0l199.4-109.49a10 10 0 005.19-8.77v-69.5a10 10 0 10-20 0v63.59l-179.38 98.5V235.38l59.2-32.5v51.52a10 10 0 0014.82 8.77l42.73-23.52a10 10 0 005.18-8.76v-62.46L492 136.89v52.6a10 10 0 1020 0V120a10 10 0 00-5.19-8.76zM302.6 21.91L481.22 120l-56.34 30.94-178.63-98.1zm0 196.17L123.96 120l58.42-32.08L361 186zm79.2-43.48L203.16 76.5l22.3-12.25 178.64 98.08zM227.17 285.34a132.19 132.19 0 00-94.09-38.98c-6.72 0-13.36.5-19.89 1.47V136.9l179.44 98.54V431.6l-31.15-17.1a133.7 133.7 0 004.66-35.08c0-35.54-13.84-68.96-38.97-94.08zm187.38-60.35l-22.73 12.51v-45.6l22.73-12.48zm0 0"/>
                                                                                          <path d="M502 219.44c-2.63 0-5.21 1.07-7.07 2.93a10.07 10.07 0 00-2.93 7.07c0 2.63 1.07 5.2 2.93 7.07 1.86 1.86 4.44 2.93 7.07 2.93s5.21-1.07 7.07-2.93a10.07 10.07 0 002.93-7.07c0-2.63-1.07-5.21-2.93-7.07a10.08 10.08 0 00-7.07-2.93zm0 0M99.46 389.42a10 10 0 007.07-17.07l-6.93-6.93h59.1c14.34 0 26 11.66 26 26 0 14.33-11.66 26-26 26h-35.02a10 10 0 100 20h35.02c25.37 0 46-20.64 46-46s-20.63-46-46-46H99.6l6.93-6.93a10 10 0 00-14.14-14.14l-24 24a10 10 0 000 14.14l24 24a9.97 9.97 0 007.07 2.93zm0 0M46.07 476.45a9.95 9.95 0 01-7.64-3.56l-.02-.02a10 10 0 117.66 3.58zm0 0"/>
                                                                                       </svg>
                                                                                    </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                                <h3 class="elementor-icon-box-title"> <span >Kiếm tra hàng</span></h3>
                                                                <p class="elementor-icon-box-description">Return over 30 days</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="elementor-element elementor-element-1a6f2cf elementor-position-left elementor-vertical-align-middle policy-respon elementor-view-default elementor-widget elementor-widget-icon-box" data-id="1a6f2cf" data-element_type="widget" data-widget_type="icon-box.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="elementor-icon-box-wrapper">
                                                            <div class="elementor-icon-box-icon">
                                                                                    <span class="elementor-icon elementor-animation-wobble-horizontal" >
                                                                                       <svg xmlns="http://www.w3.org/2000/svg" id="Capa_2" height="512" viewBox="0 0 512 512" width="512">
                                                                                          <g>
                                                                                             <path d="m71.858 418.114 48.142 24.067v59.819c0 5.523 4.478 10 10 10h106c5.522 0 10-4.477 10-10v-136.55c0-30.637-22.983-56.045-53.391-59.093-.118-.033-42.3-6.618-61.87-26.189l-22.458-22.448c-7.556-7.558-17.6-11.72-28.281-11.72v-80c0-22.056-17.944-40-40-40s-40 17.944-40 40v135.84c0 49.212 27.798 94.239 71.858 116.274zm-51.858-252.114c0-11.028 8.972-20 20-20s20 8.972 20 20v85.361c-12.196 7.052-20 20.2-20 34.639 0 10.68 4.162 20.723 11.719 28.28l51.21 51.22c3.903 3.905 10.236 3.907 14.142.001 3.905-3.905 3.906-10.236.001-14.142l-51.21-51.221c-3.78-3.779-5.862-8.8-5.862-14.138 0-8.464 5.357-16.044 13.332-18.861.003-.001.006-.003.009-.004h.001c6.896-2.444 15.077-.994 20.799 4.728l22.458 22.449c23.186 23.185 73.843 31.91 73.944 31.938 20.214 2.026 35.457 18.879 35.457 39.2v126.55h-86v-56c0-3.788-2.141-7.251-5.528-8.944l-53.669-26.83c-37.504-18.757-60.803-56.456-60.803-98.386z"></path>
                                                                                             <path d="m136 246h240c5.522 0 10-4.477 10-10v-226c0-5.523-4.478-10-10-10h-240c-5.522 0-10 4.477-10 10v226c0 5.523 4.478 10 10 10zm90-226h60v53.893l-25.975-12.855c-2.82-1.397-6.136-1.382-8.944.037l-25.081 12.67zm-80 0h60v70c0 3.473 1.802 6.698 4.761 8.518 2.957 1.819 6.649 1.974 9.748.408l35.127-17.745 35.929 17.782c3.046 1.508 6.722 1.389 9.71-.467 2.938-1.825 4.725-5.038 4.725-8.496v-70h60v206h-220z"></path>
                                                                                             <path d="m336 176h-60c-5.522 0-10 4.477-10 10s4.478 10 10 10h60c5.522 0 10-4.477 10-10s-4.478-10-10-10z"></path>
                                                                                             <circle cx="459" cy="394" r="10"></circle>
                                                                                             <path d="m472 126c-22.056 0-40 17.944-40 40v80c-10.681 0-20.725 4.162-28.279 11.717l-22.462 22.452c-12.3 12.301-43.459 24.337-61.128 26.108l-.668.072c-30.48 3.056-53.463 28.464-53.463 59.101v136.55c0 5.523 4.478 10 10 10h106c5.522 0 10-4.477 10-10v-59.819l32.73-16.362c4.939-2.47 6.942-8.476 4.473-13.417-2.469-4.938-8.474-6.942-13.416-4.473l-38.259 19.125c-3.387 1.695-5.528 5.158-5.528 8.946v56h-86v-126.55c0-20.321 15.243-37.174 35.529-39.208l.669-.072c21.691-2.175 57.605-16.262 73.201-31.857l22.462-22.452c5.724-5.724 13.913-7.166 20.797-4.726h.001c.003.001.006.003.009.004 13.771 4.865 17.917 22.551 7.471 33.001l-51.21 51.22c-3.905 3.906-3.904 10.237.001 14.142 3.904 3.904 10.237 3.905 14.142-.001l51.21-51.22c18.487-18.49 14.636-49.669-8.281-62.92v-85.361c0-11.028 8.972-20 20-20s20 8.972 20 20v135.84c0 18.296-4.617 36.46-13.353 52.528-2.639 4.852-.844 10.924 4.009 13.562 4.851 2.636 10.924.843 13.562-4.009 10.324-18.99 15.781-40.458 15.781-62.081v-135.84c0-22.056-17.944-40-40-40z"></path>
                                                                                          </g>
                                                                                       </svg>
                                                                                    </span>
                                                            </div>
                                                            <div class="elementor-icon-box-content">
                                                                <h3 class="elementor-icon-box-title"> <span >Chính sách đổi trả</span></h3>
                                                                <p class="elementor-icon-box-description">Special offers!</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-559e1cb wpb-col-sm-40 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="559e1cb" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-cd38bb6" data-id="cd38bb6" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-9c9c640 elementor-widget elementor-widget-bwp_testimonial" data-id="9c9c640" data-element_type="widget" data-widget_type="bwp_testimonial.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="bwp-testimonial layout3">
                                                            <div class="block">
                                                                <div class="testimonial-title"></div>
                                                                <div class="block_content">
                                                                    <div id="testimonial_7803579791611731542" class="slick-carousel " data-slidesToScroll="true" data-nav="0" data-dots="1" data-columns4="1" data-columns3="1" data-columns2="1" data-columns1="1" data-columns="1">
                                                                        <?= do_shortcode('[block_banner id="15" num="5" tpl="camnhankh"]'); ?>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-0652f83 wpb-col-sm-60 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="0652f83" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d42a237" data-id="d42a237" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-26031d4 elementor-widget elementor-widget-bwp_image" data-id="26031d4" data-element_type="widget" data-widget_type="bwp_image.default">
                                                    <div class="elementor-widget-container">
                                                        <div class="bwp-widget-banner default style1">
                                                            <div class="bg-banner">
                                                                <div class="banner-wrapper banners">
                                                                    <?= do_shortcode('[block_banner id="14" num="2" tpl="tuyen-dung"]'); ?>
                                                                    <div class="banner-wrapper-infor">
                                                                        <div class="info"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-fd4e882 wpb-col-sm-40 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="fd4e882" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-56a713a" data-id="56a713a" data-element_type="column">
                                        <div class="elementor-column-wrap elementor-element-populated">
                                            <div class="elementor-widget-wrap">
                                                <div class="elementor-element elementor-element-17411d7 elementor-widget elementor-widget-bwp_product_list" data-id="17411d7" data-element_type="widget" data-widget_type="bwp_product_list.default">
                                                    <div class="elementor-widget-container">
                                                        <div id="bwp_featured_13236513501611731542" class="bwp_product_list slider2  ">
                                                            <div class="title-block">
                                                                <h2><?= __('Lastest products', 'woocommerce') ?></h2>
                                                            </div>
                                                            <div class="content-product-list">
                                                                <div class="slider products-list grid slick-carousel" data-slidestoscroll="true" data-dots="false" data-nav="1" data-columns4="1" data-columns3="1" data-columns2="1" data-columns1="1" data-columns1440="1" data-columns="1">
                                                                    <div class="item-products">
                                                                        <?= do_shortcode('[block_product_group  thumb_size="thumb-100x100"]'); ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-54b5ab4 box-slider2 m-t-50" data-id="54b5ab4" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <?= do_shortcode('[block_product_by_cat num_cat="30" num_row="12"]'); ?>
                        <div class="elementor-element elementor-element-f3561f6 elementor-widget elementor-widget-bwp_image" data-id="f3561f6" data-element_type="widget" data-widget_type="bwp_image.default">
                            <div class="elementor-widget-container">
                                <div class="bwp-widget-banner layout-7">
                                    <div class="bg-banner">
                                        <div class="banner-wrapper banners">
                                            <?= do_shortcode('[block_banner id="16" num="1" tpl="home-mypham"]'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>


<section class="elementor-section elementor-top-section elementor-element elementor-element-fcf31a8 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="fcf31a8" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-005f62f" data-id="005f62f" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div class="elementor-element elementor-element-eb531da elementor-widget elementor-widget-bwp_brand" data-id="eb531da" data-element_type="widget" data-widget_type="bwp_brand.default">
                            <div class="elementor-widget-container">
                                <div id="bwp_brand_8431343981611731542" class="bwp-brand default2">
                                    <div class="slider slick-carousel" data-nav="0" data-columns4="3" data-columns3="3" data-columns2="5" data-columns1="7" data-columns1440="8" data-columns="9">
                                        <?= do_shortcode('[block_banner id="17" num="10" tpl="doitac"]'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?php
echo do_shortcode('[footer_extend]');
echo do_shortcode('[wrap_end]');
echo do_shortcode('[get_footer]');
?>