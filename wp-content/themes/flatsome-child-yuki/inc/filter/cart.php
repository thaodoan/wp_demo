<?php
/**
 * Removing attribute values from Product variation title:
 * */
add_filter( 'woocommerce_product_variation_title_include_attributes', 'variation_title_not_include_attributes' );
function variation_title_not_include_attributes( $boolean ){
    if ( ! is_cart() )
        $boolean = false;
    return $boolean;
}

/**
 * Changing the minimum quantity to 2 for all the WooCommerce products
 */
add_filter( 'woocommerce_quantity_input_args', 'pkg_wc_quantity_input_args', 10, 2 );
function pkg_wc_quantity_input_args( $args, $product ) {
    $args['min_value'] = 1;
    $args['step'] = 1;
    return $args;
}

/**
 *  remove field Cart Checkout
 * */
function custom_override_checkout_fields( $fields ) {
    unset( $fields['billing']['billing_last_name'] );
    unset( $fields['billing']['billing_company'] );
    unset( $fields['billing']['billing_country'] );
    unset( $fields['billing']['billing_postcode'] );
    $fields['billing']['billing_email']['required'] = false;
    unset( $fields['billing']['billing_address_2'] );
    //unset( $fields['billing']['billing_city'] );
    //unset( $fields['billing']['billing_state'] );

    $fields['billing']['billing_phone']['priority'] = 11;

    return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields');

/**
 * OVERRIDE: flatsome_override_existing_checkout_fields
 * */
function flatsome_override_existing_checkout_fields_custom( $fields ) {

    // Remove "form-row-wide" class from first_name
    if ( $fields['first_name']['class'][0] == 'form-row-first' ) {
        $fields['first_name']['class'][0] = 'form-row-wide';
    }

    if ( $fields['state']['class'][0] == 'form-row-wide' ) {
        $fields['state']['class'][0] = 'form-row-first';
    }

    if ( $fields['address_2']['class'][0] == 'form-row-wide' ) {
        $fields['address_2']['class'][0] = 'form-row-last';
    }

    // Make sure address 1 and address 2 is on same line
    if ( isset( $fields['address_2'] ) ) {
        $fields['address_1']['class'][] = 'form-row-wide';

        // Remove "form-row-wide" class from address 1 and address 2
        if ( $fields['address_1']['class'][0] == 'form-row-wide' ) {
            unset( $fields['address_1']['class'][0] );
        }
        if ( $fields['address_2']['class'][0] == 'form-row-wide' ) {
            unset( $fields['address_2']['class'][0] );
        }

        // Reveal label.
        if ( isset( $fields['address_2']['label_class'] ) && is_array( $fields['address_2']['label_class'] ) ) {
            $fields['address_2']['label_class'] = array_diff( $fields['address_2']['label_class'], array( 'screen-reader-text' ) );
        }
    }

    // Fix labels for floating labels option
    if ( get_theme_mod( 'checkout_floating_labels', 0 ) ) {
        $fields['address_1']['placeholder'] = __( 'Street address', 'woocommerce' );

        // Set Placeholders
        foreach ( $fields as $key => $value ) {
            if ( isset( $fields[ $key ]['label'] ) && ! isset( $fields[ $key ]['placeholder'] ) ) {
                $fields[ $key ]['placeholder'] = $fields[ $key ]['label'];
            }
        }
    }

    return $fields;
}
add_filter( 'woocommerce_default_address_fields', 'flatsome_override_existing_checkout_fields_custom', 1);

/**
 * Show notice if cart is empty.
 * @since 3.1.0
 */
remove_action( 'woocommerce_cart_is_empty', 'wc_empty_cart_message', 10 );
add_action( 'woocommerce_cart_is_empty', 'empty_cart_message_custom', 10 );
function empty_cart_message_custom() {
    wc_get_template_part('cart/content/cart', 'empty');
}

/**
 * remove payment_method from CART ORDER DETAIL
 * */
add_filter( 'woocommerce_get_order_item_totals', 'adjust_woocommerce_get_order_item_totals' );
function adjust_woocommerce_get_order_item_totals( $totals ) {
    unset($totals['payment_method']);
    //unset($totals['cart_subtotal']);
    return $totals;
}

/**
 * Remove button Continue Shopping from Cart message
 */
add_filter( 'wc_add_to_cart_message_html', function( $string, $product_id = 0 ) {
    $start = strpos( $string, '<a href=' ) ?: 0;
    $end = strpos( $string, '</a>', $start ) ?: 0;
    return substr( $string, $end ) ?: $string;
});
