<?php
/**
 * remove default sort
 */
add_filter( 'woocommerce_catalog_orderby', 'bbloomer_remove_sorting_option_woocommerce_shop' );
function bbloomer_remove_sorting_option_woocommerce_shop( $options ) {
    unset( $options['rating'] );
    unset( $options['popularity'] );
    unset( $options['menu_order'] );
    return $options;
}

