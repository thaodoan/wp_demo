<?php

/**
 * Image thumbnail sizes
 **/
function alx_setup(){
    add_image_size( 'thumb-100x100', 100, 100, true);
    add_image_size('woocommerce_thumbnail_small', 150, 113, true);
}

add_action( 'after_setup_theme', 'alx_setup' );

/**
 * Add crop attribute to 'medium' WordPress default image size
 */
add_action( 'init', 'wpdocs_change_medium_image_size' );
function wpdocs_change_medium_image_size() {
    remove_image_size( 'menu-24x24' );
    remove_image_size( 'menu-36x36' );
    remove_image_size( 'menu-48x48' );
    remove_image_size( '1536x1536' );
    remove_image_size( '2048x2048' );
    remove_image_size( 'woocommerce_single' );
    remove_image_size( 'woocommerce_gallery_thumbnail' );
    remove_image_size( 'shop_catalog' );
    remove_image_size( 'shop_single' );
    remove_image_size( 'shop_thumbnail' );
}

/**
 * Remove default image sizes here.
 * */
function prefix_remove_default_images( $sizes ) {
    unset( $sizes['menu-24x24']);
    unset( $sizes['menu-36x36']);
    unset( $sizes['menu-48x48']);
    unset( $sizes['1536x1536']);
    unset( $sizes['2048x2048']);
    unset( $sizes['woocommerce_single']);
    unset( $sizes['woocommerce_gallery_thumbnail']);
    unset( $sizes['shop_catalog']);
    unset( $sizes['shop_single']);
    unset( $sizes['shop_thumbnail']);
    return $sizes;
}
add_filter( 'intermediate_image_sizes_advanced', 'prefix_remove_default_images' );

/**
 * prevent_copy.
 * */
add_action('wp_footer', 'prevent_copy', 20);
function prevent_copy(){
    ?>
    <script type="text/javascript">
        $(document).ready(function(){
                $('.product-short-description, .product-footer, .entry-content').bind('cut copy paste contextmenu', function (e) {
                    e.preventDefault();
                });
            }
        );
    </script>
    <?php
}