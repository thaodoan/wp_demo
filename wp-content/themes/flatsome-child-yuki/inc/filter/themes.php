<?php
/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


/**
 * CONTACT FORM
 */
function hocwp_setup_theme_change_contact_form_7_recaptcha_language() {
    if ( wp_script_is( 'google-recaptcha', 'registered' ) ) {
        wp_deregister_script( 'google-recaptcha' );
        $url  = 'https://www.google.com/recaptcha/api.js';
        $lang = 'vi';
        if ( function_exists( 'qtranxf_getLanguage' ) ) {
            $lang = qtranxf_getLanguage();
        }
        $url  = add_query_arg( array( 'hl' => $lang, 'onload' => 'recaptchaCallback', 'render' => 'explicit' ), $url );
        wp_register_script( 'google-recaptcha', $url, array(), false, true );
    }
}

add_action( 'wpcf7_enqueue_scripts', 'hocwp_setup_theme_change_contact_form_7_recaptcha_language' );

/**
 * Quality image
 * */
add_filter( 'jpeg_quality', 'jam_custom_jpeg_quality' );
function jam_custom_jpeg_quality( $quality ) {
    return 100;
}
