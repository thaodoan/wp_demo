<?php
/**
 * Login page css
 * */

function wpse_login_styles() {
    wp_enqueue_style( 'wpse-custom-login', GET_STYLESHEET_DIRECTORY_URI . '/assets/css/style_login.css' );
    wp_enqueue_style( 'wpse-custom-login-1', GET_TEMPLATE_DIRECTORY_URI . '/assets/libs/bootstrap/bootstrap.min.css' );
}
add_action( 'login_enqueue_scripts', 'wpse_login_styles' );