<?php
/**
 *  Set custom woocommerce placeholder.png
 */
add_filter ('woocommerce_placeholder_img_src', 'cf_wc_custom_src', 99);

function cf_wc_custom_src(){
    $DIR = wp_upload_dir();
    $src = untrailingslashit( $DIR['baseurl'] ). '/2020/04/placeholder.png';
    return $src;
}

// Change Price Filter Widget Increment
function change_price_filter_step() {
    return 100000;
}
add_filter( 'woocommerce_price_filter_widget_step', 'change_price_filter_step', 10, 3 );

/**
 * Track product views. Always.
 */
function wc_track_product_view_always() {
    if ( ! is_singular( 'product' ) /* xnagyg: remove this condition to run: || ! is_active_widget( false, false, 'woocommerce_recently_viewed_products', true )*/ ) {
        return;
    }

    global $post;

    if ( empty( $_COOKIE['woocommerce_recently_viewed'] ) ) { // @codingStandardsIgnoreLine.
        $viewed_products = array();
    } else {
        $viewed_products = wp_parse_id_list( (array) explode( '|', wp_unslash( $_COOKIE['woocommerce_recently_viewed'] ) ) ); // @codingStandardsIgnoreLine.
    }

    // Unset if already in viewed products list.
    $keys = array_flip( $viewed_products );

    if ( isset( $keys[ $post->ID ] ) ) {
        unset( $viewed_products[ $keys[ $post->ID ] ] );
    }

    $viewed_products[] = $post->ID;

    if ( count( $viewed_products ) > 15 ) {
        array_shift( $viewed_products );
    }

    // Store for session only.
    wc_setcookie( 'woocommerce_recently_viewed', implode( '|', $viewed_products ) );
}
remove_action('template_redirect', 'wc_track_product_view', 20);
add_action( 'template_redirect', 'wc_track_product_view_always', 20 );

/**
 * Track product views. Always.
 */
function devvn_wc_custom_get_price_html( $price, $product ) {
    if ( $product->get_price() == 0 ) {
        if ( $product->is_on_sale() && $product->get_regular_price() ) {
            $regular_price = wc_get_price_to_display( $product, array( 'qty' => 1, 'price' => $product->get_regular_price() ) );

            $price = wc_format_price_range( $regular_price, __( 'Free!', 'woocommerce' ) );
        } else {
            $price = '<span class="amount">' . __( 'Contacts', 'woocommerce' ) . '</span>';
        }
    }
    return $price;
}
add_filter( 'woocommerce_get_price_html', 'devvn_wc_custom_get_price_html', 10, 2 );

/**
 * remove breadcrumb product page
 */
function remove_woo_breadcrumb(){
    remove_action('woocommerce_single_product_summary', 'flatsome_woocommerce_product_breadcrumb', 0);
}
add_action( 'init', 'remove_woo_breadcrumb', 0 );


/**
 * remove breadcrumb 'product-title, product-name'
 */
add_filter( 'woocommerce_get_breadcrumb', 'ed_change_breadcrumb' );

function ed_change_breadcrumb( $breadcrumb ) {

    if(!is_page() && is_singular()){
        array_pop($breadcrumb);
    }

    return $breadcrumb;
}

/**
 * Filter ORDERBY for RELATED PRODUCT
 */
function filter_woocommerce_output_related_products_args( $args ) {
    $args = array(
        'posts_per_page' => 4,
        'columns'        => 4,
        'orderby'        => 'ID', // @codingStandardsIgnoreLine.
        'order'        => 'DESC',
    );
    return $args;
};

add_filter( 'woocommerce_output_related_products_args', 'filter_woocommerce_output_related_products_args', 10, 1 );

/**
 * remove reset variations_link
 */
add_filter('woocommerce_reset_variations_link', '__return_empty_string');


/**
 * Checkout page - remove "Billing" from error
 */
function customize_wc_errors( $error ) {
    if ( strpos( $error, 'Billing ' ) !== false ) {
        $error = str_replace("Billing ", "", $error);
    }
    return $error;
}
add_filter( 'woocommerce_add_error', 'customize_wc_errors' );