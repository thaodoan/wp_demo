<?php

/** load init PATH */
require_once get_stylesheet_directory() . '/inc/init/set_path.php';

if ( $GLOBALS['pagenow'] === 'wp-login.php' ) {
    require GET_STYLESHEET_DIRECTORY . '/inc/filter/login.php';
}

if(!wp_doing_ajax()){
    /** register menu location */
    require_once GET_STYLESHEET_DIRECTORY . '/inc/init/register_menu_location.php';

    /** load sidebar */
    require_once GET_STYLESHEET_DIRECTORY . '/inc/init/register_sidebar.php';
}

if(is_admin()){//admin GET
    require GET_STYLESHEET_DIRECTORY . '/inc/filter/admin.php';
}

if(!is_admin() || is_ajax_from_frontend()){//Frontend or Admin_ajax
    /** load const */
    require_once GET_STYLESHEET_DIRECTORY . '/inc/init/const.php';

    /** load lib */
    require_once LIBS_DIR . '/smarty/Smarty.class.php';
    //_changed
    //require_once LIBS_DIR . '/smarty/SmartyBC.class.php';

    /** load ENVIRONMENT */
    require_once GET_STYLESHEET_DIRECTORY . '/inc/init/env.php';

    /** load filter */
    require_once GET_STYLESHEET_DIRECTORY . '/inc/init/filter.php';

    /** load function */
    require_once GET_STYLESHEET_DIRECTORY . '/inc/function/custom.php';
    require_once GET_STYLESHEET_DIRECTORY . '/inc/function/override.php';

    /** load blocks */
    require_once GET_STYLESHEET_DIRECTORY . '/inc/init/register_block.php';

    /** load css_js */
    require_once GET_STYLESHEET_DIRECTORY . '/inc/init/register_css_js.php';

    /** load clean output HTML */
    if(is_production()){
        require_once GET_STYLESHEET_DIRECTORY . '/inc/init/clean_html.php';
    }

    /** register AJAX action */
    require_once GET_STYLESHEET_DIRECTORY . '/inc/init/register_ajax.php';

    /** require header asset */
    add_action('wp_head', 'header_asset');

    /** require footer asset */
    add_action('wp_footer', 'footer_asset');
}

/** load filter */
require_once GET_STYLESHEET_DIRECTORY . '/inc/filter/site.php';

/** load customize */
require_once GET_STYLESHEET_DIRECTORY . '/inc/customize/index.php';