<?php
/**
 * BEGIN: Featured Products
 * */
register_sidebar( array(
    'name'          => __( 'Featured Products', 'flatsome' ),
    'id'            => 'featured-product-sidebar',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h4>',
    'after_title'   => '</h4><div class="is-divider"></div>',
) );


/**
 * END: Featured Products
 * */

/**
 * BEGIN: Latest Products
 * */
register_sidebar( array(
    'name'          => __( 'Latest Products', 'flatsome' ),
    'id'            => 'latest-product-sidebar',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<div class="mkdf-widget-title-holder"><span class="mkdf-active-hover">
																			<svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
																				<path class="custom-heading-bg" d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
																			</svg>
																			<span class="mkdf-active-hover-middle custom-heading-bg-middle"></span>
																			<svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
																				<path class="custom-heading-bg" d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
																			</svg>
																		</span><h5 class="mkdf-widget-title text-nowrap">',
    'after_title'   => '</h5></div>',
) );
/**
 * END: Latest Products
 * */

/**
 * BEGIN: Social icon
 * */
register_sidebar(array(
    'name' => 'Social icon Footer',
    'id' => 'social-icon-footer',
    'description' => 'Khu vực hiển thị Social Icon Footer',
    'before_widget' => '<div class="">',
    'after_widget' => '</div>',
    'before_title' => '<div class="d-none">',
    'after_title' => '</div>'
));

