<?php
/**
 * DEV / PRODUCTION environment
 * */
define('PRODUCTION_ENV', false);
define('MAINTENANCE_ENV', false);

function is_production(){
    return PRODUCTION_ENV;
}

function is_maintenance(){
    return MAINTENANCE_ENV;
}

if(is_maintenance()){
    get_template_part('page', 'maintenance');
    exit;
}