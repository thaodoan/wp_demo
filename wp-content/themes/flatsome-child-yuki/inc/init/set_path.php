<?php

define("GET_STYLESHEET_DIRECTORY_URI", get_stylesheet_directory_uri() );
define("GET_STYLESHEET_DIRECTORY", get_stylesheet_directory() );
define("GET_TEMPLATE_DIRECTORY_URI", get_template_directory_uri() );
define("GET_TEMPLATE_DIRECTORY", get_template_directory() );

define("ASSETS_PATH", GET_STYLESHEET_DIRECTORY_URI . '/assets' );
define("ASSETS_DIR", GET_STYLESHEET_DIRECTORY . '/assets' );
define("CSS_PATH", ASSETS_PATH . '/css' );
define("JS_PATH", ASSETS_PATH . '/js' );
define("IMG_PATH", ASSETS_PATH . '/img' );
define("LIBS_PATH", GET_TEMPLATE_DIRECTORY_URI . '/assets' . '/libs' );
define("LIBS_DIR", GET_TEMPLATE_DIRECTORY . '/assets/libs');
define("LIB_PATH", ASSETS_PATH . '/libs' );
define("LIB_DIR", GET_STYLESHEET_DIRECTORY . '/assets/libs');
define("BLOCK_DIR", GET_STYLESHEET_DIRECTORY . '/block');
define("LAYOUTS_DIR", GET_STYLESHEET_DIRECTORY . '/layout');
define("UPLOADS_PATH", wp_get_upload_dir()['baseurl']);

define('THEMES_SHORT_PATH',  parse_url( GET_STYLESHEET_DIRECTORY_URI, PHP_URL_PATH ));
define("SMARTY_COMPILE_DIR", '.' . THEMES_SHORT_PATH . '/assets/cache/templates_c/');
define("SMARTY_CACHE_DIR", '.' . THEMES_SHORT_PATH . '/assets/cache/template_ca/');

function is_ajax_from_frontend(){
    return wp_doing_ajax() && is_admin() && isset($_POST['ajax_front']);
}