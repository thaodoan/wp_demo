<?php if ( have_posts() ) : ?>

<div class="list_product_posts">
	<?php
	// Create IDS
	$ids = array();
	while ( have_posts() ) : the_post();
		echo do_shortcode('[block_post_item img_size="woocommerce_thumbnail" excerpt_length="50" tpl="catalog"]');
	endwhile; // end of the loop.
	?>
</div>

<?php flatsome_posts_pagination(); ?>

<?php else : ?>

	<?php get_template_part( 'template-parts/posts/content','none'); ?>

<?php endif; ?>