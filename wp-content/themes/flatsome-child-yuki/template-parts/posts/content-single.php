<div class="entry-content single-page | pt-2">

    <?php the_content(); ?>

    <?php
    wp_link_pages();
    ?>

    <?php if ( get_theme_mod( 'blog_share', 1 ) ) {
        // SHARE ICONS
        echo '<div class="blog-share text-center mt-4">';
        echo do_shortcode( '[share]' );
        echo '<div class="clearfix"></div>';
        echo '</div>';
    } ?>

</div><!-- .entry-content2 -->


        <?= do_shortcode('[block_product_by_tag tax_name="tag_product" per_page="5"]'); ?>

