<?php
/*
Template name: MHS - Flatsome child - Page - Full Width
*/
echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
echo do_shortcode('[header_extend]');
?>

<?php
/**
 * {THE_CONTENT}
 * */
while ( have_posts() ) :
    the_post();
    the_content();

endwhile; // End of the loop.
?>

<?php
echo do_shortcode('[footer_extend]');
echo do_shortcode('[get_footer]');
echo do_shortcode('[wrap_end]');
?>