<?php
function blockBreadCrumb(){
    $file_name = 'block_breadcrumb.tpl';

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/extend/breadcrumb');
    if(!is_user_logged_in()){
        $smarty->setCompileCheck(false);
    }
    if(!IS_FRONT_PAGE){
        $breadcrumbs = new WC_Breadcrumb();
        $breadcrumbs->add_crumb( __('Home', 'flatsome'), apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
        $breadcrumbs->generate();
        $breadcrumbs = $breadcrumbs->get_breadcrumb();
        $smarty->assign('BREAD_CRUMBS', $breadcrumbs);
        return $smarty->fetch($file_name);
    }
    return '';
}
add_shortcode( 'block_breadcrumb', 'blockBreadCrumb' );

