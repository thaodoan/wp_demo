<?php
function blockCopyright(){
    echo do_shortcode(flatsome_option('footer_left_text'));
}
add_shortcode('block_copyright', 'blockCopyright');

function blockCopyright2(){
    echo do_shortcode(flatsome_option('footer_right_text'));
}
add_shortcode('block_copyright2', 'blockCopyright2');
