<?php
function flatsome_pages_in_search_results_custom($args ){
    extract(shortcode_atts(array(
        'tpl' => '',
        'block_title' => __('Posts found', 'woocommerce'),
        'posts_per_page' => 12,
    ), $args));

    if(!is_search() || !get_theme_mod('search_result', 1)) return;
    global $post, $wp_query;
    $BLOCK_CONTENT = '';

    if( get_search_query() ){
        $attr = [
            'post_type' => array( 'post'),
            's' => get_search_query(),
            'numberposts' => -1,
            'orderby' => 'publish_date',
            'order' => 'DESC',
            'posts_per_page' => $posts_per_page,
            'ignore_sticky_posts' => true,
        ];

        query_posts( $attr );

        $smarty = new Smarty;
        $smarty->setTemplateDir(BLOCK_DIR.'/posts');

        $count = $wp_query->post_count;
        $list_type = get_theme_mod( 'search_result_style', 'row' );
        while ( have_posts() ){
            the_post();
            $smarty->assign('list_type', $list_type);
            $smarty->assign('ID', $post->ID);
            $BLOCK_CONTENT .= $smarty->fetch('block_search_result_'.$tpl.'.tpl');
        }
        wp_reset_query();

        if(file_exists(BLOCK_DIR . '/posts/block_search_result_' . $tpl . '_wrap.tpl')){
            $smarty->assign('BLOCK_TITLE', $block_title);
            $smarty->assign('COUNT', $count);
            $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
            return $smarty->fetch('block_search_result_'.$tpl.'_wrap.tpl');
        }
    }
    return $BLOCK_CONTENT;
}
add_shortcode('block_list_post_search', 'flatsome_pages_in_search_results_custom');