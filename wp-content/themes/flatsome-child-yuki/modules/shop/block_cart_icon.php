<?php
function blockCartIcon($args){
    extract(shortcode_atts(array(
        'tpl' => '',
        'cache' => true
    ), $args));
    $cache = $cache && !is_user_logged_in();

    $file_name = 'block_cart_icon_'.$tpl.'.tpl';

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop');

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }

    if(!$smarty->isCached($file_name)){
        $smarty->assign('LINK', esc_url( wc_get_cart_url() ));
        $smarty->assign('TITLE', __('View cart', 'woocommerce'));
        $smarty->assign('TEXT', __('Cart', 'woocommerce'));
        $smarty->assign('TOTAL',  WC()->cart->get_total());
    }
    return $smarty->fetch($file_name);
}
add_shortcode( 'block_cart_icon', 'blockCartIcon' );
