<?php
/**
 * @see search_shortcode()
 * */
function search_shortcode_custom($args) {
    extract(shortcode_atts(array(
        'tpl' => '',
        'index' => 0,
        'cache' => true
    ), $args));
    $cache = $cache && !is_user_logged_in();

    $file_name = 'block_product_search_'.$tpl.'.tpl';

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop');

    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }

    if(!$smarty->isCached($file_name)){
        if ( get_theme_mod( 'search_placeholder' ) ) {
            $placeholder = get_theme_mod( 'search_placeholder' );
        }else{
            $placeholder = __( 'Search', 'woocommerce' ) . '&hellip;';
        }

        if(get_theme_mod( 'header_search_categories')){
            $attr = array(
                'number'     => '999',
                'orderby'    => 'name',
                'hide_empty' => true,
            );
            $product_cat = get_terms( 'product_cat', $attr );
            $selected_cat  = isset( $_REQUEST['product_cat'] ) ? $_REQUEST['product_cat'] : '';
            $smarty->assign('PRODUCT_CAT', $product_cat);
            $smarty->assign('SELECTED_CAT', $selected_cat);
        }

        $smarty->assign('FORM_ACTION', esc_url( home_url( '/' ) ));
        $smarty->assign('SEARCH_PLACEHOLDER', esc_attr($placeholder));
        $smarty->assign('SEARCH_VALUE', get_search_query());
        $smarty->assign('FORM_INDEX', $index );

        if(defined( 'ICL_LANGUAGE_CODE' )){
            $smarty->assign('ICL_LANGUAGE_CODE', ICL_LANGUAGE_CODE);
        }
    }

    return $smarty->fetch($file_name);
}
add_shortcode('block_product_search', 'search_shortcode_custom');
