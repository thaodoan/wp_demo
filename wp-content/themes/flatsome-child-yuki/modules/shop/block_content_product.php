<?php
function blockContentProduct($args){
    extract(shortcode_atts(array(
        'tpl' => '',
        'size' => 'woocommerce_thumbnail'
    ), $args));

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop/block_content_product');

    global $post, $product;

    // Ensure visibility.
    if ( empty( $product ) || ! $product->is_visible() ) {
        return;
    }

    // Check stock status.
    $out_of_stock = get_post_meta( $post->ID, '_stock_status', true ) == 'outofstock';

    // Extra post classes.
    $classes   = array();
    $classes[] = 'product-small';
    $classes[] = 'col';
    $classes[] = 'has-hover';

    if ( $out_of_stock ) $classes[] = 'out-of-stock';

    if(fl_woocommerce_version_check( '3.4.0' )){
        $classes = esc_attr( implode( ' ', wc_get_product_class( $classes, $product ) ) );
    }else{
        $classes = join( ' ', get_post_class( $classes, $product ) );
    }

    $PRICE = $product->get_price_html();
    $TITLE = get_the_title();
    $LINK = get_the_permalink();
    $IMG_SRC = shopical_get_featured_image(get_the_ID(), $size);

    $smarty->assign('product', $product);
    $smarty->assign('classes', $classes);
    $smarty->assign('TITLE', $TITLE);
    $smarty->assign('LINK', $LINK);
    $smarty->assign('PRICE', $PRICE);
    $smarty->assign('IMG_SRC', $IMG_SRC);
    return $smarty->fetch('block_content_product_'.$tpl.'.tpl');
}

add_shortcode( 'block_content_product', 'blockContentProduct' );