<?php
function blockMiniCart($args){
    extract(shortcode_atts(array(
        'tpl' => ''
    ), $args));
    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop');
    $smarty->display('block_mini_cart_' . $tpl . '.tpl');
}
add_shortcode( 'block_mini_cart', 'blockMiniCart' );
