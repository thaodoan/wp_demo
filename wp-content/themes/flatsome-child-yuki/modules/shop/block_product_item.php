<?php
/**
 * @see:
 * */
function blockProductItem($args){
    extract(shortcode_atts(array(
        'tpl' => '',
        'img_size' => 'woocommerce_thumbnail',
        'cache' => true,
    ), $args));

    $cache = $cache && !is_user_logged_in();
    $file_name = 'block_product_item_' . $tpl . '.tpl';
    $smarty = new SmartyBC;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop/block_product_item');
    if($cache){
        $smarty->setCompileCheck(false);
    }

    global $product;
    $post_id = $product->get_ID();
    $post_title = $product->get_title();

    $THUMB = cf_get_thumb_info($img_size);

    if ( $alt = get_the_post_thumbnail_alt($post_id) ) {
        $THUMB->img_alt = $alt;
    } else {
        $THUMB->img_alt = $post_title;
    }


    $smarty->assign('PRODUCT', $product);
    $smarty->assign('THUMB', $THUMB);
    $smarty->assign('TITLE', $post_title);
    $smarty->assign('LINK', esc_url($product->get_permalink()));
    $smarty->assign('PRICE', $product->get_price_html());

    return $smarty->fetch($file_name);
}

add_shortcode( 'block_product_item', 'blockProductItem' );