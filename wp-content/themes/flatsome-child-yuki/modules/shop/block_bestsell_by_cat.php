<?php
/**
 * @see: WC_Widget_Recently_Viewed
 * */
add_shortcode( 'block_bestsell_by_cat', 'product_bestsell_by_cat' );

function product_bestsell_by_cat($args) {
    if (!class_exists('WooCommerce')) {
        return;
    }

    extract(shortcode_atts(array(
        'num_row' => 5,
        'tpl' => '',
        'thumb_size' => 'thumb-100x100',
        'category' => '',
        'cache' => false,
    ), $args));

    $cache = $cache && !is_user_logged_in();

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR . '/shop/block_bestsell_by_cat');
    $file_name = 'block_bestsell_by_cat_'.$tpl.'_wrap.tpl';
    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
        $smarty->setCompileCheck(false);
    }
    global $subcat;

    if(!$smarty->isCached($file_name)){
        $BLOCK_CONTENT = '';
        $query_args = array(
            'posts_per_page' => $num_row,
            'post_status' => 'publish',
            'post_type' => 'product',
            'no_found_rows' => 1,
            'meta_key' => 'total_sales',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
        );
        $query_args['tax_query'][] = array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => $subcat
        );

        $product = new WP_Query($query_args);
        if($product->have_posts()) :
            while($product->have_posts()) : $product->the_post();
                $smarty->assign('thumb_size', $thumb_size);
                $BLOCK_CONTENT .= $smarty->fetch('block_bestsell_by_cat_'.$tpl.'.tpl');
                $smarty->clearAllAssign();
            endwhile;
            wp_reset_postdata();
        endif;
        $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
    }
    if($cache){
        $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    }
    return $smarty->fetch($file_name);
}
