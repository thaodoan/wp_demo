<?php
/**
 * @see: WC_Widget_Recently_Viewed
 * */
add_shortcode( 'block_product_recent', 'product_recent_shortcode' );

function product_recent_shortcode($args) {
    extract(shortcode_atts(array(
        'num' => 5,
        'tpl' => '',
        'block_title' => __('Recently Viewed Products', 'woocommerce'),
        'img_size' => 'thumb-100x100',
    ), $args));

    $viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', wp_unslash( $_COOKIE['woocommerce_recently_viewed'] ) ) : array();

    $viewed_products = array_reverse( array_filter( array_map( 'absint', $viewed_products ) ) );

    // Exclude current ID
    if(is_product()){
        // Method GET
        $current_id = get_the_ID();
    }elseif(isset($_POST['current_id'])){
        // Method AJAX
        $current_id = $_POST['current_id'];
    }
    if(isset($current_id)){
        $viewed_products = array_diff( $viewed_products, [$_POST['current_id']] );
    }

    if ( empty( $viewed_products ) ) {
        return;
    }


    $query_args = array(
        'posts_per_page' => $num,
        'no_found_rows'  => 1,
        'post_status'    => 'publish',
        'post_type'      => 'product',
        'post__in'       => $viewed_products,
        'orderby'        => 'post__in',
    );
    if ( 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
        $query_args['tax_query'] = array(
            array(
                'taxonomy' => 'product_visibility',
                'field'    => 'name',
                'terms'    => 'outofstock',
                'operator' => 'NOT IN',
            ),
        ); // WPCS: slow query ok.
    }

    $r = new WP_Query( apply_filters( 'woocommerce_recently_viewed_products_widget_query_args', $query_args ) );

    $smarty = new Smarty;
    $smarty->setTemplateDir(BLOCK_DIR.'/shop');
    $smarty->setCaching(Smarty::CACHING_LIFETIME_CURRENT);
    $smarty->setCompileCheck(false);

    $BLOCK_CONTENT = '';

    if ( $r->have_posts() ) {
        while ( $r->have_posts() ) {
            $r->the_post();
            global $product;
            $product->link = esc_url( $product->get_permalink() );

            $product->title = wp_kses_post( $product->get_name() );
            $product->price = $product->get_price_html();

            $THUMB = cf_get_thumb_info( $img_size );
            $THUMB->img_alt = get_the_post_thumbnail_alt(get_the_ID());
            if(!$THUMB->img_alt){
                $THUMB->img_alt = $product->title;
            }

            $smarty->assign('product', $product);
            $smarty->assign('THUMB', $THUMB);
            $BLOCK_CONTENT .= $smarty->fetch('block_recent_'.$tpl.'.tpl');
            $smarty->clearAllAssign();
        }
    }
    wp_reset_postdata();

    if(file_exists(BLOCK_DIR .'/shop/block_recent_'.$tpl.'_wrap.tpl')){
        $smarty->assign('BLOCK_TITLE', $block_title);
        $smarty->assign('BLOCK_CONTENT', $BLOCK_CONTENT);
        return $smarty->fetch('block_recent_'.$tpl.'_wrap.tpl');
    }
    return $BLOCK_CONTENT;
}

function ajax_product_recent(){
    echo product_recent_shortcode([]);
    exit;
}

function localize_script_recent(){
    wp_enqueue_script('ajax_product_recent', ASSETS_PATH . '/themes/custom/ajax_product_recent.js', false);
    if(is_product()){
        wp_localize_script( 'themes-js', 'ajax_product_data', array(
                'current_id' => get_the_ID()
            )
        );
    }
}

function do_ajax_product_recent(){
    add_action('wp_footer', 'localize_script_recent');
}