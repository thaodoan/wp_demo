<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
echo do_shortcode('[header_extend]');

//get_header( 'shop' );

do_action('flatsome_before_product_page');

?>


<?php
/**
 * woocommerce_before_main_content hook
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 */
do_action( 'woocommerce_before_main_content' );
?>

<?php while ( have_posts() ) : the_post(); ?>

    <?php
    if(get_theme_mod('product_layout') == 'custom') {
        wc_get_template_part( 'content', 'single-product-custom' );
    } else {
        wc_get_template_part( 'content', 'single-product' );
    }
    ?>

<?php endwhile; // end of the loop. ?>

<?php
/**
 * woocommerce_after_main_content hook
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );
?>

<?php

do_action('flatsome_after_product_page');

//get_footer( 'shop' );
?>

<?php
echo do_shortcode('[footer_extend]');
echo do_shortcode('[get_footer]');
echo do_shortcode('[wrap_end]');
?>
