<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product;

$classes = array();
if($product->is_on_sale()) $classes[] = 'price-on-sale';
if(!$product->is_in_stock()) $classes[] = 'price-not-in-stock'; ?>

<div class="price-wrapper mt-3 pt-3 mb-3 border-top">
    <div class="price product-page-price | <?php echo implode(' ', $classes); ?> ">
        <?php
        if ( $product->get_price() == 0 && !$product->is_on_sale() && !$product->get_regular_price() ) {
            $price = '<p class="font-weight-500 d-inline-block text-size-3"><span class="d-none">' . __('Price:', 'woocommerce') . '&nbsp;</span><span class="price product-page-price | d-inline-block color-pink text-size-2"><span class="amount mb-0"> ' . __('Contacts', 'woocommerce') .'</span></span></p>';
            $price .= "<p>". __( 'Contact text', 'woocommerce' ) ."</p>";
            $price .= "<p>Hotline: <a class='color-red' href='tel:" . $GLOBALS['cgv']['phone'] . "'>". $GLOBALS['cgv']['phone_format'] ."</a></p>";
            $price .= "<p>Email: <a class='color-green' href='mailto:" . $GLOBALS['cgv']['email'] . "'>". $GLOBALS['cgv']['email'] ."</a></p>";
            $price .= '<p><a class="single_add_to_cart_button button alt btn" href='. get_page_link("814") .'>' . __( 'Send contact', 'woocommerce' ) . '</a></p>';
        }else{
            $price = '<span class="mb-0 font-weight-500 d-inline-block text-size-3"><span class="align-middle | text-size-1">' . __('Price:', 'woocommerce') . '&nbsp;</span><span class="price product-page-price | d-inline-block color-pink text-size-2">
                    <span class="amount mb-0"> ' . $product->get_price_html() . '</span>
                    </span>
                    </span>';
            $price .= do_shortcode('[block_sale_flash tpl="detail"]');
        }
        echo $price;
        ?>
    </div>
</div>
