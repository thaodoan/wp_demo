<div class="fix-more-padding">
    <div class="co-1 col-lg-12 ">
        <div class="product-main">
            <div class="row-divided">
                <div class="row m-0">
                    <div class="large-<?php echo flatsome_option('product_image_width'); ?> col-12 col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?php
                        /**
                         * woocommerce_before_single_product_summary hook
                         *
                         * @hooked woocommerce_show_product_sale_flash - 10
                         * @hooked woocommerce_show_product_images - 20
                         */
                        woocommerce_show_product_images();
                        //do_action( 'woocommerce_before_single_product_summary' );
                        ?>
                        <div class="clear"></div>
                        <div class="row mt-4">
                            <div class="col ">
                                <?php woocommerce_template_single_sharing(); ?>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="product-info summary entry-summary col col-fit <?php flatsome_product_summary_classes();?> | mt-4 mt-md-0">
                        <?php
                        /**
                         *
                         *
                         * hook
                         *
                         * @hooked woocommerce_template_single_title - 5
                         * @hooked woocommerce_template_single_rating - 10
                         * @hooked woocommerce_template_single_price - 10
                         * @hooked woocommerce_template_single_excerpt - 20
                         * @hooked woocommerce_template_single_add_to_cart - 30
                         * @hooked woocommerce_template_single_meta - 40
                         * @hooked woocommerce_template_single_sharing - 50
                         */
                        woocommerce_template_single_title();
                        woocommerce_template_single_excerpt();
                        woocommerce_template_single_price();
                        woocommerce_template_single_add_to_cart();
                        woocommerce_template_single_meta();
                        //do_action( 'woocommerce_single_product_summary' );
                        ?>

                    </div><!-- .summary -->

                </div><!-- .row -->
                <div class="product-footer mt-3">
                    <?php
                    /**
                     * woocommerce_after_single_product_summary hook
                     *
                     * @hooked woocommerce_output_product_data_tabs - 10
                     * @hooked woocommerce_upsell_display - 15
                     * @hooked woocommerce_output_related_products - 20
                     */
                    woocommerce_output_product_data_tabs();
                    woocommerce_upsell_display(null,null, $order_by = "ID");
                    //woocommerce_output_related_products();
                    //do_action( 'woocommerce_after_single_product_summary' );
                    ?>

                    <?php echo do_shortcode('[block_posts_by_tag tag_product="'.$product.'" posts_per_page="10" tpl="product"]'); ?>
                </div>
            </div><!-- .row -->
        </div><!-- .product-main -->
    </div>
</div>
