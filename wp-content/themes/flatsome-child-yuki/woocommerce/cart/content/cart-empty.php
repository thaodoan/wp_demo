<div class="d-flex">
    <img class="cart-empty-img mx-auto" src="<?= IMG_PATH.'/icons/empty-cart.png'; ?>">
</div>
<div class="d-flex">
    <p class="cart-empty mx-auto text-center">
        <?= __( 'Your cart is currently empty.', 'woocommerce' ); ?>
    </p>
</div>
<div class="d-flex">
    <p class="return-to-shop mx-auto text-center">
        <a class="button primary wc-backward | btn btn-warning" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
            <?php echo ( is_rtl() ? '&#8594;' : '&#8592;' ) . '&nbsp;'?><?php esc_html_e( 'Return to shop', 'woocommerce' ); ?>
        </a>
    </p>
</div>
