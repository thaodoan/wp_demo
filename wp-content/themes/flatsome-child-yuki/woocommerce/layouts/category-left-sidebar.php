<div class="fix-more-padding row">
    <div class="co-1 col-lg-9 order-lg-2">
        <div class="row">
            <div id="content" class="col">
                <div class="clear"></div>

                <div class="shop-page-heading">
                    <div class="row">
                        <div class="col-12">
                            <?php woocommerce_result_count(); ?>
                        </div>
                    </div>
                </div>
                <?php
                /**
                 * Hook: woocommerce_before_main_content.
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20 (FL removed)
                 * @hooked WC_Structured_Data::generate_website_data() - 30
                 */
                //do_action( 'woocommerce_before_main_content' );

                ?>

                <?php
                /**
                 * Hook: woocommerce_archive_description.
                 *
                 * @hooked woocommerce_taxonomy_archive_description - 10
                 * @hooked woocommerce_product_archive_description - 10
                 */
                do_action( 'woocommerce_archive_description' );
                ?>

                <div class="text-md-right">
                    <?php woocommerce_catalog_ordering(); ?>
                </div>

                <div class="pro-bg">

                    <?php

                    if ( fl_woocommerce_version_check( '3.4.0' ) ? woocommerce_product_loop() : have_posts() ) {

                        /**
                         * Hook: woocommerce_before_shop_loop.
                         *
                         * @hooked wc_print_notices - 10
                         * @hooked woocommerce_result_count - 20 (FL removed)
                         * @hooked woocommerce_catalog_ordering - 30 (FL removed)
                         */
                        do_action( 'woocommerce_before_shop_loop' );

                        //woocommerce_product_loop_start();
                        ?>
                        <div class="homepage-section">
                            <div class="panel-custom  ">
                                <div class="row panel-body">
                                    <?php  if ( wc_get_loop_prop( 'total' ) ) {
                                        while ( have_posts() ) {
                                            the_post();

                                            /**
                                             * Hook: woocommerce_shop_loop.
                                             *
                                             * @hooked WC_Structured_Data::generate_product_data() - 10
                                             */
                                            do_action( 'woocommerce_shop_loop' );
                                            echo do_shortcode("[block_product_item tpl='group_home' img_size='woocommerce_thumbnail']");
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php

                        //woocommerce_product_loop_end();

                        /**
                         * Hook: woocommerce_after_shop_loop.
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action( 'woocommerce_after_shop_loop' );
                    } else {
                        /**
                         * Hook: woocommerce_no_products_found.
                         *
                         * @hooked wc_no_products_found - 10
                         */
                        do_action( 'woocommerce_no_products_found' );
                    }
                    ?>
                </div>

                <div class="clear"></div>
                <?php
                /**
                 * Hook: flatsome_products_after.
                 *
                 * @hooked flatsome_products_footer_content - 10
                 */
                //do_action( 'flatsome_products_after' );
                /**
                 * Hook: woocommerce_after_main_content.
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                //do_action( 'woocommerce_after_main_content' );
                ?>

                <?= do_shortcode('[block_list_post_search posts_per_page=8]') ?>
            </div>
        </div>
    </div>

    <div class="co-2 col-lg-3 order-lg-1 mt-4 mt-lg-0">
        <div id="shop-sidebar">
            <div class="mb-5">
                <?php
                if(is_active_sidebar('shop-sidebar')) {
                    dynamic_sidebar('shop-sidebar');
                } else{
                    echo '<p>You need to assign Widgets to <strong>"Shop Sidebar"</strong> in <a href="'.get_site_url().'/wp-admin/widgets.php">Appearance > Widgets</a> to show anything here</p>';
                }
                ?>
            </div>

            <div id="block_product_recent">
                <?php do_ajax_product_recent(); ?>
            </div>
        </div><!-- .sidebar-inner -->
    </div>
</div>


