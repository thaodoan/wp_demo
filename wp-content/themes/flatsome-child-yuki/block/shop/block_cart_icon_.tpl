{*
* REQUIRED class to update cart number
* class .cart_refragment  -> .cart_count
*}

{* BEGIN: main *}

<a class="dropdown-toggle cart-icon" data-toggle="dropdown" data-hover="dropdown" data-delay="0" href="{$LINK}" title="{$TITLE}">
	<div class="icons-cart cart_refragment">
		<i class="fa fa-shopping-cart"></i>
		<span class="cart-count">
			<span class="cart_count"></span>
		</span>
	</div>
</a>

{* END: main *}


