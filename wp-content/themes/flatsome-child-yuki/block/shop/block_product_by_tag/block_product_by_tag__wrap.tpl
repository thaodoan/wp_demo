<div class="mt-4">
    <h3 class="product-section-title container-width product-section-title-related pb-half | uppercase display-6 mb-2 font-weight-500 title_bg_line">
        {$BLOCK_TITLE}
    </h3>
    <div class="homepage-section">
        <div class="panel-custom row">
            <div class="flex-row panel-body">
                {$BLOCK_CONTENT}
            </div>
        </div>
    </div>
</div><!-- block_product_by_tag -->