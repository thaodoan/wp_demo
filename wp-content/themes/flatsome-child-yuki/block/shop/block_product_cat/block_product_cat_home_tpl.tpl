{* BEGIN: main *}

{* BEGIN: top_menu *}

<li  class="level-0 menu-item-12295  {if $CAT->_has_sub}menu-item-has-children{/if}    menu-item menu-item-type-custom menu-item-object-custom  mega-menu mega-menu-fullwidth-width     " >
    <a href="{$CAT->link}"><span class="menu-item-text">{$CAT->name}</span></a>
    {* BEGIN: sub *}
    {if $CAT->_has_sub}
        {block name="SUB"}{$smarty.block.child}{/block}
    {/if}
    {* END: sub *}
</li>

{* END: top_menu *}

{* END: main *}