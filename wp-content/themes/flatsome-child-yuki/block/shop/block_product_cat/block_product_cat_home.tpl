{extends file="block_product_cat_{$tpl}_tpl.tpl"}

{block name="SUB"}
    {* BEGIN: sub_menu *}
    <div class="sub-menu">
        <div data-elementor-type="wp-post" data-elementor-id="12585" class="elementor elementor-12585" data-elementor-settings="[]">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <section class="elementor-section elementor-top-section elementor-element elementor-element-a0d7f4b wpb-p-0 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="a0d7f4b" data-element_type="section" data-settings="">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-fc440f5 wpb-p-0" data-id="fc440f5" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            {* BEGIN: loop *}
                                            {foreach $SUB_CAT as $SUB}
                                                <div class="elementor-element elementor-element-26de65d title-vertical elementor-widget elementor-widget-heading" data-id="26de65d" data-element_type="widget" data-widget_type="heading.default">
                                                    <div class="elementor-widget-container">
                                                        <a href="{$SUB->link}" title="{$SUB->name}" class=" | ">
                                                            <h2 class="elementor-heading-title elementor-size-default">{$SUB->name} ({$SUB->count})</h2>
                                                        </a>
                                                    </div>
                                                </div>
                                            {/foreach}
                                            {* END: loop *}
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-eeae105 hidden-sm hidden-md" data-id="eeae105" data-element_type="column" data-settings="">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-f94aa70 elementor-widget elementor-widget-bwp_product_list" data-id="f94aa70" data-element_type="widget" data-widget_type="bwp_product_list.default">
                                                <div class="elementor-widget-container">
                                                    <div id="bwp_default_18796911011614926422" class="bwp_product_list product-menu  ">
                                                        <div class="product-list-top">
                                                            <div class="title-block">
                                                                <h2>BEST SELLER PRODUCTS</h2>
                                                            </div>
                                                        </div>
                                                        <div class="content products-list grid row">
                                                            {php}
                                                                echo do_shortcode('[block_bestsell_by_cat]');
                                                            {/php}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
    {* END: sub_menu *}
{/block}