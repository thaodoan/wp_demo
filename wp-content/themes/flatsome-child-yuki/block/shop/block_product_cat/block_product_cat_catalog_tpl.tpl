{* BEGIN: main *}

{* BEGIN: top_menu *}

<li class="item {if $CAT->_current_parent}active{/if}">
    <a href="{$CAT->link}" class="position-relative  | {if $CAT->_current}current-menu-item{/if}">
        {$CAT->name}&nbsp;&nbsp;<span class="metis_count position-absolute | color-holder">({$CAT->count})</span>
    </a>
    {if $CAT->_has_sub}
        <span class="fa arrow expand"></span>
    {/if}

    {* BEGIN: sub *}
    {if $CAT->_has_sub}
        {block name="SUB"}{$smarty.block.child}{/block}
    {/if}
    {* END: sub *}
</li>

{* END: top_menu *}

{* END: main *}