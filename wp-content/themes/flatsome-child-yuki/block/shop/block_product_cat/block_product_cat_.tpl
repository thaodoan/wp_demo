{* BEGIN: main *}
<nav class="navbar ">
    <ul class="navbar-nav">

        {* BEGIN: top_menu *}
        {foreach $CATS as $CAT}

            <li class="nav-item">
                <a class="nav-link" href="{$CAT->link}">
                    {$CAT->name} <span class="count">{$CAT->count}</span>
                </a>
            </li>

        {/foreach}
        {* END: top_menu *}

    </ul>
</nav>

{* END: main *}