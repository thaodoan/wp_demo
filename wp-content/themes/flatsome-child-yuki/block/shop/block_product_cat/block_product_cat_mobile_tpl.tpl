{* BEGIN: main *}

{* BEGIN: top_menu *}

<li class=" {if $CAT->_current_parent}{/if} {if $CAT->_has_sub}dropdown{/if}">
    <a href="{$CAT->link}" class="  | {if $CAT->_current}current-menu-item{/if} hvr-wobble-skew {if $CAT->_has_sub}dropdown-toggle header-menu{/if}">
        <img src="{$CAT->thumb}">{$CAT->name}

        {if $CAT->_has_sub}
            <i class="fa fa-angle-down pull-right"></i>
        {/if}
    </a>

    {* BEGIN: sub *}
    {if $CAT->_has_sub}
        {block name="SUB"}{$smarty.block.child}{/block}
    {/if}
    {* END: sub *}
</li>

{* END: top_menu *}

{* END: main *}