<a href="{$LINK}" id="wishlist-total" title="{$TEXT}">
    <i class="fa fa-heart-o "></i>
    <span class="hidden-xs">{$TEXT} (<span class="wishlist-counter">{$COUNT}</span>)</span>
</a>