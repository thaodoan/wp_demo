{if $product->get_type() == 'variable'}
    {do_shortcode('[block_quickview_btn]')}
{else}
    <a title="{$product->add_to_cart_text()}" href="{$product->add_to_cart_url()}" data-quantity="{$args['quantity']}" class="{$args['class']} | add_to_cart_button " {$args['attributes']}>
        {$product->add_to_cart_text()}
    </a>
{/if}

