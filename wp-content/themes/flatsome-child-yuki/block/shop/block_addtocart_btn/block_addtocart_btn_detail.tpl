{if $product->get_type() == 'variable'}
    {do_shortcode('[block_quickview_btn tpl="detail"]')}
{else}
    <a title="{$product->add_to_cart_text()}" href="{$product->add_to_cart_url()}" data-quantity="{$args['quantity']}" class="{$args['class']} | pcart" {$args['attributes']}>
        {$product->add_to_cart_text()}
    </a>
{/if}