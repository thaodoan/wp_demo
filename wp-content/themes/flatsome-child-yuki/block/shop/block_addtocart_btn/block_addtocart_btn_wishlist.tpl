<a href="{$product->add_to_cart_url()}" data-quantity="{$args['quantity']}" class="{$args['class']} | text-nowrap font-weight-500 text-size-n1 btn btn-warning" {$args['attributes']}>
    <i class="fa fa-plus"></i>{$product->add_to_cart_text()}
</a>