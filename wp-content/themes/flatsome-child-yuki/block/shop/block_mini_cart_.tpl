{*
* REQUIRED: class ".widget_shopping_cart_content" to auto load CART content
*}

{* BEGIN: main*}
<div class="mkdf-sc-dropdown">
    <div class="mkdf-sc-dropdown-inner">
        <div class="widget_shopping_cart_content">
            {* mini cart here: *}
            {* @see: woocommerce\cart\mini-cart.php *}
        </div>
    </div>
</div>



{* END: main*}

