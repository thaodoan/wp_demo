<aside class="widget woocommerce widget_recently_viewed_products">
    <span class="widget-title shop-sidebar">{$BLOCK_TITLE}</span>
    <div class="is-divider small mb-0"></div>
    <ul class="product_list_widget | ">
        {$BLOCK_CONTENT}
    </ul>
</aside>