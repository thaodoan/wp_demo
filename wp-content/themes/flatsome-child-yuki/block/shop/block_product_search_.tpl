{*/**
* The template for displaying product search form
*
* This template can be overridden by copying it to yourtheme/woocommerce/product-searchform.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see     https://docs.woocommerce.com/document/template-structure/
* @package WooCommerce/Templates
* @version 3.3.0
*/*}

{* BEGIN: main *}

<form role="search" method="get" class="search-from" action="{$FORM_ACTION}">
    <div class="search-box">
        {* BEGIN: input_search *}
        <input type="text" value="{$SEARCH_VALUE}" name="s" id="ss" class="input-search s" placeholder="{$SEARCH_PLACEHOLDER}" />
        {* END: input_search *}
        <div class="result-search-products-content">
            <ul class="result-search-products"></ul>
        </div>
    </div>
    <button id="searchsubmit2" class="btn" type="submit"> <span class="search-icon"> <i class="fa fa-search"></i> </span> <span>search</span> </button>
    {* required hidden field*}
    <input type="hidden" name="post_type" value="product" />

    {if isset($ICL_LANGUAGE_CODE)}
        <input type="hidden" name="lang" value="{$ICL_LANGUAGE_CODE}" />
    {/if}
</form>
{* END: main *}