
{* BEGIN: main *}
<div class="{$classes} | product-layout product-grid col-lg-3 col-md-4 col-sm-6 col-xs-12">
	<div class="product-thumb transition">
		<div class="image">
			<a href="{$LINK}">
				<img src="{$IMG_SRC}" alt="{$TITLE}" title="{$TITLE}" class="img-responsive center-block">
			</a>
			{do_shortcode('[block_sale_flash]')}

		</div>

		<div class="caption text-center">
			<h4 class="text-size-1 mt-2"><a class="text2line" href="{$LINK}">{$TITLE}</a></h4>
			<p class="price color-red mt-2 font-weight-500">
				{$PRICE}
			</p>
			<div class="rating"></div>
		</div>

		<div class="button-group text-center m-button">
			<div class="image-tools is-small top right | d-none">
				{* button Wishlist*}
				{do_shortcode('[yith_wcwl_add_to_wishlist]')}
			</div>

			{*<div class="d-none">
				*}{* button Add to Cart*}{*
				{do_shortcode('[block_addtocart_btn tpl="home"]')}
			</div>*}

		</div>
	</div>

</div>

{* END: main *}


