{* BEGIN: main *}
{* BEGIN: loop *}
<li class="content-widget-product | ">
    <a href="{$product->link}">
        <img src="{$THUMB->img_src}" width="{$THUMB->img_width}" class="woocommerce-placeholder wp-post-image" alt="{$THUMB->img_alt}">
        <span class="product-title">{$product->title}</span>
    </a>
    <div class="price color-pink font-weight-500">
        {$product->price}
    </div>

    {if $product->get_stock_quantity() == 0}
        <p class="out_of_stock">
            {php}
                echo __( 'Out of stock', 'woocommerce' );
            {/php}
        </p>
    {/if}
</li>
{* BEGIN: loop *}
{* END: main *}