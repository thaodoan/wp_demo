<section class="elementor-section elementor-top-section elementor-element elementor-element-7478cc7 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="7478cc7" data-element_type="section">
    <div class="elementor-container elementor-column-gap-extended">
        <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-7ae683e" data-id="7ae683e" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        {* BEGIN: CAT *}
                        <div class="elementor-element elementor-element-2ac1734 title-block-9 elementor-widget elementor-widget-heading" data-id="2ac1734" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                                <h2 class="elementor-heading-title elementor-size-default">
                                    <a href="{$CAT->link}">{$CAT->name}</a>
                                </h2>
                            </div>
                        </div>
                        {* BEGIN: CAT *}

                        {* BEGIN: PRODUCTS *}
                        <div class="elementor-element elementor-element-a9ddd8e elementor-widget elementor-widget-bwp_product_list" data-id="a9ddd8e" data-element_type="widget" data-widget_type="bwp_product_list.default">
                            <div class="elementor-widget-container">
                                <div id="bwp_default_10788761941611795676" class="bwp_product_list default  no-title">
                                    <div class="content products-list grid row">
                                        {$LIST_PRODUCT}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {* END: PRODUCTS *}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
