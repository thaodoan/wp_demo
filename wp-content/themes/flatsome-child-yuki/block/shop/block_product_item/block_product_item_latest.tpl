<div class="item">
	<div class="item-product">
		<div class="item-thumb">
			<div class="product-thumb-hover">
				<a href="{$LINK}" class="woocommerce-LoopProduct-link">
					<img width="1000" height="1000" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" data-lazy-src="{$THUMB->img_src}" />
					<noscript><img width="1000" height="1000" src="{$THUMB->img_src}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image" alt="" loading="lazy" /></noscript>
				</a>
			</div>
		</div>
		<div class="content-bottom">
			<div class="item-title"> <a href="{$LINK}">{$TITLE}</a></div>
			<div class="price"> {$PRICE}</div>
		</div>
	</div>
</div>
