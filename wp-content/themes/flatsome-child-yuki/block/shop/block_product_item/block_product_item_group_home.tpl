{*<div class="block_product flex-row-items col-6 col-sm-4 col-md-4 col-lg-4">
	<div class="product-box">
		<div class="product-thumbnail">
			<a class="image_link" href="{$LINK}" title="{$TITLE}">
				<img width="{$THUMB->img_width}" src="{$THUMB->img_src}" srcset="{$THUMB->img_srcset}" sizes="(max-width: 576px) 50vw, 270px" alt="{$THUMB->img_alt}" title="{$TITLE}" class="" />
			</a>
		</div>

		<div class="product-info effect a-left">
			<div class="info_hhh">
				<h3 class="product-name product-name-hover text2line text-center">
					<a href="{$LINK}">{$TITLE}</a>
				</h3>
				<div class="price-box clearfix text-center">
                    <span class="price product-price color-pink">
                        {$PRICE}
                    </span>

					{do_shortcode('[block_sale_flash]')}

				</div>
			</div>
		</div>

		<div class="button-group text-center m-button d-none">
			*}{* button Wishlist*}{*
			{do_shortcode('[yith_wcwl_add_to_wishlist]')}

			*}{* button Add to Cart*}{*
			{do_shortcode('[block_addtocart_btn tpl="home"]')}
		</div>
	</div>
</div>*}

<div class="item-product col-xl-3 col-lg-3 col-md-4 col-6">
	<div class="products-entry content-product2 clearfix product-wapper">
		<div class="products-thumb">
			<div class='product-lable'> {do_shortcode('[block_sale_flash]')}</div>
			<div class="product-thumb-hover">
				<a href="{$LINK}" class="woocommerce-LoopProduct-link">
					<img width="{$THUMB->img_width}" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" srcset="{$THUMB->img_srcset}" sizes="(max-width: 576px) 50vw, 270px" alt="{$THUMB->img_alt}" title="{$TITLE}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail " loading="lazy" data-lazy-src="{$THUMB->img_src}"/>
				</a>
			</div>
			<div class='product-button'>
				{*{do_shortcode('[block_addtocart_btn tpl="home"]')}*}
			</div>
		</div>
		<div class="products-content">
			<div class="contents">
				<h3 class="product-title"><a href="{$LINK}">{$TITLE}</a></h3>
				<span class="price">
					{$PRICE}
				</span>
				{if $PRODUCT->get_stock_quantity() == 0}
					<p class="out_of_stock">
						{php}
							echo __( 'Out of stock', 'woocommerce' );
						{/php}
					</p>
				{/if}
			</div>
		</div>
	</div>
</div>