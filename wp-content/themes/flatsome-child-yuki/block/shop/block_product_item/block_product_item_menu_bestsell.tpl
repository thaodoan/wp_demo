<div class="item col-xl-12 col-lg-12 col-md-12 col-12">
	<div class="item-product">
		<div class="item-thumb">
			<a href="{$LINK}">
				<img width="{$THUMB->img_width}" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201000%201000'%3E%3C/svg%3E" srcset="{$THUMB->img_srcset}" sizes="(max-width: 576px) 50vw, 270px" alt="{$THUMB->img_alt}" title="{$TITLE}" class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail " loading="lazy" data-lazy-src="{$THUMB->img_src}"/>
			</a>
		</div>
		<div class="content-bottom">
			<div class="item-title"> <a href="{$LINK}">{$TITLE}</a></div>
			<div class="price"> {$PRICE}</div>
			{if $PRODUCT->get_stock_quantity() == 0}
				<p class="out_of_stock">
					{php}
						echo __( 'Out of stock', 'woocommerce' );
					{/php}
				</p>
			{/if}
		</div>
	</div>
</div>
