{* BEGIN: main *}
<li  class="level-0 menu-item-9477      menu-item menu-item-type-post_type menu-item-object-page {if $TOP_MENU->_has_sub}menu-item-has-children{/if}  std-menu      " >
	<a href="{$TOP_MENU->url}"><span class="menu-item-text">{$TOP_MENU->title}</span></a>
	{* BEGIN: _has_sub *}
	{if $TOP_MENU->_has_sub}
		{block name="SUB"}{$smarty.block.child}{/block}
	{/if}
	{* END: _has_sub *}
</li>
{* END: main *}