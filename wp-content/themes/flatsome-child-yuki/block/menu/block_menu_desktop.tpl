{extends file="block_menu_desktop_tpl.tpl"}

{block name="TOP_MENU"}

{/block}

{block name="SUB"}
    {* BEGIN: sub_menu *}
    <div class="sub-menu">
        <div data-elementor-type="wp-post" data-elementor-id="12246" class="elementor elementor-12246" data-elementor-settings="[]">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <section class="elementor-section elementor-top-section elementor-element elementor-element-9c716f3 content-megamenu elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9c716f3" data-element_type="section" data-settings="">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-7c9b2cc wpb-col-sm-100" data-id="7c9b2cc" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-c184aa2 elementor-widget elementor-widget-text-editor" data-id="c184aa2" data-element_type="widget" data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <ul>
                                                            {* BEGIN: loop *}
                                                            {foreach $SUB_MENU as $index => $SUB}
                                                                {if $index % 2 != 0}
                                                                    <li class="{$SUB->classes} || ">
                                                                        <a href="{$SUB->url}" title="{$SUB->description}" class=" || ">
                                                                            {$SUB->title} {$index}
                                                                        </a>
                                                                    </li>
                                                                {/if}
                                                            {/foreach}
                                                            {* END: loop *}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-7c9b2cc wpb-col-sm-100" data-id="7c9b2cc" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-c184aa2 elementor-widget elementor-widget-text-editor" data-id="c184aa2" data-element_type="widget" data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <ul>
                                                            {* BEGIN: loop *}
                                                            {foreach $SUB_MENU as $index => $SUB}
                                                                {if $index % 2 == 0}
                                                                    <li class="{$SUB->classes} || ">
                                                                        <a href="{$SUB->url}" title="{$SUB->description}" class=" || ">
                                                                            {$SUB->title}
                                                                        </a>
                                                                    </li>
                                                                {/if}
                                                            {/foreach}
                                                            {* END: loop *}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-06777b8 wpb-col-sm-100 hidden-sm hidden-xs" data-id="06777b8" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-45f87c2 title elementor-widget elementor-widget-heading" data-id="45f87c2" data-element_type="widget" data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h2 class="elementor-heading-title elementor-size-default">Bài viết mới nhất</h2>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-38b1f54 elementor-widget elementor-widget-bwp_recent_post" data-id="38b1f54" data-element_type="widget" data-widget_type="bwp_recent_post.default">
                                                <div class="elementor-widget-container">
                                                    <div class="bwp-recent-post blog-menu">
                                                        <div class="block">
                                                            <div class="block_content">
                                                                <div id="recent_post_5963185231614906621" class="row">
                                                                    {php}echo do_shortcode('[block_latest_post tpl="home" type="row" posts="8" columns__sm="2" text_align="left" show_date="text" image_size="thumbnail" image_height="75%"]');{/php}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
{/block}