<div class="elementor-element elementor-element-2907a59 list-link elementor-widget elementor-widget-text-editor" data-id="2907a59" data-element_type="widget" data-widget_type="text-editor.default">
    <div class="elementor-widget-container">
        <div class="elementor-text-editor elementor-clearfix">
            <ul>
                {$BLOCK_CONTENT}
            </ul>
        </div>
    </div>
</div>