{extends file="block_menu_mobile_tpl.tpl"}

{block name="TOP_MENU"}
    {* BEGIN: top_menu *}
    <a href="{$TOP_MENU->url}" class=" | {if $TOP_MENU->_has_sub}dropdown-toggle header-menu{/if}" >
        {$TOP_MENU->title}
        {if $TOP_MENU->_has_sub}<i class="fa fa-angle-down pull-right"></i>{/if}
    </a>
    {* END: top_menu *}
{/block}

{block name="SUB"}

    {* BEGIN: SUB_MENU *}
    {if $TOP_MENU->_has_sub}

        <span class="mobile_arrow">
							<svg xmlns="http://www.w3.org/2000/svg" width="12" height="17" viewBox="0 0 11 17" class="mkdf-menu-arrow">
								<path d="M9.1 8.7L7.8 6.6C7 5.7 5.7 4.5 5.2 3.2S3.9 1.1 3.9 1.1L3.5 0.7C2.6 1.5 4.4 5.7 6.1 7.4c2.2 2.1 1.3 3.8-1.7 5.9 -3 1.3-4 2.7-1.8 2.3 0.9 0 3.1-1.8 4.4-3.1l1.7-1.7C8.7 10.8 9.9 9.9 9.1 8.7z" />
							</svg>
						</span>
        <ul class="sub_menu">

            {* BEGIN: loop *}
            {foreach $SUB_MENU as $SUB}

                <li class="menu-item menu-item-type-post_type menu-item-object-page">
                    <a href="{$SUB->url}" title="{$SUB->description}" class="{$SUB->classes} || ">
                        <span>{$SUB->title}</span>
                    </a>
                </li>

            {/foreach}
            {* END: loop *}

        </ul>

    {/if}
    {* END: SUB_MENU *}
{/block}