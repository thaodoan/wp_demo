{extends file="block_menu__tpl.tpl"}

{block name="TOP_MENU"}
	{* BEGIN: top_menu *}
	<a class="nav-link {if $TOP_MENU->_current}active{/if} || {$TOP_MENU->classes} ||" href="{$TOP_MENU->url}" title="{$TOP_MENU->description}" >
		{$TOP_MENU->title}
	</a>
	{* END: top_menu *}
{/block}

{block name="SUB"}
	<div class="sub-menu">
		<div data-elementor-type="wp-post" data-elementor-id="12246" class="elementor elementor-12246" data-elementor-settings="[]">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-9c716f3 content-megamenu elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9c716f3" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
							<div class="elementor-row">
								<div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-7c9b2cc wpb-col-sm-100" data-id="7c9b2cc" data-element_type="column">
									<div class="elementor-column-wrap elementor-element-populated">
										<div class="elementor-widget-wrap">

											<div class="elementor-element elementor-element-c184aa2 elementor-widget elementor-widget-text-editor" data-id="c184aa2" data-element_type="widget" data-widget_type="text-editor.default">
												<div class="elementor-widget-container">
													<div class="elementor-text-editor elementor-clearfix">
														<ul>
															{* BEGIN: loop *}
															{foreach $SUB_MENU as $index => $SUB}
																{if $index%2 != 0}

																{/if}
																<li class="{$SUB->classes} || ">
																	<a href="{$SUB->url}" title="{$SUB->description}" class=" || ">
																		{$SUB->title}
																	</a>
																</li>

															{/foreach}
															{* END: loop *}
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-06777b8 wpb-col-sm-100 hidden-sm hidden-xs" data-id="06777b8" data-element_type="column">
									<div class="elementor-column-wrap elementor-element-populated">
										<div class="elementor-widget-wrap">
											<div class="elementor-element elementor-element-45f87c2 title elementor-widget elementor-widget-heading" data-id="45f87c2" data-element_type="widget" data-widget_type="heading.default">
												<div class="elementor-widget-container">
													<h2 class="elementor-heading-title elementor-size-default">REcent Post</h2>
												</div>
											</div>
											<div class="elementor-element elementor-element-38b1f54 elementor-widget elementor-widget-bwp_recent_post" data-id="38b1f54" data-element_type="widget" data-widget_type="bwp_recent_post.default">
												<div class="elementor-widget-container">
													<div class="bwp-recent-post blog-menu">
														<div class="block">
															<div class="block_content">
																<div id="recent_post_5963185231614906621" class="row">
																	<div class="post-grid col-lg-12 col-md-12 col-sm-12 col-xs-12 post-3976 post type-post status-publish format-standard has-post-thumbnail hentry category-backpack category-life-style category-organic tag-beauty tag-ear-care">
																		<div class="item">
																			<a class="post-thumbnail" href="https://wpbingosite.com/wordpress/broxi/2018/05/30/traveling-solo-is-awesome/" aria-hidden="true">
																				<img width="720" height="484" src="https://wpbingosite.com/wordpress/broxi/wp-content/uploads/2018/05/Blog-5-720x484.jpg" class="attachment-thumbnail size-thumbnail wp-post-image lazyloaded" alt="Traveling Solo Is Awesome" loading="lazy" data-ll-status="loaded">
																				<noscript><img width="720" height="484" src="https://wpbingosite.com/wordpress/broxi/wp-content/uploads/2018/05/Blog-5-720x484.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Traveling Solo Is Awesome" loading="lazy" /></noscript>
																			</a>
																			<div class="post-content">
																				<h2 class="entry-title"><a href="https://wpbingosite.com/wordpress/broxi/2018/05/30/traveling-solo-is-awesome/">Traveling Solo Is Awesome</a></h2>
																				<div class="day-cmt">
																					<span class="entry-date"><time class="entry-date" datetime="2018-05-30T04:42:28+00:00">30.May.2018</time></span>
																					<div class="comments-link"> <a href="#respond"> 4<span> Comments</span> </a></div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="post-grid col-lg-12 col-md-12 col-sm-12 col-xs-12 post-3974 post type-post status-publish format-image has-post-thumbnail hentry category-backpack category-life-style category-organic category-salads tag-baby-needs post_format-post-format-image">
																		<div class="item">
																			<a class="post-thumbnail" href="https://wpbingosite.com/wordpress/broxi/2018/05/30/a-beautiful-sunday-morning/" aria-hidden="true">
																				<img width="720" height="484" src="https://wpbingosite.com/wordpress/broxi/wp-content/uploads/2018/05/Blog-7-720x484.jpg" class="attachment-thumbnail size-thumbnail wp-post-image lazyloaded" alt="A Beautiful Sunday Morning" loading="lazy" data-ll-status="loaded">
																				<noscript><img width="720" height="484" src="https://wpbingosite.com/wordpress/broxi/wp-content/uploads/2018/05/Blog-7-720x484.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="A Beautiful Sunday Morning" loading="lazy" /></noscript>
																			</a>
																			<div class="post-content">
																				<h2 class="entry-title"><a href="https://wpbingosite.com/wordpress/broxi/2018/05/30/a-beautiful-sunday-morning/">A Beautiful Sunday Morning</a></h2>
																				<div class="day-cmt">
																					<span class="entry-date"><time class="entry-date" datetime="2018-05-30T04:40:39+00:00">30.May.2018</time></span>
																					<div class="comments-link"> <a href="#respond"> 1<span> Comment</span> </a></div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="post-grid col-lg-12 col-md-12 col-sm-12 col-xs-12 post-3971 post type-post status-publish format-image has-post-thumbnail hentry category-backpack category-life-style category-organic tag-electric tag-fashion post_format-post-format-image">
																		<div class="item">
																			<a class="post-thumbnail" href="https://wpbingosite.com/wordpress/broxi/2018/05/30/kitchen-inspired-on-japanese/" aria-hidden="true">
																				<img width="720" height="484" src="https://wpbingosite.com/wordpress/broxi/wp-content/uploads/2018/05/Blog-6-720x484.jpg" class="attachment-thumbnail size-thumbnail wp-post-image lazyloaded" alt="Kitchen inspired on Japanese" loading="lazy" data-ll-status="loaded">
																				<noscript><img width="720" height="484" src="https://wpbingosite.com/wordpress/broxi/wp-content/uploads/2018/05/Blog-6-720x484.jpg" class="attachment-thumbnail size-thumbnail wp-post-image" alt="Kitchen inspired on Japanese" loading="lazy" /></noscript>
																			</a>
																			<div class="post-content">
																				<h2 class="entry-title"><a href="https://wpbingosite.com/wordpress/broxi/2018/05/30/kitchen-inspired-on-japanese/">Kitchen inspired on Japanese</a></h2>
																				<div class="day-cmt">
																					<span class="entry-date"><time class="entry-date" datetime="2018-05-30T04:33:26+00:00">30.May.2018</time></span>
																					<div class="comments-link"> <a href="#respond"> 1<span> Comment</span> </a></div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
{/block}