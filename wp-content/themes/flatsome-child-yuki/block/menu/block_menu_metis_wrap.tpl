{php}
	wp_register_style( 'metismenu-style', LIBS_PATH . '/metismenu' .'/jquery.metisMenu.css', 'all' );
	wp_register_script( 'metismenu-script', LIBS_PATH . '/metismenu' .'/jquery.metismenu.js', 'all' );
	wp_enqueue_style( 'metismenu-style' );
	wp_enqueue_script( 'metismenu-script' );
{/php}

<div class="mkdf-widget-title-holder">
                    <span class="mkdf-active-hover">
																			<svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-left">
																				<path class="custom-heading-bg" d="M2 0C0.9 0 0 0.9 0 2l2 23.8c0 1.1 0.9 2 2 2h3.9V0H2z"></path>
																			</svg>
																			<span class="mkdf-active-hover-middle custom-heading-bg-middle"></span>
																			<svg xmlns="http://www.w3.org/2000/svg" width="15" height="100%" preserveAspectRatio="none" viewBox="-0.5 -0.5 11 29" class="mkdf-active-hover-right">
																				<path class="custom-heading-bg" d="M5.9 0c1.1 0 2 0.9 2 2L5 25.8c-0.2 1.6-1.1 1.9-3 2H0V0H5.9"></path>
																			</svg>
																		</span>
	<h5 class="mkdf-widget-title text-nowrap">{$BLOCK_TITLE}</h5>
</div>

<nav class="clearfix panel metismenu | unselectable">
	<aside class="sidebar">
		<nav class="sidebar-nav">
			<ul id="menu_{$MENU_ID}">
				{$BLOCK_CONTENT}
			</ul>
		</nav>
	</aside>
</nav>

<script type="text/javascript">
	$(function () {
		$('#menu_{$MENU_ID}').metisMenu({
			toggle: false
		});
	});
</script>