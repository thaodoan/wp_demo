{* BEGIN: main *}
<div class="mt-4">
    <div class="">
        <h4 class="h5">{$BLOCK_TITLE} ({$COUNT})</h4>
        <div class="is-divider small"></div>
    </div>

    <div class="list_product_posts">
        {$BLOCK_CONTENT}
    </div>
</div>
{* END: main *}