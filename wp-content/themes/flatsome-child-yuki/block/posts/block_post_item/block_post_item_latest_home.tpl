{* BEGIN: loop *}
<div class="post-grid col-lg-12 col-md-12 col-sm-12 col-xs-12 post-3976 post type-post status-publish format-standard has-post-thumbnail hentry category-backpack category-life-style category-organic tag-beauty tag-ear-care">
    <div class="item">
        <a class="post-thumbnail" href="{$LINK}" aria-hidden="true">
            <img width="720" height="484" src="{$THUMB->img_src}" class="attachment-thumbnail size-thumbnail wp-post-image" alt="{$TITLE}" >
        </a>
        <div class="post-content">
            <h2 class="entry-title"><a href="{$LINK}">{$TITLE}</a></h2>
            <div class="day-cmt">
                <span class="entry-date"><time class="entry-date" >{$PUBLISH_DATE}</time></span>
            </div>
        </div>
    </div>
</div>

{* BEGIN: loop *}