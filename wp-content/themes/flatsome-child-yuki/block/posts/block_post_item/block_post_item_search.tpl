{* BEGIN: loop *}
<div class="post_item_search col-6 col-sm-4 col-md-4 col-lg-3">
    <div class="post-item">
        <a href="{$LINK}" class="">
            <div class="box box-text-bottom box-blog-post has-hover">
                <div class="box-image">
                    <div class="image-cover" style="">
                        <img class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="{$THUMB->img_src}">
                    </div>
                </div>
                <div class="box-text text-center">
                    <div class="box-text-inner blog-post-inner">
                        <h5 class="post-title is-large ">{$TITLE}</h5>
                        <div class="is-divider"></div>
                        <p class="from_the_blog_excerpt ">{$EXCERPT}</p>
                    </div>
                </div>
                {$PUBLISH_DATE}
            </div>
        </a>
    </div>
</div>
{* BEGIN: loop *}