{* BEGIN: loop *}
<li class=" | ">
    <a href="{$LINK}" title="{$TITLE}">
        <img class="post-thumb" src="{$THUMB->img_src}" width="{$THUMB->img_width}" srcset="{$THUMB->img_srcset}" sizes="100px" alt="{$THUMB->img_alt}">
        <div class="post-title text-size-2">{$TITLE}</div>
    </a>
    <div class="post-date text-size-n1">{$PUBLISH_DATE}</div>
    <div class="post-excerpt text2line text-size-n1">{$EXCERPT}</div>
</li>
{* BEGIN: loop *}