{* BEGIN: loop *}
<div class="col-6 col-sm-4 col-md-4 col-lg-3">
    <div class="post-item">
        <a href="{$LINK}" class="">
            <div class="box box-text-bottom box-blog-post has-hover">
                <div class="box-image">
                    <div class="image-cover" style="">
                        {$THUMB}
                    </div>
                </div>
                <div class="box-text text-center">
                    <div class="box-text-inner blog-post-inner">
                        <h5 class="post-title is-large ">{$TITLE}</h5>
                        <div class="is-divider"></div>
                        <p class="from_the_blog_excerpt ">{$EXCERPT}</p>
                    </div>
                </div>
                <div class="badge absolute top post-date badge-outline">
                    <div class="badge-inner">
                        <span class="post-date-day">{$DATE}</span><br>
                        <span class="post-date-month is-xsmall">{$MONTH}/{$YEAR}</span>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
{* BEGIN: loop *}