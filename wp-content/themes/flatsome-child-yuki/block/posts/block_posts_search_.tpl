<form role="search" method="get" class="" action="{$FORM_ACTION}">
    <div id="search" class="input-group">
        <input type="search" value="" name="s" placeholder="{$SEARCH_PLACEHOLDER}" class="form-control input-lg">
        <span class="input-group-btn"><button type="submit" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button></span>
    </div>
</form>



