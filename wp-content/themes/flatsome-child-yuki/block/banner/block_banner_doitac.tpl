<div class="item">
    <div class="item-image">
        <img src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2095%2095'%3E%3C/svg%3E" alt="{$BANNER.src_full}" class="" loading="lazy" data-lazy-src="{$BANNER.src_full}">
        <noscript><img src="{$BANNER.src_full}" alt="{$BANNER.alt}"></noscript>
    </div>
</div>
