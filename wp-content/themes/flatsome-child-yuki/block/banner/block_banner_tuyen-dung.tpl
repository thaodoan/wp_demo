{* BEGIN: main *}

<div class="bwp-image">
    {* BEGIN: loop *}

    {* BEGIN: type_image_link *}
    {if $BANNER.type_image_link}
        <a id="{$BANNER.link_id}" class="{$BANNER.link_class}>" rel="{$BANNER.link_rel}" href="{$BANNER.link}" target="{$BANNER.target}" title="{$BANNER.link_title}">
            <img width="{$BANNER.img_width}" alt="{$BANNER.alt}" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" data-lazy-src="{$BANNER.src_full}" class=" | "/>
            <noscript><img src="{$BANNER.src_full}" alt="{$BANNER.alt}"></noscript>
        </a>
    {/if}
    {* END: type_image_link *}

    {* BEGIN: type_image *}
    {if $BANNER.type_image}
        <img width="{$BANNER.img_width}" alt="{$BANNER.alt}" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E"  data-lazy-src="{$BANNER.src_full}" class=" | "/>
        <noscript><img src="{$BANNER.src_full}" alt="{$BANNER.alt}"></noscript>
    {/if}
    {* END: type_image *}

    {* BEGIN: loop *}
</div>
{* END: main *}