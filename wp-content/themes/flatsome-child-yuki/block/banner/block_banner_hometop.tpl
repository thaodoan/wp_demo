{* BEGIN: main *}

{* BEGIN: loop *}

<rs-slide data-key="rs-{$BANNER.src_full}" data-title="Slide" data-thumb="" data-anim="ei:d;eo:d;s:d;r:default;t:papercut;sl:d;">
    <img loading="lazy" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201101%20531'%3E%3C/svg%3E" title="slider-13" width="1101" height="531" data-bg="f:auto;" class="rev-slidebg" data-no-retina data-lazy-src="{$BANNER.src_full}">
    <noscript><img loading="lazy" src="{$BANNER.src_full}" title="slider-13" width="1101" height="531" data-bg="f:auto;" class="rev-slidebg" data-no-retina></noscript>
</rs-slide>


<div class="elementor-element elementor-element-b316f60 elementor-widget elementor-widget-bwp_image" data-id="b316f60" data-element_type="widget" data-widget_type="bwp_image.default">
    <div class="elementor-widget-container">
        <div class="bwp-widget-banner layout-4 style1">
            <div class="bg-banner">
                <div class="banner-wrapper banners">
                    <div class="bwp-image">
                        <a href="{$BANNER.link}">
                            <img src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%200%200'%3E%3C/svg%3E" alt="Banner Image" class=""  data-lazy-src="{$BANNER.src_full}">
                            <noscript><img src="{$BANNER.src_full}" alt="{$BANNER.alt}"></noscript>
                        </a>
                    </div>
                    <div class="banner-wrapper-infor">
                        <div class="info">
                            <div class="content">
                                <h3 class="title-banner">{$BANNER.link_title}</h3>
                                <a class="button" href="{$BANNER.link}">{$BANNER.info}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{* BEGIN: loop *}

{* END: main *}