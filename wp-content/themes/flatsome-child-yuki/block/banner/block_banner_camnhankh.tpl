<div class="testimonial-content">
    <div class="item">
        <div class="testimonial-item">
            <div class="star star-5"></div>
            <div class="testimonial-customer-position">
                <p class="post-excerpt">{$BANNER.info}</p>
            </div>
        </div>
        <div class="testimonial-info">
            <div class="testimonial-image">
                <img width="95" height="95" src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2095%2095'%3E%3C/svg%3E" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" loading="lazy" data-lazy-src="{$BANNER.src_full}" />
                <noscript><img width="95" height="95" src="{$BANNER.src_full}" class="attachment-thumbnail size-thumbnail wp-post-image" alt="" loading="lazy" /></noscript>
            </div>
            <div class="content">
                <h2 class="testimonial-customer-name">{$BANNER.title}</h2>
                <div class="testimonial-job">{$BANNER.link_class}</div>
            </div>
        </div>
    </div>
</div>
