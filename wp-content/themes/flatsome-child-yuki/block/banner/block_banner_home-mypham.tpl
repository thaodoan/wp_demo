<div class="bwp-image">
    <a href="{$BANNER.link}">
        <img src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2095%2095'%3E%3C/svg%3E" alt="{$BANNER.src_full}" class="" loading="lazy" data-lazy-src="{$BANNER.src_full}">
        <noscript><img src="{$BANNER.src_full}" alt="{$BANNER.alt}"></noscript>
    </a>
</div>
<div class="banner-wrapper-infor">
    <div class="info">
        <div class="content">
            <h3 class="title-banner">{$BANNER.link_title}</h3>
            <a class="button" href="{$BANNER.link}">{$BANNER.info}</a>
        </div>
    </div>
</div>