<?php
/*
Template name: MHS - Flatsome Child - Lien he
*/
echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
echo do_shortcode('[header_extend]');
?>

<div class="fix-more-padding row">



<div id="content" class="col-md-12 col-sm-12">
    <div class="text-center">
        <?php if(has_post_thumbnail()):?>
            <img src="<?= get_the_post_thumbnail_url(); ?>" alt="<?= get_the_title(); ?>">
        <?php endif; ?>
    </div>
    <div class="page-contact">
        <p class="text-center font-weight-bold text-size-4">
            Liên hệ với chúng tôi tại:
        </p>
        <div class="row">
            <div class="col-lg-6">
                <?= do_shortcode('[contact-form-7 id="849" title="Form Liên hệ"]')?>
            </div>
            <div class="content col-lg-6 mt-4 mt-lg-0">
                <?php
                // TO SHOW THE PAGE CONTENTS
                while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                    <?php the_content(); ?> <!-- Page Content -->
                <?php
                endwhile; //resetting the page loop
                wp_reset_query(); //resetting the page query
                ?>
            </div>
        </div>
    </div>
</div>

</div>

<?php
echo do_shortcode('[footer_extend]');
echo do_shortcode('[get_footer]');
echo do_shortcode('[wrap_end]');
?>

