<header id='bwp-header' class="bwp-header header-v4">
    <div id="bwp-topbar" class="topbar-v2 hidden-sm hidden-xs">
        <div class="topbar-inner">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 topbar-left hidden-sm hidden-xs">
                        <div class="ship"> <?= $GLOBALS['cgv']['topbar_text'] ?></div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 topbar-right">
                        <div class="email hidden-xs"> <i class="fa fa-envelope-o"></i>
                            <a href="mailto:<?= $GLOBALS['cgv']['email'] ?>"><?= $GLOBALS['cgv']['email'] ?></a>
                        </div>
                        <ul class="social-link">
                            <?= do_shortcode("[block_menu location='social-icon-1' tpl='social']")?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="header-mobile">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-3 col-3 header-left">
                    <div class="navbar-header"> <button type="button" id="show-megamenu"  class="navbar-toggle"> <span>Menu</span> </button></div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6 header-center ">
                    <div class="wpbingoLogo">
                        <?= do_shortcode('[block_logo]'); ?>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-3 col-3 header-right">
                    <div class="wpbingo-verticalmenu-mobile">
                        <div class="navbar-header"> <button type="button" id="show-verticalmenu"  class="navbar-toggle"> <span>Vertical</span> </button></div>
                    </div>
                    <div class="broxi-topcart dropdown">
                        <div class="dropdown mini-cart top-cart" data-text_added="Product was added to cart successfully!">
                            <div class="remove-cart-shadow"></div>
                            <?= do_shortcode('[block_cart_icon]'); ?>
                            <div class="cart-popup dropdown">
                                <?= do_shortcode('[block_mini_cart]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-desktop">
        <div class='header-wrapper' data-sticky_header="0">
            <div class="container">
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-12 xol-12">
                        <div class="wpbingoLogo">
                            <?= do_shortcode('[block_logo]'); ?>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-5 col-md-12 col-sm-12 col-12 header-left">
                        <div class="header-search-form hidden-sm hidden-xs">
                            <?= do_shortcode('[block_product_search]'); ?>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-12 col-sm-12 col-12 header-right">
                        <div class="header-phone">
                            <div class="phone"> <label class="font-bold">HOTLINE</label>
                                <a href="tel:<?= do_shortcode("[cgv phone]")?>">
                                    <?= do_shortcode("[cgv phone_format]")?>
                                </a>
                            </div>
                        </div>
                        <div class="header-page-link">
                            <div class="broxi-topcart dropdown">
                                <div class="dropdown mini-cart top-cart" >
                                    <div class="remove-cart-shadow"></div>
                                    <?= do_shortcode('[block_cart_icon]'); ?>
                                    <div class="cart-popup dropdown">
                                        <?= do_shortcode('[block_mini_cart]'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="content-header-bottom">
                    <div class="header-vertical-menu">
                        <div class="categories-vertical-menu hidden-sm hidden-xs show"
                             data-textmore="Danh mục khác ..."
                             data-textclose="Đóng"
                             data-max_number_1530="12"
                             data-max_number_1200="12"
                             data-max_number_991="12">
                            <h3 class="widget-title"><i class="fa fa-bars" aria-hidden="true"></i><?= __("Category", "woocommerce")?></h3>
                            <div class="verticalmenu">
                                <div  class="bwp-vertical-navigation primary-navigation navbar-mega">
                                    <div class="menu-vertical-menu-container">
                                        <?= do_shortcode('[block_product_cat tpl="home" numcat=100]')?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpbingo-menu-mobile header-menu">
                        <div class="header-menu-bg">
                            <div class="wpbingo-menu-wrapper">
                                <div class="megamenu">
                                    <nav class="navbar-default">
                                        <div  class="bwp-navigation primary-navigation navbar-mega" data-text_close = "Đóng">
                                            <div class="float-menu">
                                                <nav id="main-navigation" class="std-menu clearfix">
                                                    <div class="menu-main-menu-container">
                                                        <?= do_shortcode("[block_menu location='primary' tpl='desktop']"); ?>
                                                    </div>
                                                </nav>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</header>


<div id="bwp-main" class="bwp-main">
    <div id="main-content" class="main-content">
        <div id="primary" class="content-area container">
            <div id="content" class="site-content" role="main">
                <article id="post-11328" class="post-11328 page type-page status-publish hentry">
                    <div class="entry-content clearfix">
                        <div data-elementor-type="wp-page" data-elementor-id="11328" class="elementor elementor-11328" data-elementor-settings="[]">
                            <div class="elementor-inner">
                                <div class="elementor-section-wrap">
                                    <section class="elementor-section elementor-top-section elementor-element elementor-element-8455916 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="8455916" data-element_type="section">
                                        <div class="elementor-container elementor-column-gap-default">
                                            <div class="elementor-row">
                                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-b2122cf box-vertical2 m-t-30" data-id="b2122cf" data-element_type="column">
                                                    <div class="elementor-column-wrap">
                                                        <div class="elementor-widget-wrap"></div>
                                                    </div>
                                                </div>
                                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-6827057 box-slider2" data-id="6827057" data-element_type="column">
                                                    <div class="elementor-column-wrap elementor-element-populated">
                                                        <div class="elementor-widget-wrap">