</div>
</div>
</div>
</div>
</article>
</div>
</div>
</div>
</div>

<footer id="bwp-footer" class="bwp-footer footer-3">
    <div data-elementor-type="wp-post" data-elementor-id="11277" class="elementor elementor-11277" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-e4a01d1 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="e4a01d1" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-ebc8f1c wpb-col-sm-50" data-id="ebc8f1c" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-19670e2 elementor-widget elementor-widget-image" data-id="19670e2" data-element_type="widget" data-widget_type="image.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-image">
                                                    <?= do_shortcode('[block_logo]');?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-6d03ff3 elementor-widget elementor-widget-text-editor" data-id="6d03ff3" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix">
                                                    <p class="">
                                                        <?= get_theme_mod( 'banner_text' ); ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-56c5b07 wpb-col-sm-50" data-id="56c5b07" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <?= do_shortcode("[block_menu location='primary' tpl='footer']"); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-ebc8f1c wpb-col-sm-50" data-id="ebc8f1c" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element">
                                            <div class="font-weight-bold footer-phonenumber">HOTLINE: </div>
                                            <div class="footer-phonenumber-text"><p><?= do_shortcode('[cgv phone_format]')?></p></div>
                                        </div>

                                        <div class="elementor-element elementor-element-623f98a elementor-position-left elementor-view-default elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="623f98a" data-element_type="widget" data-widget_type="icon-box.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-icon-box-wrapper">
                                                    <div class="elementor-icon-box-icon">
                                          <span class="elementor-icon elementor-animation-">
                                             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Capa_4" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                <g>
                                                   <g>
                                                      <path d="M256,0C156.748,0,76,80.748,76,180c0,33.534,9.289,66.26,26.869,94.652l142.885,230.257    c2.737,4.411,7.559,7.091,12.745,7.091c0.04,0,0.079,0,0.119,0c5.231-0.041,10.063-2.804,12.75-7.292L410.611,272.22    C427.221,244.428,436,212.539,436,180C436,80.748,355.252,0,256,0z M384.866,256.818L258.272,468.186l-129.905-209.34    C113.734,235.214,105.8,207.95,105.8,180c0-82.71,67.49-150.2,150.2-150.2S406.1,97.29,406.1,180    C406.1,207.121,398.689,233.688,384.866,256.818z"></path>
                                                   </g>
                                                </g>
                                                <g>
                                                   <g>
                                                      <path d="M256,90c-49.626,0-90,40.374-90,90c0,49.309,39.717,90,90,90c50.903,0,90-41.233,90-90C346,130.374,305.626,90,256,90z     M256,240.2c-33.257,0-60.2-27.033-60.2-60.2c0-33.084,27.116-60.2,60.2-60.2s60.1,27.116,60.1,60.2    C316.1,212.683,289.784,240.2,256,240.2z"></path>
                                                   </g>
                                                </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                             </svg>
                                          </span>
                                                    </div>
                                                    <div class="elementor-icon-box-content">
                                                        <h3 class="elementor-icon-box-title"> <span> <?= do_shortcode('[cgv company_address]')?></span></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-d0e9f05 elementor-position-left elementor-view-default elementor-vertical-align-top elementor-widget elementor-widget-icon-box" data-id="d0e9f05" data-element_type="widget" data-widget_type="icon-box.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-icon-box-wrapper">
                                                    <div class="elementor-icon-box-icon">
                                          <span class="elementor-icon elementor-animation-">
                                             <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="Capa_1" x="0px" y="0px" viewBox="0 0 477.867 477.867" style="enable-background:new 0 0 477.867 477.867;" xml:space="preserve">
                                                <g>
                                                   <g>
                                                      <path d="M460.8,68.267H17.067C7.641,68.267,0,75.908,0,85.333v307.2c0,9.426,7.641,17.067,17.067,17.067H460.8    c9.426,0,17.067-7.641,17.067-17.067v-307.2C477.867,75.908,470.226,68.267,460.8,68.267z M432.811,102.4L238.933,251.529    L45.056,102.4H432.811z M443.733,375.467h-409.6V137.062L228.54,286.6c6.13,4.707,14.657,4.707,20.787,0l194.406-149.538V375.467z    "></path>
                                                   </g>
                                                </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                                <g> </g>
                                             </svg>
                                          </span>
                                                    </div>
                                                    <div class="elementor-icon-box-content">
                                                        <h3 class="elementor-icon-box-title"> <span> <?= do_shortcode('[cgv email]')?></span></h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="font-weight-bold working-time">GIỜ LÀM VIỆC:</p>
                                        <div class="working-time-text"><?= do_shortcode('[cgv working_time]')?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-56c5b07 wpb-col-sm-50" data-id="56c5b07" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="wpb_map_wraper">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.306422742283!2d106.65120121533427!3d10.787826661931792!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752eca726230eb%3A0xc89deb4e06032fa4!2zNTI0IEzDvSBUaMaw4budbmcgS2nhu4d0LCBQaMaw4budbmcgNywgVMOibiBCw6xuaCwgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2shk!4v1614070331698!5m2!1svi!2shk" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-294e4fa elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="294e4fa" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 text-center elementor-top-column elementor-element elementor-element-c42e18a wpb-col-sm-50 footer-coppyright" data-id="c42e18a" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-1faf130 elementor-widget__width-auto elementor-widget-mobile__width-inherit elementor-widget-tablet__width-auto elementor-widget elementor-widget-text-editor" data-id="1faf130" data-element_type="widget" data-widget_type="text-editor.default">
                                            <div class="elementor-widget-container">
                                                <div class="elementor-text-editor elementor-clearfix">
                                                    <?= do_shortcode('[block_copyright]')?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="zalo-container right" style="bottom:20px; right: 100px">
        <a id="zalo-btn" href="https://zalo.me/<?= do_shortcode('[cgv phone]'); ?>" target="_blank" rel="noopener noreferrer nofollow">
            <div class="zalo-ico zalo-has-notify">
                <div class="zalo-ico-main">
                    <img alt="Contact Me on Zalo" class="" src="<?php echo ASSETS_PATH . '/img/zalo-1.png'?>">
                </div>
                <em></em>
            </div>
        </a>
    </div>
</footer>