



<!-- default css -->
<link rel="StyleSheet" href="<?= CSS_PATH.'/flatsome.css'; ?>" type='text/css' media='all' />

<link rel="StyleSheet" href="<?= CSS_PATH.'/flatsome-shop.css'; ?>" type='text/css' media='all' />

<!-- default css -->

<!-- default css -->

<link rel="StyleSheet" href="<?= CSS_PATH . '/' ?>mm-vertical.css">
<link rel='stylesheet' href="<?= CSS_PATH.'/themes_reset.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/shop_reset.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/news.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/news_reset.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/wishlist_reset.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/custom.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/assets.css'; ?>" type='text/css' media='all' />
<link rel='stylesheet' href="<?= CSS_PATH.'/cart_reset.css'; ?>" type='text/css' media='all' />
<?php if(IS_CART || IS_CHECKOUT):?>
    <link rel='stylesheet' href="<?= CSS_PATH.'/checkout_reset.css'; ?>" type='text/css' media='all' />
<?php endif; ?>

<!-- default css -->

<!-- BEGIN: themes css -->
<link rel='stylesheet' href="<?= LIBS_PATH.'/bootstrap/bootstrap.min.css'; ?>" type='text/css' media='all' />
<script>document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
<link media="all" href="/wp-content/cache/autoptimize/css/autoptimize_0193fa20cb019a69fbc74d7b5e4b9b99.css" rel="stylesheet" />
<link rel='dns-prefetch' href='http://fonts.googleapis.com/' />

<link rel='stylesheet' id='dashicons-css'  href='/wp-includes/css/dashicons.min40df.css?ver=5.6' type='text/css' media='all' />
<link rel='stylesheet' id='broxi-fonts-css'  href='https://fonts.googleapis.com/css?family=Jost%3Aital%2Cwght%400%2C200%3B0%2C300%3B0%2C400%3B0%2C500%3B0%2C600%3B0%2C700%3B0%2C800%3B0%2C900%3B1%2C200%3B1%2C300%3B1%2C400%3B1%2C500%3B1%2C600%3B1%2C700%3B1%2C800%3B1%2C900%7COpen%2BSans&amp;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href='/wp-content/uploads/elementor/css/custom-frontend.mined7f.css?ver=1610348539' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-9482-css'  href='/wp-content/cache/autoptimize/css/autoptimize_single_f07d5122c2602858e41016d22b6c7df3ed7f.css?ver=1610348539' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-11328-css'  href='/wp-content/cache/autoptimize/css/autoptimize_single_51dbb3834d5ced55bb1fac03c6f62c8b68b1.css?ver=1610454890' type='text/css' media='all' />
<script type="text/template" id="tmpl-variation-template"><div class="woocommerce-variation-description">{{{ data.variation.variation_description }}}</div>
    <div class="woocommerce-variation-price">{{{ data.variation.price_html }}}</div>
    <div class="woocommerce-variation-availability">{{{ data.variation.availability_html }}}</div>
</script>
<noscript>
    <style>.woocommerce-   gallery{ opacity: 1 !important; }</style>
</noscript>
<script type="text/javascript">
    function setREVStartSize(e){
        //window.requestAnimationFrame(function() {
        window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;
        window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;
        try {
            var pw = document.getElementById(e.c).parentNode.offsetWidth,
                newh;
            pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
            e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
            e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
            e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
            e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
            e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
            e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
            e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);
            if(e.layout==="fullscreen" || e.l==="fullscreen")
                newh = Math.max(e.mh,window.RSIH);
            else{
                e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
                for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];
                e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
                e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
                for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];

                var nl = new Array(e.rl.length),
                    ix = 0,
                    sl;
                e.tabw = e.tabhide>=pw ? 0 : e.tabw;
                e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
                e.tabh = e.tabhide>=pw ? 0 : e.tabh;
                e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;
                for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
                sl = nl[0];
                for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}
                var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);
                newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
            }
            if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));
            document.getElementById(e.c).height = newh+"px";
            window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";
        } catch(e){
            console.log("Failure at Presize of Slider:" + e)
        }
        //});
    };
</script>
<noscript>
    <style id="rocket-lazyload-nojs-css">.rll-youtube-player, [data-lazy-src]{display:none !important;}</style>
</noscript>
<!--END: themes css-->