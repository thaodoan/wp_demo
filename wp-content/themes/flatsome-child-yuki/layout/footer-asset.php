

<!-- BEGIN: themes assets-->
<link href="https://fonts.googleapis.com/css?family=Roboto:500%2C700%2C400" rel="stylesheet" property="stylesheet" media="all" type="text/css" >
<script type="text/javascript">(function () {
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    })()
</script> <script type="text/javascript">if(typeof revslider_showDoubleJqueryError === "undefined") {
        function revslider_showDoubleJqueryError(sliderID) {
            var err = "<div class='rs_error_message_box'>";
            err += "<div class='rs_error_message_oops'>Oops...</div>";
            err += "<div class='rs_error_message_content'>";
            err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
            err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
            err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
            err += "</div>";
            err += "</div>";
            var slider = document.getElementById(sliderID); slider.innerHTML = err; slider.style.display = "block";
        }
    }
</script>
<script type='text/javascript' id='dokan-i18n-jed-js-extra'>
    var dokan = {"nonce":"1d7f7759b4","seller":{"available":"Available","notAvailable":"Not Available"},"delete_confirm":"Are you sure?","wrong_message":"Something went wrong. Please try again.","vendor_percentage":"100","commission_type":"percentage","rounding_precision":"6","mon_decimal_point":".","product_types":["simple"],"rest":{"root":"","nonce":"8ad6055db4","version":"dokan\/v1"},"api":null,"libs":[],"routeComponents":{"default":null},"routes":[],"urls":{"assetsUrl":""}};</script> <script type='text/javascript' id='elementor-frontend-js-before'>var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1200,"xl":1440,"xxl":1600},"version":"3.0.16","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"http:\/\/wpbingosite.com\/wordpress\/broxi\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":11328,"title":"Home%20Page%205%20%E2%80%93%20Broxi%20%E2%80%93%20Organic%20%26%20Food%20WooCommerce%20WordPress%20Theme","excerpt":"","featuredImage":false}};</script> <script type="text/javascript">setREVStartSize({c: 'rev_slider_138_1',rl:[1240,1200,778,480],el:[531,525,393,300],gw:[1101,800,600,480],gh:[531,525,393,300],type:'standard',justify:'',layout:'fullwidth',mh:"0"});
    var	revapi138,
        tpj;
    function revinit_revslider1381() {
        jQuery(function() {
            tpj = jQuery;
            revapi138 = tpj("#rev_slider_138_1");
            if(revapi138==undefined || revapi138.revolution == undefined){
                revslider_showDoubleJqueryError("rev_slider_138_1");
            }else{
                revapi138.revolution({
                    visibilityLevels:"1240,1200,778,480",
                    gridwidth:"1101,800,600,480",
                    gridheight:"531,525,393,300",
                    minHeight:"none",
                    spinner:"spinner0",
                    perspective:600,
                    perspectiveType:"global",
                    keepBPHeight:true,
                    editorheight:"531,525,393,300",
                    responsiveLevels:"1240,1200,778,480",
                    progressBar:{disableProgressBar:true},
                    navigation: {
                        wheelCallDelay:1000,
                        onHoverStop:false,
                        bullets: {
                            enable:true,
                            tmp:"<span class=\"tp-bullet-title\"></span>",
                            style:"bullet-4",
                            hide_onmobile:true,
                            hide_under:"480px",
                            h_align:"left",
                            h_offset:70,
                            v_offset:40,
                            space:10
                        }
                    },
                    fallbacks: {
                        allowHTML5AutoPlayOnAndroid:true
                    },
                });
            }

        });
    } // End of RevInitScript
    var once_revslider1381 = false;
    if (document.readyState === "loading") {document.addEventListener('readystatechange',function() { if((document.readyState === "interactive" || document.readyState === "complete") && !once_revslider1381 ) { once_revslider1381 = true; revinit_revslider1381();}});} else {once_revslider1381 = true; revinit_revslider1381();}
</script> <script>var htmlDivCss = unescape("%23rev_slider_138_1_wrapper%20.bullet-4.tp-bullets%20%7B%0A%7D%0A%23rev_slider_138_1_wrapper%20.bullet-4.tp-bullets%3Abefore%20%7B%0A%09content%3A%27%20%27%3B%0A%09position%3Aabsolute%3B%0A%09width%3A100%25%3B%0A%09height%3A100%25%3B%0A%09background%3Atransparent%3B%0A%09padding%3A10px%3B%0A%09margin-left%3A-10px%3Bmargin-top%3A-10px%3B%0A%09box-sizing%3Acontent-box%3B%0A%7D%0A%23rev_slider_138_1_wrapper%20.bullet-4%20.tp-bullet%20%7B%0A%09width%3A13px%3B%0A%09height%3A13px%3B%0A%09position%3Aabsolute%3B%0A%09background%3Argba%28255%2C%20255%2C%20255%2C%200.36%29%3B%0A%09border-radius%3A50%25%3B%0A%09cursor%3A%20pointer%3B%0A%09box-sizing%3Acontent-box%3B%0A%20%20width%3A8px%3B%0A%20%20height%3A8px%3B%0A%7D%0A%23rev_slider_138_1_wrapper%20.bullet-4%20.tp-bullet%3Ahover%2C%0A%23rev_slider_138_1_wrapper%20.bullet-4%20.tp-bullet.selected%20%7B%0A%09background%3A%23ffffff%3B%0A%7D%0A%0A%0A");
    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
    if(htmlDiv) {
        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
    }else{
        var htmlDiv = document.createElement('div');
        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
    }
</script> <script>window.lazyLoadOptions = {
        elements_selector: "img[data-lazy-src],.rocket-lazyload",
        data_src: "lazy-src",
        data_srcset: "lazy-srcset",
        data_sizes: "lazy-sizes",
        class_loading: "lazyloading",
        class_loaded: "lazyloaded",
        threshold: 300,
        callback_loaded: function(element) {
            if ( element.tagName === "IFRAME" && element.dataset.rocketLazyload == "fitvidscompatible" ) {
                if (element.classList.contains("lazyloaded") ) {
                    if (typeof window.jQuery != "undefined") {
                        if (jQuery.fn.fitVids) {
                            jQuery(element).parent().fitVids();
                        }
                    }
                }
            }
        }};
    window.addEventListener('LazyLoad::Initialized', function (e) {
        var lazyLoadInstance = e.detail.instance;

        if (window.MutationObserver) {
            var observer = new MutationObserver(function(mutations) {
                var image_count = 0;
                var iframe_count = 0;
                var rocketlazy_count = 0;

                mutations.forEach(function(mutation) {
                    for (i = 0; i < mutation.addedNodes.length; i++) {
                        if (typeof mutation.addedNodes[i].getElementsByTagName !== 'function') {
                            return;
                        }

                        if (typeof mutation.addedNodes[i].getElementsByClassName !== 'function') {
                            return;
                        }

                        images = mutation.addedNodes[i].getElementsByTagName('img');
                        is_image = mutation.addedNodes[i].tagName == "IMG";
                        iframes = mutation.addedNodes[i].getElementsByTagName('iframe');
                        is_iframe = mutation.addedNodes[i].tagName == "IFRAME";
                        rocket_lazy = mutation.addedNodes[i].getElementsByClassName('rocket-lazyload');

                        image_count += images.length;
                        iframe_count += iframes.length;
                        rocketlazy_count += rocket_lazy.length;

                        if(is_image){
                            image_count += 1;
                        }

                        if(is_iframe){
                            iframe_count += 1;
                        }
                    }
                } );

                if(image_count > 0 || iframe_count > 0 || rocketlazy_count > 0){
                    lazyLoadInstance.update();
                }
            } );

            var b      = document.getElementsByTagName("body")[0];
            var config = { childList: true, subtree: true };

            observer.observe(b, config);
        }
    }, false);
</script>
<script defer src="/wp-content/cache/autoptimize/js/autoptimize_840a7490ebe05386aa5c89c37b1ba1ab.js"></script>
<!-- END: themes assets-->

<!-- BEGIN: default assets-->
<script src="<?= LIBS_PATH . '/bootstrap/popper.min.js' ?>"></script>

<link defer rel='stylesheet' href="<?= LIBS_PATH . '/font-awesome/css/' ?>font-awesome.css" type='text/css' media='all' />
<link rel='stylesheet' href="<?= LIBS_PATH.'/hover-css/css/hover-min.css'; ?>" type='text/css' media='all' />

<!-- END: default assets-->

<!-- EMBED -->
<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = '//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.12&appId=591476614353655&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
<script>var wFb= document.getElementById("fbPage").clientWidth;document.getElementById("fb-page").setAttribute("data-width",wFb); </script>


<div class="chat-now "></div>
<div class="fb-customerchat"
     theme_color="#6699cc"
     attribution=setup_tool
     page_id="<?= do_shortcode('[cgv fb_page_id]'); ?>"
     logged_in_greeting="<?= do_shortcode('[cgv logged_in_greeting]'); ?>"
     logged_out_greeting="<?= do_shortcode('[cgv logged_out_greeting]'); ?>"
     greeting_dialog_delay="15"
     greeting_dialog_display="fade"
>
</div>

<script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>