<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

<?php wp_footer(); ?>
</body>
</html>
