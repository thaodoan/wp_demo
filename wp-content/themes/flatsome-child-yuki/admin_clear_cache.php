<?php
/*
Template name: Clear Cache - Flatsome Child - MHS
*/
echo do_shortcode('[get_header]');
echo do_shortcode('[wrap_begin]');
?>

    <div>
        Clear Smarty cache -- <?= current_time('d-m-Y H:i:s') ?> <br/>
        <?php clearSmartyCache(); ?>
    </div>

<?php
echo do_shortcode('[get_footer]');
echo do_shortcode('[wrap_end]');