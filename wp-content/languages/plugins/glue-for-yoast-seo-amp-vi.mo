��    .      �  =   �      �     �     �            	        '     6  ]   G     �     �     �  4   �                       	   (  
   2     =  5   N     �     �     �     �     �  ;   �  N      "   O     r  6     \   �  :        N  <   [     �  
   �      �  
   �  &   �       	        !  :   *  5   e     �  1  �     �	     �	     �	     	
     
     
  
   .
  �   9
     �
     �
     �
  ;   �
     4     H     U     [     a     o     }  P   �     �  
   �     	            D   )  [   n  '   �     �  >     |   G  P   �       e   "      �     �  *   �     �  +   �     #     5     D  X   U  5   �     �                   #   +           !   &                                )                
   %   ,                      $              (   -          "   .                                        	       *         '    AMP AMP Analytics AMP Header color AMP icon Analytics Analytics code Background color Because your Google Analytics plugin by Yoast is active, your AMP pages will also be tracked. Blockquotes Border color Content colors Currently pages are not supported by the AMP plugin. Default image Design Disabled Enabled Extra CSS Extra code Extra code in %s Generally you'd want this to be your news post types. Glue for Yoast SEO & AMP Hover color Images Joost de Valk Links Make sure to connect your Google Analytics plugin properly. Makes sure the default WordPress AMP plugin uses the proper Yoast SEO metadata Must be at least 32px &times; 32px No underline Optionally add a valid google analytics tracking code. Optionally you can override the default AMP tracking code with your own by putting it below: Pageviews will be tracked using the following account: %s. Please note: Post is enabled by default, feel free to enable any of them. Post meta info color Post types Post types that have AMP support Text color The image must be at least 696px wide. Title color Underline Use Logo Used when a post doesn't have an image associated with it. https://wordpress.org/plugins/glue-for-yoast-seo-amp/ https://yoast.com PO-Revision-Date: 2018-12-17 01:42:34+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: GlotPress/2.4.0-alpha
Language: vi_VN
Project-Id-Version: Plugins - Glue for Yoast SEO &amp; AMP - Stable (latest release)
 AMP Phân tích AMP Màu đầu trang AMP AMP icon Phân tích Mã phân tích Màu nền Bởi vì plugin Google Analytics của bạn bởi Yoast đang hoạt động, trang AMP của bạn cũng sẽ được theo dõi. Khối trích dẫn Màu đường viền Màu nội dung Hiện tại plugin của AMP không hỗ trợ các trang. Ảnh mặc định Thiết kế Tắt Bật Bổ sung CSS Mã bổ sung Mã bổ sung trong %s Nói chung bạn sẽ muốn đây là loại bài viết tin tức của bạn. Glue for Yoast SEO & AMP Màu hover Ảnh Joost de Valk Liên kết Đảm bảo kết nối đúng plugin Google Analytics của bạn. Đảm bảo plugin WordPress AMP mặc định sử dụng đúng metadata của Yoast SEO Phải có ít nhất 32px &times; 32px Không gạch dưới Tùy chọn thêm mã theo dõi phân tích Google hợp lệ. Theo tùy chọn, bạn có thể ghi đè mã theo dõi AMP mặc định của mình bằng cách đặt nó bên dưới: Số lần truy cập trang sẽ được theo dõi bằng tài khoản sau: %s. Xin lưu ý: Bài viết được bật theo mặc định, vui lòng bật bất kỳ tệp nào trong số đó. Màu thông tin meta bài viết Loại bài viết Các loại bài viết có hỗ trợ AMP Màu văn bản Hình ảnh phải rộng ít nhất 696px. Màu tiếu đề Gạch dưới Sử dụng logo Được sử dụng khi một bài đăng không có hình ảnh liên quan đến nó. https://wordpress.org/plugins/glue-for-yoast-seo-amp/ https://yoast.com 