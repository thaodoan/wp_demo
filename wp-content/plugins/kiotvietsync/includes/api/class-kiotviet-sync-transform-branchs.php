<?php
class Kiotviet_Sync_Transform_Branchs
{
    public function saveConfig()
    {
        $data = kiotviet_sync_get_request('data', []);
        if (!empty($data['configBranchStock'])) {
            kiotviet_sync_set_data('config_branch_stock', $data['configBranchStock']);
        }

        if (!empty($data['configBranchOrder'])) {
            kiotviet_sync_set_data('config_branch_order', $data['configBranchOrder']);
        }
        return true;
    }

    public function getConfig()
    {
        $configBranchStock = kiotviet_sync_get_data('config_branch_stock', null);
        $configBranchOrder = kiotviet_sync_get_data('config_branch_order', null);
        return [
            "config_branch_stock" => json_decode(html_entity_decode(stripslashes($configBranchStock)), true),
            "config_branch_order" => json_decode(html_entity_decode(stripslashes($configBranchOrder)), true),
        ];
    }
}
