<?php
require_once plugin_dir_path(dirname(__FILE__)) . '.././vendor/autoload.php';

use Kiotviet\Kiotviet\HttpClient;

class Kiotviet_Sync_Api
{
    public function __construct()
    {
        $this->HttpClient = new HttpClient();
    }

    public function reSyncOrder()
    {
        $orderId = sanitize_key(kiotviet_sync_get_request('order'));
        if (!class_exists('OrderHookAction')) {
            require_once KIOTVIET_PLUGIN_PATH . 'includes/public_actions/OrderHookAction.php';
        }

        $orderHookAction = new OrderHookAction();
        $clientId = kiotviet_sync_get_data('client_id', "");
        $clientSecret = kiotviet_sync_get_data('client_secret', "");
        $retailer = kiotviet_sync_get_data('retailer', "");
        if ($clientId && $clientSecret && $retailer) {
            $response = $orderHookAction->order_processed($orderId);
        } else {
            $response = [
                "msg" => "Website không có kết nối với gian hàng KiotViet",
                "status" => "error",
            ];
        }

        wp_send_json($response);
    }

    public function registerWebhook()
    {
        KiotvietSyncHelper::registerWebhook();
        wp_send_json($this->HttpClient->responseSuccess(true));
    }

    public function removeWebhook()
    {
        KiotvietSyncHelper::removeWebhook();
        wp_send_json($this->HttpClient->responseSuccess(true));
    }

    public function getConfig()
    {
        $clientId = kiotviet_sync_get_data('client_id', "");
        $clientSecret = kiotviet_sync_get_data('client_secret', "");
        $retailer = kiotviet_sync_get_data('retailer', "");
        return wp_send_json($this->HttpClient->responseSuccess(array(
            'client_id' => $clientId,
            'client_secret' => $clientSecret,
            'retailer' => $retailer,
        )));
    }

    public function checkVersion()
    {
        $url = "https://kvsync.kiotviet.dev/versions/kv-sync-version.txt";
        $data = file_get_contents($url);
        if ($data) {
            $data = json_decode($data, true);
            if (KIOTVIET_PLUGIN_VERSION !== $data['version']) {
                wp_send_json($this->HttpClient->responseSuccess(true));
            }
            wp_send_json($this->HttpClient->responseSuccess(false));
        }
        wp_send_json($this->HttpClient->responseSuccess(false));
    }

    public function removeConfig()
    {
        kiotviet_sync_delete_data("client_id");
        kiotviet_sync_delete_data("client_secret");
        kiotviet_sync_delete_data("retailer");
        return wp_send_json($this->HttpClient->responseSuccess(true));
    }

    public function exec()
    {
        $type = sanitize_text_field(kiotviet_sync_get_request('type'));
        $function = sanitize_text_field(kiotviet_sync_get_request('function'));
        $instance = null;
        $response = null;
        switch ($type) {
            case 'category':
                $instance = new Kiotviet_Sync_Transform_Categories();
                switch ($function) {
                    case 'search':
                        $response = $instance->searchCategory();
                        break;
                    case 'deleteAll':
                        $response = $instance->deleteAllCategory();
                        break;
                    case 'add':
                        $response = $instance->addCategory();
                        break;
                    case 'map':
                        $response = $instance->getCategoryMap();
                        break;
                    case 'update':
                        $response = $instance->updateCategory();
                        break;
                    case 'deleteSync':
                        $response = $instance->deleteArrayCategorySync();
                        break;
                    case 'updateMap':
                        $response = $instance->updateCategoryMap();
                        break;
                    default:
                        # code...
                        break;
                }
                break;
            case 'product':
                $instance = new Kiotviet_Sync_Transform_Products();
                switch ($function) {
                    case 'search':
                        $response = $instance->searchProduct();
                        break;
                    case 'deleteAll':
                        $response = $instance->deleteAllProduct();
                        break;
                    case 'add':
                        $response = $instance->addProduct();
                        break;
                    case 'map':
                        $response = $instance->getProductMap();
                        break;
                    case 'update':
                        $response = $instance->updateProduct();
                        break;
                    case 'deleteSync':
                        $response = $instance->deleteArrayProductSync();
                        break;
                    case 'updatePrice':
                        $response = $instance->updatePrice();
                        break;
                    case 'updateStock':
                        $response = $instance->updateStock();
                        break;
                    case 'updateStatusSync':
                        $response = $instance->updateStatusSync();
                        break;

                    default:
                        # code...
                        break;
                }
                break;
            case 'pricebook':
                $instance = new Kiotviet_Sync_Transform_PriceBooks();
                switch ($function) {
                    case 'save':
                        $response = $instance->saveConfig();
                        break;
                    case 'get':
                        $response = $instance->getConfig();
                        break;
                    default:
                        # code...
                        break;
                }
                break;
            case 'branch':
                $instance = new Kiotviet_Sync_Transform_Branchs();
                switch ($function) {
                    case 'save':
                        $response = $instance->saveConfig();
                        break;
                    case 'get':
                        $response = $instance->getConfig();
                        break;
                    default:
                        # code...
                        break;
                }
                break;
            default:
                # code...
                break;
        }
        return wp_send_json($this->HttpClient->responseSuccess($response));
    }
}
