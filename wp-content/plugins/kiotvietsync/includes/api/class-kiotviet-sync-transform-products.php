<?php

class Kiotviet_Sync_Transform_Products
{
    private $KiotvietWcProduct, $wpdb, $retailer;
    public function __construct()
    {
        global $wpdb;
        $this->wpdb = $wpdb;
        $this->retailer = kiotviet_sync_get_data('retailer', "");
        $this->KiotvietWcProduct = new KiotvietWcProduct();
    }

    public function searchProduct()
    {
        $q = sanitize_text_field(kiotviet_sync_get_request('q', ""));
        $data = [];

        $query = "SELECT a.* FROM `{$this->wpdb->prefix}posts` AS a LEFT JOIN `{$this->wpdb->prefix}kiotviet_sync_products` AS b ON a.ID = b.product_id WHERE a.post_status = 'publish' AND a.post_type = 'product' AND b.product_id IS NULL";
        if ($q) {
            $query .= " AND a.post_title LIKE '%" . $q . "%'";
        }

        $query .= " LIMIT 20";
        $products = $this->wpdb->get_results($query);

        // get products map not retailer current
        $productsMap = $this->wpdb->get_results("SELECT a.* FROM `{$this->wpdb->prefix}posts` AS a LEFT JOIN `{$this->wpdb->prefix}kiotviet_sync_products` AS b ON a.ID = b.product_id WHERE a.post_status = 'publish' AND a.post_type = 'product' AND b.retailer != '" . $this->retailer . "' LIMIT 20");
        $data = array_merge($products, $productsMap);
        return $data;
    }

    public function getProductMap()
    {
        $data = kiotviet_sync_get_request('data', []);
        $productMaps = [];
        $parentIds = [];
        if ($data) {
            $productMaps = $this->wpdb->get_results("SELECT * FROM `{$this->wpdb->prefix}kiotviet_sync_products` AS a INNER JOIN `{$this->wpdb->prefix}posts` AS b ON a.product_id = b.ID WHERE a.product_kv_id IN (" . implode(",", $data) . ") AND `retailer` = '" . $this->retailer . "'", ARRAY_A);
        }

        foreach ($productMaps as $key => $item) {
            if (!empty($item['post_parent'])) {
                $productParent = wc_get_product($item['post_parent']);
                $productMaps[$key]["product_parent_name"] = $productParent ? $productParent->get_name() : "";
            }
        }

        return $productMaps;
    }

    public function addProduct()
    {
        $data = kiotviet_sync_get_request('data', []);
        $data = json_decode(html_entity_decode(stripslashes($data)), true);
        $typeProduct = kiotviet_sync_get_request('typeProduct', '');
        $products = [];
        // check product not exits website
        $getCategoryIdMaps = $this->getCategoryIdMaps($typeProduct, $data);
        $productSyncMaps = $this->getProductSync($data);
        try {
            foreach ($data as $item) {
                if (empty($productSyncMaps[$item['kv_id']])) {
                    if ($typeProduct == 'simple') {
                        // set category map
                        $item['category_ids'] = array(!empty($getCategoryIdMaps[$item['category_kv']]) ? $getCategoryIdMaps[$item['category_kv']] : []);
                        $productSimple = $this->KiotvietWcProduct->import_product($item);
                        if (!is_wp_error($productSimple)) {
                            kv_sync_log('KiotViet', 'Website', 'Tạo sản phẩm thành công' . ' Mã sản phẩm: #' . $productSimple, json_encode($item), 2, $productSimple);
                            $products[] = [
                                'productId' => $productSimple,
                                'productKvId' => $item['kv_id'],
                                'dataRaw' => $item['data_raw'],
                            ];
                        }
                    } else if ($typeProduct == 'variable') {
                        if ($item['type'] == "variable") {
                            // set category map
                            $item['category_ids'] = array(!empty($getCategoryIdMaps[$item['category_kv']]) ? $getCategoryIdMaps[$item['category_kv']] : []);
                            $parentId = $this->KiotvietWcProduct->import_product($item);
                            if (!is_wp_error($parentId)) {
                                kv_sync_log('KiotViet', 'Website', 'Tạo sản phẩm thành công' . ' Mã sản phẩm: #' . $parentId, json_encode($item), 2, $parentId);
                                $insert = [
                                    'product_id' => $parentId,
                                    'product_kv_id' => 0,
                                    'data_raw' => $item["data_raw"],
                                    'parent' => $item['master_product_id'],
                                ];

                                $this->insertProductSync($insert);
                            }
                        } else if ($item['type'] == "variation") {
                            $wcProductSync = $this->wpdb->get_row("SELECT `product_id` FROM {$this->wpdb->prefix}kiotviet_sync_products WHERE `parent` = " . $item['master_product_id'] . " AND `retailer` = '" . $this->retailer . "'", ARRAY_A);
                            if ($wcProductSync) {
                                $item['parent_id'] = $wcProductSync['product_id'];
                                $productVariant = $this->KiotvietWcProduct->import_product($item);
                                if (!is_wp_error($productVariant)) {
                                    $products[] = [
                                        'productId' => $productVariant,
                                        'productKvId' => $item['kv_id'],
                                        'dataRaw' => $item['data_raw'],
                                    ];
                                }
                            }
                        }
                    }
                }

            }

            foreach ($products as $product) {
                $insert = [
                    'product_id' => $product["productId"],
                    'product_kv_id' => $product["productKvId"],
                    'data_raw' => $product["dataRaw"],
                    'parent' => !empty($product['parent']) ? $product['parent'] : 0,
                ];
                $this->insertProductSync($insert);
            }
        } catch (Exception $e) {
            return ($e->getMessage());
        }

        return true;
    }

    public function getProductSync($data)
    {
        $productKvIds = [];
        $productSyncMaps = [];
        foreach ($data as $item) {
            $productKvIds[] = $item['kv_id'];
        }

        if($productKvIds){
            $productSyncs = $this->wpdb->get_results("SELECT * FROM {$this->wpdb->prefix}kiotviet_sync_products WHERE `product_kv_id` IN (" . implode(",", $productKvIds) . ")", ARRAY_A);
        }

        foreach ($productSyncs as $productSync) {
            $productSyncMaps[$productSync['product_kv_id']] = $productSync['product_id'];
        }
        return $productSyncMaps;
    }

    public function deleteAllProduct()
    {
        // Map and remove => ignore remove
        $productMapIds = kiotviet_sync_get_request("product_map_ids", []);
        if ($productMapIds) {
            $wcProductSync = $this->wpdb->get_results("SELECT product_id FROM {$this->wpdb->prefix}kiotviet_sync_products");
            $productSyncIds = [];
            foreach ($wcProductSync as $item) {
                $productSyncIds[] = $item->product_id;
            }
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}term_relationships WHERE `object_id` IN (SELECT ID FROM {$this->wpdb->prefix}posts WHERE ID NOT IN (" . implode(",", array_merge($productSyncIds, $productMapIds)) . ") AND post_type IN ('product','product_variation'))");
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}postmeta WHERE post_id IN (SELECT ID FROM {$this->wpdb->prefix}posts WHERE ID NOT IN (" . implode(",", array_merge($productSyncIds, $productMapIds)) . ") AND post_type IN ('product','product_variation'))");
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}posts WHERE ID NOT IN (" . implode(",", array_merge($productSyncIds, $productMapIds)) . ") AND post_type IN ('product','product_variation')");
        } else {
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}term_relationships WHERE `object_id` IN (SELECT ID FROM {$this->wpdb->prefix}posts WHERE post_type IN ('product','product_variation'))");
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}postmeta WHERE post_id IN (SELECT ID FROM {$this->wpdb->prefix}posts WHERE post_type IN ('product','product_variation'))");
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}posts WHERE post_type IN ('product','product_variation')");
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}kiotviet_sync_products");
        }

        return true;
    }

    public function updateProduct()
    {
        $data = kiotviet_sync_get_request('data', []);
        $data = json_decode(html_entity_decode(stripslashes($data)), true);
        $typeProduct = sanitize_text_field(kiotviet_sync_get_request('typeProduct', ''));
        $getCategoryIdMaps = $this->getCategoryIdMaps($typeProduct, $data, false);
        foreach ($data as $item) {
            // update product
            if ($typeProduct == 'simple') {
                // set category map
                $item['args']['category_ids'] = array(!empty($getCategoryIdMaps[$item['args']['category_kv']]) ? $getCategoryIdMaps[$item['args']['category_kv']] : []);
                $update = $this->KiotvietWcProduct->import_product($item['args']);
                if (!is_wp_error($update)) {
                    kv_sync_log('KiotViet', 'Website', 'Map sản phẩm thành công' . ' Mã sản phẩm: #' . $update, json_encode($item['args']), 3, $item['id']);
                    $insert = [
                        'product_id' => $item['id'],
                        'product_kv_id' => $item['productKv']['id'],
                        'data_raw' => json_encode($item["productKv"]),
                        "created_at" => kiotviet_sync_get_current_time(),
                    ];
                    $this->insertProductSync($insert, true);
                }
            } else if ($typeProduct == 'variable') {
                if ($item['args']['type'] == "variable") {
                    $item['args']['category_ids'] = array(!empty($getCategoryIdMaps[$item['args']['category_kv']]) ? $getCategoryIdMaps[$item['args']['category_kv']] : []);
                    $item['args']['id'] = $item['id'];
                    $parentId = $this->KiotvietWcProduct->import_product($item['args']);
                    if (!is_wp_error($parentId)) {
                        kv_sync_log('KiotViet', 'Website', 'Map sản phẩm thành công' . ' Mã sản phẩm: #' . $parentId, json_encode($parentProduct), 3, $item['id']);
                        $insert = [
                            'product_id' => $item['id'],
                            'product_kv_id' => 0,
                            'data_raw' => json_encode($item["productKv"]),
                            'parent' => $item['args']['master_product_id'],
                            "created_at" => kiotviet_sync_get_current_time(),
                        ];
                        $this->insertProductSync($insert, true);
                    }
                } else if ($item['args']['type'] == "variation") {
                    $wcProductAsync = $this->wpdb->get_row("SELECT `product_id` FROM {$this->wpdb->prefix}kiotviet_sync_products WHERE `parent` = " . $item['args']['master_product_id'] . " AND `retailer` = '" . $this->retailer . "'", ARRAY_A);
                    if ($wcProductAsync) {
                        $item['args']['parent_id'] = $wcProductAsync['product_id'];
                        $productVariant = $this->KiotvietWcProduct->import_product($item['args']);
                        if (!is_wp_error($productVariant)) {
                            $insert = [
                                'product_id' => $productVariant,
                                'product_kv_id' => $item['args']['kv_id'],
                                'data_raw' => $item['args']['data_raw'],
                            ];
                            $this->insertProductSync($insert, true);
                        }
                    }
                }
            }
        }
        return true;
    }

    public function deleteArrayProductSync()
    {
        $ids = kiotviet_sync_get_request('data', []);
        if ($ids) {
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}kiotviet_sync_products WHERE `product_kv_id` IN (" . implode(",", $ids) . ")");
        }
        return true;
    }

    public function deleteProductSync($product_kv_id)
    {
        $delete = [
            "product_kv_id" => $product_kv_id,
            "retailer" => $this->retailer,
        ];

        $this->wpdb->delete($this->wpdb->prefix . "kiotviet_sync_products", $delete);
    }

    public function insertProductSync($data, $map = false)
    {
        // delete product map not current retailer
        $insert = [
            "product_id" => $data['product_id'],
            "product_kv_id" => $data['product_kv_id'],
            "data_raw" => $data['data_raw'],
            "parent" => $data['parent'],
            "retailer" => $this->retailer,
            "created_at" => kiotviet_sync_get_current_time(),
        ];

        if ($map) {
            $wcProductSync = $this->wpdb->get_row("SELECT * FROM {$this->wpdb->prefix}kiotviet_sync_products WHERE `product_id` = " . $data['product_id'] . " AND `retailer` != '" . $this->retailer . "'", ARRAY_A);
            if ($wcProductSync) {
                $update = [
                    "product_kv_id" => $data['product_kv_id'],
                    "retailer" => $this->retailer,
                    "data_raw" => $data['data_raw'],
                    "parent" => $data['parent'],
                ];
                $this->wpdb->update($this->wpdb->prefix . "kiotviet_sync_products", $update, array("id" => $wcProductSync['id']));
            } else {
                $this->wpdb->insert($this->wpdb->prefix . "kiotviet_sync_products", $insert);
            }
        } else {
            $this->wpdb->insert($this->wpdb->prefix . "kiotviet_sync_products", $insert);
        }

    }

    public function getCategoryIdMaps($typeProduct, $data, $add = true)
    {
        $categoryIds = [];
        $result = [];
        foreach ($data as $item) {
            $categoryIds[] = $add ? $item['category_kv'] : $item['args']['category_kv'];
        }

        if ($categoryIds) {
            $wcCategoryAsync = $this->wpdb->get_results("SELECT * FROM {$this->wpdb->prefix}kiotviet_sync_categories WHERE `category_kv_id` IN (" . implode(",", $categoryIds) . ") AND `retailer` = '" . $this->retailer . "'");
            foreach ($wcCategoryAsync as $item) {
                $result[$item->category_kv_id] = $item->category_id;
            }
        }

        return $result;
    }

    public function updatePrice()
    {
        $data = kiotviet_sync_get_request('data', []);
        $productIds = [];
        $productMapIds = [];
        foreach ($data as $item) {
            $productIDs[] = $item['productKvId'];
        }

        $wcProductSync = $this->wpdb->get_results("SELECT * FROM {$this->wpdb->prefix}kiotviet_sync_products WHERE `product_kv_id` IN (" . implode(",", $productIDs) . ") AND `retailer` = '" . $this->retailer . "'");
        foreach ($wcProductSync as $item) {
            $productMapIds[$item->product_kv_id] = $item->product_id;
        }

        foreach ($data as $item) {
            $productId = !empty($productMapIds[$item['productKvId']]) ? $productMapIds[$item['productKvId']] : 0;
            if ($productId) {
                $product = wc_get_product($productId);
                if ($product) {
                    $product->set_regular_price($item['regularPrice']);
                    $product->set_sale_price($item['salePrice']);
                    $product->save();
                }
            }
        }
        return true;
    }

    public function updateStock()
    {
        $data = kiotviet_sync_get_request('data', []);
        $productIds = [];
        $productMapIds = [];
        foreach ($data as $item) {
            $productIDs[] = $item['productKvId'];
        }

        $wcProductSync = $this->wpdb->get_results("SELECT * FROM {$this->wpdb->prefix}kiotviet_sync_products WHERE `product_kv_id` IN (" . implode(",", $productIDs) . ") AND `retailer` = '" . $this->retailer . "'");
        foreach ($wcProductSync as $item) {
            $productMapIds[$item->product_kv_id] = $item->product_id;
        }

        foreach ($data as $item) {
            $productId = !empty($productMapIds[$item['productKvId']]) ? $productMapIds[$item['productKvId']] : 0;
            if ($productId) {
                $product = wc_get_product($productId);
                if ($product) {
                    $product->set_stock_quantity($item['stock']);
                    $product->save();

                    // update stock parent
                    if ($product->get_type() == 'variation') {
                        $parentId = $product->get_parent_id();
                        $wcParentProduct = wc_get_product($parentId);
                        if($wcParentProduct){
                            $this->KiotvietWcProduct->updateStockProductParent($wcParentProduct);
                        }
                    }
                }
            }
        }
        return true;
    }

    public function updateStatusSync()
    {
        $productId = sanitize_key(kiotviet_sync_get_request('product_id', 0));
        $status = kiotviet_sync_get_request('status', 0);
        if ($productId) {
            $productSync = $this->wpdb->get_row("SELECT * FROM {$this->wpdb->prefix}kiotviet_sync_products WHERE `product_id` = " . $productId . " AND `retailer` = '" . $this->retailer . "'", ARRAY_A);
            if ($productSync) {
                $update = [
                    "status" => $status,
                ];
                $this->wpdb->update($this->wpdb->prefix . "kiotviet_sync_products", $update, array("id" => $productSync['id']));
            }
        }
        return $status;
    }
}
