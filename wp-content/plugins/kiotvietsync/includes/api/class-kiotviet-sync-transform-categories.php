<?php

class Kiotviet_Sync_Transform_Categories
{
    private $KiotvietWcCategory;
    private $wpdb;
    private $retailer;
    public function __construct()
    {
        global $wpdb;
        $this->KiotvietWcCategory = new KiotvietWcCategory();
        $this->wpdb = $wpdb;
        $this->retailer = kiotviet_sync_get_data('retailer', "");
    }

    public function searchCategory()
    {
        $q = sanitize_text_field(kiotviet_sync_get_request('q', ""));
        $uncategorizedId = $this->KiotvietWcCategory->getUncategorizedId();
        $data = [];
        $term_ids = $this->wpdb->get_col("SELECT * FROM `{$this->wpdb->prefix}term_taxonomy` WHERE `taxonomy` = 'product_cat'");
        if ($term_ids) {
            $query = "SELECT a.* FROM `{$this->wpdb->prefix}terms` AS a LEFT JOIN `{$this->wpdb->prefix}kiotviet_sync_categories` AS b ON a.term_id = b.category_id WHERE a.name LIKE '%" . $q . "%' AND a.term_id IN (" . implode(",", $term_ids) . ") AND b.category_id IS NULL";
            if ($q) {
                $query .= " AND a.name LIKE '%" . $q . "%' AND a.term_id != " . $uncategorizedId;
            } else {
                $query .= " AND a.term_id != " . $uncategorizedId;
            }

            $query .= " LIMIT 20";
            // get category
            $categories = $this->wpdb->get_results($query);

            // get categorys not map with retailer current
            $categoriesMap = $this->wpdb->get_results("SELECT a.* FROM `{$this->wpdb->prefix}terms` AS a LEFT JOIN `{$this->wpdb->prefix}kiotviet_sync_categories` AS b ON a.term_id = b.category_id WHERE a.term_id IN (" . implode(",", $term_ids) . ") AND b.retailer != '" . $this->retailer . "' LIMIT 20");
            $data = array_merge($categories, $categoriesMap);
        }

        return $data;
    }

    public function deleteAllCategory()
    {
        $categoryMapIds = kiotviet_sync_get_request("category_map_ids", []);
        $uncategorizedId = $this->KiotvietWcCategory->getUncategorizedId();
        $categoryNotRemove = $categoryMapIds;

        // not remove uncategorized and category map
        if ($uncategorizedId) {
            $categoryNotRemove[] = $uncategorizedId;
        }

        // if action map => not remove category asyned
        if ($categoryMapIds) {
            $wcCategorySync = $this->wpdb->get_results("SELECT category_id FROM {$this->wpdb->prefix}kiotviet_sync_categories");
            $categorySyncIds = [];
            foreach ($wcCategorySync as $item) {
                $categorySyncIds[] = $item->category_id;
            }
            $this->wpdb->query("DELETE a,b FROM {$this->wpdb->prefix}terms AS a
            INNER JOIN {$this->wpdb->prefix}term_taxonomy AS b ON a.term_id = b.term_id
            WHERE b.taxonomy = 'product_cat' AND a.term_id NOT IN (" . implode(",", array_merge($categorySyncIds, $categoryNotRemove)) . ")");
        } else {
            $this->wpdb->query("DELETE a,b FROM {$this->wpdb->prefix}terms AS a
            INNER JOIN {$this->wpdb->prefix}term_taxonomy AS b ON a.term_id = b.term_id
            WHERE b.taxonomy = 'product_cat' AND a.term_id != $uncategorizedId");
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}kiotviet_sync_categories WHERE `retailer` = '" . $this->retailer . "'");
        }

        return true;
    }

    public function getCategoryMap()
    {
        $data = kiotviet_sync_get_request('data', []);
        if ($data) {
            $data = $this->wpdb->get_results("SELECT * FROM `{$this->wpdb->prefix}kiotviet_sync_categories` AS a INNER JOIN `{$this->wpdb->prefix}terms` AS b ON a.category_id = b.term_id WHERE a.category_kv_id IN (" . implode(",", $data) . ") AND `retailer` = '" . $this->retailer . "'", ARRAY_A);
        }
        return $data;
    }

    public function addCategory()
    {
        $data = kiotviet_sync_get_request('data', []);
        foreach ($data as $item) {
            $category_id = $this->KiotvietWcCategory->add_category($item);
            if (!is_wp_error($category_id)) {
                $insert = [
                    'category_id' => $category_id,
                    'category_kv_id' => $item["categoryKvId"],
                    'data_raw' => $item["dataRaw"],
                ];

                $this->insertCategorySync($insert);
            }
        }

        return true;
    }

    public function updateCategory()
    {
        $data = kiotviet_sync_get_request('data', []);
        foreach ($data as $item) {
            $categoryKv = json_decode(html_entity_decode(stripslashes($item['categoryKv'])), true);
            $update = $this->KiotvietWcCategory->edit_category($item);
            if (!is_wp_error($update)) {
                // insert category sync after remove
                $insert = [
                    'category_id' => $item['id'],
                    'category_kv_id' => $categoryKv["categoryId"],
                    'data_raw' => $item["categoryKv"],
                ];
                $this->insertCategorySync($insert);
            }
        }
        return true;
    }

    public function deleteCategorySync($category_kv_id)
    {
        $delete = [
            "category_kv_id" => $category_kv_id,
            "retailer" => $this->retailer,
        ];
        $this->wpdb->delete($this->wpdb->prefix . "kiotviet_sync_categories", $delete);
    }

    public function insertCategorySync($data)
    {
        $wcCategorySync = $this->wpdb->get_row("SELECT * FROM {$this->wpdb->prefix}kiotviet_sync_categories WHERE `category_id` = " . $data['category_id'] . " AND `retailer` != '" . $this->retailer . "'", ARRAY_A);
        if ($wcCategorySync) {
            $update = [
                "category_kv_id" => $data['category_kv_id'],
                "retailer" => $this->retailer,
                "data_raw" => $data['data_raw'],
            ];

            $this->wpdb->update($this->wpdb->prefix . "kiotviet_sync_categories", $update, array("id" => $wcCategorySync['id']));
        } else {
            $insert = [
                "category_id" => $data['category_id'],
                "category_kv_id" => $data['category_kv_id'],
                "data_raw" => $data['data_raw'],
                "retailer" => $this->retailer,
                "created_at" => kiotviet_sync_get_current_time(),
            ];

            $this->wpdb->insert($this->wpdb->prefix . "kiotviet_sync_categories", $insert);
        }
    }

    public function deleteArrayCategorySync()
    {
        $ids = kiotviet_sync_get_request('data', []);
        if ($ids) {
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}kiotviet_sync_categories WHERE `category_kv_id` IN (" . implode(",", $ids) . ") AND `retailer` = '" . $this->retailer . "'");
        }
        return true;
    }

    public function updateCategoryMap()
    {
        $data = kiotviet_sync_get_request('data', []);
        $categoryKvIds = [];
        $response = [];
        foreach ($data as $item) {
            $categoryKv = json_decode(html_entity_decode(stripslashes($item['categoryKv'])), true);
            $categoryKvIds[] = $categoryKv['categoryId'];
        }

        if ($categoryKvIds) {
            $wcCategorySyncs = $this->wpdb->get_results("SELECT * FROM {$this->wpdb->prefix}kiotviet_sync_categories WHERE `category_kv_id` IN (" . implode(",", $categoryKvIds) . ") AND `retailer` = '" . $this->retailer . "'");
            if ($wcCategorySyncs) {
                $categoryMap = [];
                foreach ($wcCategorySyncs as $wcCategorySync) {
                    $categoryMap[$wcCategorySync->category_kv_id] = $wcCategorySync->category_id;
                }
            }
        }

        foreach ($data as $item) {
            $categoryKv = json_decode(html_entity_decode(stripslashes($item['categoryKv'])), true);
            $item['id'] = $categoryMap[$categoryKv['categoryId']];
            $result = $this->KiotvietWcCategory->edit_category($item);
            if(!is_wp_error($result)){
                $response[] = $categoryKv['categoryId'];
            }
        }

        return $response;
    }
}
