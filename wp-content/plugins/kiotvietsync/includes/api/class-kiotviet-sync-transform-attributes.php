<?php
class Kiotviet_Sync_Transform_Attributes
{
    private $KiotvietWcAttribute, $wpdb;
    public function __construct()
    {
        global $wpdb;
        $this->KiotvietWcAttribute = new KiotvietWcAttribute();
        $this->wpdb = $wpdb;
    }

    public function searchAttribute()
    {
        $q = sanitize_text_field(kiotviet_sync_get_request("q", ""));
        $attributes = [];
        if ($q) {
            $query = "SELECT a.* FROM `{$this->wpdb->prefix}woocommerce_attribute_taxonomies` AS a LEFT JOIN `{$this->wpdb->prefix}kiotviet_sync_attributes` AS b ON a.attribute_id = b.attribute_id WHERE a.attribute_label LIKE '%" . $q . "%' AND b.attribute_id IS NULL LIMIT 10";
        }else{
            $query = "SELECT a.* FROM `{$this->wpdb->prefix}woocommerce_attribute_taxonomies` AS a LEFT JOIN `{$this->wpdb->prefix}kiotviet_sync_attributes` AS b ON a.attribute_id = b.attribute_id WHERE b.attribute_id IS NULL LIMIT 20";
        }

        $attributes = $this->wpdb->get_results($query);
        return $attributes;
    }

    public function deleteAllAttribute()
    {
        $attributeMapIds = kiotviet_sync_get_request("attribute_map_ids", []);
        $this->wpdb->query("DELETE FROM `{$this->wpdb->prefix}terms` WHERE `term_id` IN (SELECT `term_id` FROM `{$this->wpdb->prefix}term_taxonomy` WHERE `taxonomy` LIKE 'pa_%')");
        $this->wpdb->query("DELETE FROM `{$this->wpdb->prefix}term_taxonomy` WHERE `taxonomy` LIKE 'pa_%'");
        $this->wpdb->query("DELETE FROM `{$this->wpdb->prefix}term_relationships` WHERE `term_taxonomy_id` not IN (SELECT `term_taxonomy_id` FROM `wp_term_taxonomy`)");

        $query = "DELETE FROM `{$this->wpdb->prefix}woocommerce_attribute_taxonomies`";
        if ($attributeMapIds) {
            $wcAttributeSync = $this->wpdb->get_results("SELECT attribute_id FROM {$this->wpdb->prefix}kiotviet_sync_attributes");
            $attributeSyncIds = [];
            foreach ($wcAttributeSync as $item) {
                $attributeSyncIds[] = $item->attribute_id;
            }
            $query .= " WHERE `attribute_id` NOT IN (" . implode(",", array_merge($attributeMapIds, $attributeSyncIds)) . ")";
        } else {
            $this->wpdb->query("DELETE FROM `{$this->wpdb->prefix}kiotviet_sync_attributes`");
        }

        $this->wpdb->query($query);
        delete_transient('wc_attribute_taxonomies');
        return true;
    }

    public function getAttributeMap()
    {
        $data = kiotviet_sync_get_request('data', []);
        if ($data) {
            $data = $this->wpdb->get_results("SELECT * FROM `{$this->wpdb->prefix}kiotviet_sync_attributes` AS a INNER JOIN `{$this->wpdb->prefix}woocommerce_attribute_taxonomies` AS b ON a.attribute_id = b.attribute_id WHERE a.attribute_kv_id IN (" . "'" . implode("', '", $data) . "'" . ") LIMIT 10", ARRAY_A);
        }
        return $data;
    }

    public function addAttribute()
    {
        $data = kiotviet_sync_get_request('data', []);
        foreach ($data as $item) {
            $attribute_id = $this->KiotvietWcAttribute->add_attribute($item);
            if (!is_wp_error($attribute_id)) {
                $this->insertAttributeSync($attribute_id, $item["name"], $item["dataRaw"]);
            }
        }

        $attribute_taxonomies = $this->wpdb->get_results("SELECT * FROM {$this->wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name != '' ORDER BY attribute_name ASC;");
        set_transient('wc_attribute_taxonomies', $attribute_taxonomies);
        return true;
    }

    public function updateAttribute()
    {
        $data = kiotviet_sync_get_request('data', []);
        foreach ($data as $item) {
            $attributeKv = json_decode(html_entity_decode(stripslashes($item['attributeKv'])), true);
            $wcAttributeSync = $this->wpdb->query("SELECT `attribute_kv_id` FROM {$this->wpdb->prefix}kiotviet_sync_attributes WHERE `attribute_kv_id` = " . "'" . $attributeKv['attributeName'] . "'" . "", ARRAY_A);
            if (!$wcAttributeSync) {
                if (!empty($item['id'])) {
                    $update = $this->KiotvietWcAttribute->edit_attribute(($item));
                    if ((is_wp_error($update) && $update->get_error_code() == 'product_attribute_slug_exits' && $item['attribute']['name'] == $attributeKv['attributeName']) || !is_wp_error($update)) {
                        $this->insertAttributeSync($item['id'], $attributeKv["attributeName"], $item["attributeKv"]);
                    }
                }
            } else {
                if (empty($item['id'])) {
                    $this->deleteAttributeSync($attributeKv["attributeName"]);
                } else {
                    $slug = wc_sanitize_taxonomy_name(stripslashes($item['args']['name']));
                    $checkAttributeExits = taxonomy_exists(wc_attribute_taxonomy_name($slug));
                    // category in website is remove
                    if (!$checkAttributeExits) {
                        $this->deleteAttributeSync($attributeKv["attributeName"]);

                        $update = $this->KiotvietWcAttribute->edit_attribute($item);
                        if (!is_wp_error($update)) {
                            $this->insertAttributeSync($item['id'], $attributeKv["attributeName"], $item["attributeKv"]);
                        }

                    }
                }
            }
        }

        $attribute_taxonomies = $this->wpdb->get_results("SELECT * FROM {$this->wpdb->prefix}woocommerce_attribute_taxonomies WHERE attribute_name != '' ORDER BY attribute_name ASC;");
        set_transient('wc_attribute_taxonomies', $attribute_taxonomies);

        return true;
    }

    public function deleteAttributeSync($attribute_kv_id)
    {
        $delete = [
            "attribute_kv_id" => $attribute_kv_id,
        ];
        $this->wpdb->delete($this->wpdb->prefix . "kiotviet_sync_attributes", $delete);
    }

    public function insertAttributeSync($attribute_id, $attribute_kv_id, $data_raw)
    {
        $insert = [
            "attribute_id" => $attribute_id,
            "attribute_kv_id" => $attribute_kv_id,
            "data_raw" => $data_raw,
            "created_at" => kiotviet_sync_get_current_time(),
        ];
        $this->wpdb->insert($this->wpdb->prefix . "kiotviet_sync_attributes", $insert);
    }

    public function deleteArrayAttributeSync()
    {
        $ids = kiotviet_sync_get_request('data', []);
        if ($ids) {
            $this->wpdb->query("DELETE FROM {$this->wpdb->prefix}kiotviet_sync_attributes WHERE `attribute_kv_id` IN (" . implode(",", $ids) . ")");
        }
        return true;
    }
}
