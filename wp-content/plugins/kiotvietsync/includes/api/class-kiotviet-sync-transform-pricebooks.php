<?php
class Kiotviet_Sync_Transform_PriceBooks
{
    public function saveConfig()
    {
        $data = kiotviet_sync_get_request('data', []);
        if (!empty($data['regularPrice'])) {
            kiotviet_sync_set_data('regular_price', $data['regularPrice']);
        }

        if (!empty($data['salePrice'])) {
            kiotviet_sync_set_data('sale_price', $data['salePrice']);
        }
        return true;
    }

    public function getConfig()
    {
        $regularPrice = kiotviet_sync_get_data('regular_price');
        $salePrice = kiotviet_sync_get_data('sale_price');
        return [
            "regular_price" => json_decode(html_entity_decode(stripslashes($regularPrice)), true),
            "sale_price" => json_decode(html_entity_decode(stripslashes($salePrice)), true),
        ];
    }
}
