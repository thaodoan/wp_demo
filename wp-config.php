<?php
define('WP_CACHE', false); // Added by WP Rocket
define('WP_MEMORY_LIMIT', '256M');
define('COOKIE_DOMAIN', false);
set_time_limit(300);
define( 'WP_AUTO_UPDATE_CORE', false );

/**
 * setting wordpress multisite
 * */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', true);
define('DOMAIN_CURRENT_SITE', 'wpdemo.test');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_demo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g>rR})TX-,ue4`J8f$P*qf;cqLQ`AOC!c]b|=iRhLm@n(pk;B,b;gN_&#-GSleL!' );
define( 'SECURE_AUTH_KEY',  'eJ-6>j3~@Wped?.GGk{lMIw$ihT%G(Bjy2U+$$je)(hvvn RjPBO{Dkw^1y[}Uyx' );
define( 'LOGGED_IN_KEY',    'a8.=)l&t*<]u.I:WrsK2q0_ttvb(,6lD)D~oh}Fk{n0oOo()d*Y <!Pn~O2YB]3f' );
define( 'NONCE_KEY',        '3{a(JzhsURWrZPRKX+_sZn+8^utdDAiC[0pU|YEV_6~p{1#c#8Y,*Eq@=HI<`/r#' );
define( 'AUTH_SALT',        '](>CC;V-rW;Nyb<s>z2?$g{:47G nmu6H:[6FvQe2~&F.@=:Z;opC(,nA/b#2Lq)' );
define( 'SECURE_AUTH_SALT', 'wO.@?+*,F/JAB7(}oYS}mgx*ZXh08kCGp247hu)(U>(z#5I~1dkuGg&Oi}-TU|Yr' );
define( 'LOGGED_IN_SALT',   '8kzhwN20C1d qlyc:=a4{#1J%EszyB`mIJv}g&sX%WNrQP^IF!V2p#HHlDtAT^tc' );
define( 'NONCE_SALT',       '9JbzZaiK.ZEj$=h+$2~_Vd%5[OSGl9&SU}Xx=~g5?VCU|o`M$D2:=f`$_,R`C<bL' );
define('EM_GUTENBERG', true);
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define('WP_DEBUG_DISPLAY', true);

@ini_set('display_errors', E_ALL);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );