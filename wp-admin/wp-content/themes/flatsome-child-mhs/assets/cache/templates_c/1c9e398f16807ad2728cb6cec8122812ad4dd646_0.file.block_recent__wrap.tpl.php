<?php
/* Smarty version 3.1.33, created on 2020-05-16 17:15:16
  from 'D:\webdesign\public_html\wp_demo\wp-content\themes\flatsome-child-mhs\block\shop\block_recent__wrap.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5ec01fa459d1c5_35480703',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1c9e398f16807ad2728cb6cec8122812ad4dd646' => 
    array (
      0 => 'D:\\webdesign\\public_html\\wp_demo\\wp-content\\themes\\flatsome-child-mhs\\block\\shop\\block_recent__wrap.tpl',
      1 => 1589649307,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ec01fa459d1c5_35480703 (Smarty_Internal_Template $_smarty_tpl) {
?><aside class="widget woocommerce widget_recently_viewed_products">
    <span class="widget-title shop-sidebar"><?php echo $_smarty_tpl->tpl_vars['BLOCK_TITLE']->value;?>
</span>
    <div class="is-divider small mb-0"></div>
    <ul class="product_list_widget | ">
        <?php echo $_smarty_tpl->tpl_vars['BLOCK_CONTENT']->value;?>

    </ul>
</aside><?php }
}
