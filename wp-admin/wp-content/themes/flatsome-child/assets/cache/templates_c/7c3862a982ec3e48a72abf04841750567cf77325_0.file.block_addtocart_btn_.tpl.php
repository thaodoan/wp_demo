<?php
/* Smarty version 3.1.33, created on 2019-11-06 09:13:35
  from 'D:\public_html\test_p\wp-content\themes\flatsome-child\block\shop\block_addtocart_btn_.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dc28ebfdd08e4_77349248',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7c3862a982ec3e48a72abf04841750567cf77325' => 
    array (
      0 => 'D:\\public_html\\test_p\\wp-content\\themes\\flatsome-child\\block\\shop\\block_addtocart_btn_.tpl',
      1 => 1573030952,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dc28ebfdd08e4_77349248 (Smarty_Internal_Template $_smarty_tpl) {
?><a href="<?php echo $_smarty_tpl->tpl_vars['product']->value->add_to_cart_url();?>
" data-quantity="<?php echo $_smarty_tpl->tpl_vars['args']->value['quantity'];?>
" class="<?php echo $_smarty_tpl->tpl_vars['args']->value['class'];?>
 | text-nowrap font-weight-500 text-size-n1 btn btn-warning" <?php echo $_smarty_tpl->tpl_vars['args']->value['attributes'];?>
>
    <?php echo $_smarty_tpl->tpl_vars['product']->value->add_to_cart_text();?>

</a><?php }
}
