<?php
/* Smarty version 3.1.33, created on 2021-01-28 08:51:37
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_recent__wrap.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60127b19302224_36957564',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '81f1b87b1667a997f03d6ca2a8a333d9f27520ff' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_recent__wrap.tpl',
      1 => 1588556470,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60127b19302224_36957564 (Smarty_Internal_Template $_smarty_tpl) {
?><aside class="widget woocommerce widget_recently_viewed_products">
    <span class="widget-title shop-sidebar"><?php echo $_smarty_tpl->tpl_vars['BLOCK_TITLE']->value;?>
</span>
    <div class="is-divider small mb-0"></div>
    <ul class="product_list_widget | ">
        <?php echo $_smarty_tpl->tpl_vars['BLOCK_CONTENT']->value;?>

    </ul>
</aside><?php }
}
