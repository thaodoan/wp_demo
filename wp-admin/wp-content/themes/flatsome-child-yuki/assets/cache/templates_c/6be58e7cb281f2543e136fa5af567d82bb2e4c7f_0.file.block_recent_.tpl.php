<?php
/* Smarty version 3.1.33, created on 2021-01-28 08:51:37
  from 'D:\public_html\test_p2\wp_demo\wp-content\themes\flatsome-child-yuki\block\shop\block_recent_.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_60127b191fae24_92532098',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6be58e7cb281f2543e136fa5af567d82bb2e4c7f' => 
    array (
      0 => 'D:\\public_html\\test_p2\\wp_demo\\wp-content\\themes\\flatsome-child-yuki\\block\\shop\\block_recent_.tpl',
      1 => 1589947555,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_60127b191fae24_92532098 (Smarty_Internal_Template $_smarty_tpl) {
?><li class="content-widget-product | ">
    <a href="<?php echo $_smarty_tpl->tpl_vars['product']->value->link;?>
">
        <img src="<?php echo $_smarty_tpl->tpl_vars['THUMB']->value->img_src;?>
" width="<?php echo $_smarty_tpl->tpl_vars['THUMB']->value->img_width;?>
" class="woocommerce-placeholder wp-post-image" alt="<?php echo $_smarty_tpl->tpl_vars['THUMB']->value->img_alt;?>
">
        <span class="product-title"><?php echo $_smarty_tpl->tpl_vars['product']->value->title;?>
</span>
    </a>
    <div class="price color-pink font-weight-500">
        <?php echo $_smarty_tpl->tpl_vars['product']->value->price;?>

    </div>
</li>
<?php }
}
